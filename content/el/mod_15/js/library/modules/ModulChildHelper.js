var ModulChildHelper = {
    mParent: parent.ModulHelper.mParent,
    mhypeDoc: null,
    mFirstLoad: true,
    mChangeScene: false,
    layoutType: parent.ModulHelper.mParent.Config.layoutType,
    animTimeline: {},
    animSymTimeline: {}
};
ModulChildHelper.docLoaded = function(hypeDocument, element, event) {
    ModulChildHelper.mhypeDoc = hypeDocument;
    window.myHypeContainerId = hypeDocument.documentId();
    if (ModulChildHelper.isScalePossible()) {
        $('#' + window.myHypeContainerId).css({
            '-moz-transform-origin': '0% 0%',
            '-webkit-transform-origin': '0% 0%',
            '-ms-transform-origin': '0% 0%',
            '-o-transform-origin': '0% 0%',
            'transform-origin': '0% 0%',
            margin: 0
        });
        ModulChildHelper.scaleSite();
        $(window).resize(function() {
            ModulChildHelper.scaleSite();
        });
    }
    ModulChildHelper.mhypeDoc.currSceneElem = function() {
        var hc = document.getElementById(hypeDocument.documentId());
        var sa = hc.getElementsByClassName("HYPE_scene");
        for (i = 0; i < sa.length; i++) {
            if (sa[i].style.display === "block") return sa[i];
        }
        return "body";
    }
    return true;
};
ModulChildHelper.sceneLoaded = function(hypeDocument, event) {
    ModulChildHelper.scaleSite();
    ModulChildHelper.loadContent();
};
ModulChildHelper.sceneUnloaded = function(hypeDocument, element, event) {
    ModulChildHelper.unloadedContent();
};
ModulChildHelper.layoutChange = function(hypeDocument, element, event) {
    if (ModulChildHelper.layoutType !=0) {
        return "layout_" + ModulChildHelper.mParent.Config.breakIdx;
    }
};
ModulChildHelper.isScalePossible = function() {
    can = 'MozTransform' in document.body.style;
    if(!can) can = 'webkitTransform' in document.body.style;
    if(!can) can = 'msTransform' in document.body.style;
    if(!can) can = 'OTransform' in document.body.style;
    if(!can) can = 'transform' in document.body.style;
    if(!can) can = 'Transform' in document.body.style;
    return can;
};
ModulChildHelper.scaleSite = function() {
    var hypeContainer = $('#' + window.myHypeContainerId);
    var containerWidth = hypeContainer.width();
    var containerHeight = hypeContainer.height();
    var parentWidth = parent.ModulHelper.childFrameElement.offsetWidth;
    var parentHeight = parent.ModulHelper.childFrameElement.offsetHeight;
    var scaleWidth = parentWidth / containerWidth;
    var scaleHeight = parentHeight / containerHeight;
    var scale = Math.max(scaleWidth, scaleHeight);
    var left = (containerWidth * scale < parentWidth) ? ((parentWidth - (containerWidth * scale)) / 2) + 'px' : '0px';
    var top = (containerHeight * scale < parentHeight) ? ((parentHeight - (containerHeight * scale)) / 2) + 'px' : '0px';
    hypeContainer.css({
        "-moz-transform"    : "scale("+scale+")",
        "-webkit-transform" : "scale("+scale+")",
        "-ms-transform"     : "scale("+scale+")",
        "-o-transform"      : "scale("+scale+")",
        "transform"         : "scale("+scale+")",
        "left" : left,
        "top" : top
    });
};
ModulChildHelper.loadContent = function() {
    var i, j;
    ModulChildHelper.mhypeDoc.contentSymbol = [];
    var allID = document.querySelectorAll('[id]');
    allID.forEach(function(el, sy) {
        if (ModulChildHelper.mhypeDoc.getSymbolInstanceById(el.id) != null && ModulChildHelper.mhypeDoc.getSymbolInstanceById(el.id) != undefined) {
            ModulChildHelper.mhypeDoc.contentSymbol.push(ModulChildHelper.mhypeDoc.getSymbolInstanceById(el.id));
        }
    });
    ModulChildHelper.mhypeDoc.setParentBGColor = function() {
        setTimeout(function() {
            ModulChildHelper.mParent.EffectManager.changeBGColor($(ModulChildHelper.mhypeDoc.currSceneElem()).css("background-color"));
        }, 100);
    };
    ModulChildHelper.mhypeDoc.setContentText = function(txtObj, contentID) {
        if (!ModulChildHelper.mParent.Config.useContentText) return;
        setTimeout(function() {
            i = 0;
            for (obj in txtObj) {
                $("." + contentID + "_" + i).html("" + txtObj[contentID + "_" + i]);
                i++;
            }
        }, 100);
    };
    ModulChildHelper.mParent.Config.hypeContent.hypeContentChild = null;
    ModulChildHelper.mParent.Config.hypeContent.hypeContentChild = ModulChildHelper.mhypeDoc;
    ModulChildHelper.mParent.Main.checkContent();
    var scID = parseInt(ModulChildHelper.mhypeDoc.currentSceneName().substr(6));
    if (ModulChildHelper.mParent.Config.sceneLocation[ModulChildHelper.mParent.Config.page] < scID) {
        ModulChildHelper.mParent.Config.sceneLocation[ModulChildHelper.mParent.Config.page] = scID;
    }
    ModulChildHelper.mParent.Config.currentScene = scID;
    ModulChildHelper.mParent.ScormManager.setLmsLocation();
};
ModulChildHelper.openPDF = function(_pdfFileName) {
    ModulChildHelper.mParent.PopupManager.openPDFFrame(_pdfFileName);
};
ModulChildHelper.playBacksound = function(_id) {
    ModulChildHelper.mParent.AudioManager.playBacksound(_id);
};
ModulChildHelper.stopBacksound = function() {
    ModulChildHelper.mParent.AudioManager.stopBacksound();
};
ModulChildHelper.playSoundFX = function(_id) {
    ModulChildHelper.mParent.AudioManager.playSoundFX(_id);
};
ModulChildHelper.stopSoundFX = function(_id) {
    ModulChildHelper.mParent.AudioManager.stopSoundFX(_id);
};
ModulChildHelper.playHostInteraction = function(_id, _useContinue, _symID, _callback) {
    if (ModulChildHelper.mParent.Config.useAudio) {
        ModulChildHelper.mParent.AudioManager.playChildAudio(_id, _useContinue, _symID, _callback);
    }
    if (ModulChildHelper.mParent.Config.useSubtitle) {
        var idSub = parseInt(_id) - 1;
        ModulChildHelper.mParent.PopupManager.updateChildSubtitleText(idSub);
    }
};
ModulChildHelper.stopHostInteraction = function() {
    if (ModulChildHelper.mParent.Config.useAudio) {
        ModulChildHelper.mParent.AudioManager.stopChildAudio();
    }
    if (ModulChildHelper.mParent.Config.useSubtitle) {
        ModulChildHelper.mParent.PopupManager.resetSubtitleText();
    }
};
ModulChildHelper.playAudio = function(_id, _useContinue, _symID, _callback) {
    ModulChildHelper.playHostInteraction(_id, _useContinue, _symID, _callback);
};
ModulChildHelper.stopAudio = function() {
    ModulChildHelper.stopHostInteraction();
};
ModulChildHelper.setSubtitle = function(_id) {
    var _idx = (_id + 1);
    ModulChildHelper.playHostInteraction(_idx);
};
ModulChildHelper.resetSubtitle = function() {
    ModulChildHelper.stopHostInteraction();
};
ModulChildHelper.closeThisFrame = function() {
    ModulChildHelper.mParent.Config.hypeContent.hypeContentChild = null;
    parent.ModulHelper.mhypeDoc.closingFrame();
};
if("HYPE_eventListeners" in document === false) {
    window.HYPE_eventListeners = Array();
}
window.HYPE_eventListeners.push({"type":"HypeDocumentLoad", "callback":ModulChildHelper.docLoaded});
window.HYPE_eventListeners.push({"type":"HypeSceneLoad", "callback":ModulChildHelper.sceneLoaded});
window.HYPE_eventListeners.push({"type":"HypeSceneUnload", "callback":ModulChildHelper.sceneUnloaded});
window.HYPE_eventListeners.push({"type":"HypeLayoutRequest", "callback":ModulChildHelper.layoutChange});