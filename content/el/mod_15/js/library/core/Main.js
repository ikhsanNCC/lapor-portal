Config.screenFrame = document.getElementById('screenF');
Config.helpFrame = document.getElementById('helpF');
Config.indexListFrame = document.getElementById('indexListF');
Config.glossaryFrame = document.getElementById('glossaryF');
Config.volumeFrame = document.getElementById('audioF');
Config.content1Frame = document.getElementById('content-1F');
Config.content2Frame = document.getElementById('content-2F');
Config.preloaderFrame = document.getElementById('loading');
var Main = {};
Main.hidePreload = function() {
    Config.preloaderState = false;
    TweenMax.to(Config.preloaderFrame, 0.3, {opacity:0,display:'none'});
};
Main.showPreload = function() {
    Config.preloaderState = true;
    TweenMax.set(Config.preloaderFrame, {display:"block"});
    TweenMax.to(Config.preloaderFrame, 0.3, {opacity:1});
};
Main.setFramesFirstTime = function() {
    //Init Main/Content Frame
    Config.frameObject = 1;
    Config.content1Frame.src = "about:blank";
    Config.content2Frame.src = "about:blank";
    TweenMax.set(Config.content1Frame, {display:'block'});
    TweenMax.set(Config.content2Frame, {display:'none'});
    switch(Config.slidingType) {
        case 0:
        {
            var cW = document.getElementById('mod-wrapper').offsetWidth;
            Config.content1Frame.style.top = "0px";
            Config.content2Frame.style.top = "0px";
            Config.content1Frame.style.left = "0px";
            Config.content2Frame.style.left = cW + "px";
            Config.content1Frame.style.zIndex = "0";
            Config.content2Frame.style.zIndex = "-1";
        }
        break;
        case 1:
        {
            var cH = document.getElementById('mod-wrapper').offsetHeight;
            Config.content1Frame.style.top = "0px";
            Config.content2Frame.style.top = cH + "px";
            Config.content1Frame.style.left = "0px";
            Config.content2Frame.style.left = "0px";
            Config.content1Frame.style.zIndex = "0";
            Config.content2Frame.style.zIndex = "-1";
        }
        break;
        case 2:
        {
            Config.content1Frame.style.top = "0px";
            Config.content2Frame.style.top = "0px";
            Config.content1Frame.style.left = "0px";
            Config.content2Frame.style.left = "0px";
            Config.content1Frame.style.opacity = "1";
            Config.content2Frame.style.opacity = "0";
            Config.content1Frame.style.zIndex = "0";
            Config.content2Frame.style.zIndex = "-1";
        }
        break;
        case 3:
        {
            Config.content1Frame.style.top = "0px";
            Config.content2Frame.style.top = "0px";
            Config.content1Frame.style.left = "0px";
            Config.content2Frame.style.left = "0px";
            Config.content1Frame.style.zIndex = "0";
            Config.content2Frame.style.zIndex = "-1";
            TweenMax.set(Config.content1Frame, {transformOrigin:"50% 50%", opacity: 1, scale: 1});
            TweenMax.set(Config.content2Frame, {transformOrigin:"50% 50%", opacity: 0, scale: 1.2});
        }
        break;
    }
    debugMsg("Set content frame for first time, slidingType : " + Config.slidingType);
    //Init Help Frame
    Config.helpFrame.style.display = 'none';
    if (Config.hypeHelp != null) {
        Config.hypeHelp.pauseTimelineNamed('Main Timeline');
        Config.hypeHelp.goToTimeInTimelineNamed(0, 'Main Timeline');
    }
    switch(Config.helpType) {
        case 0:
        {
            TweenMax.set(Config.helpFrame, {opacity:0});
        }
        break;
        case 1:
        {
            var hW = Config.helpFrame.offsetWidth;
            var finalL = 0 - (hW - (hW * 2)) - 50;
            Config.helpFrame.style.left = finalL + "px";
            TweenMax.set(Config.helpFrame, {opacity:0});
        }
        break;
        case 2:
        {
            var fW = document.getElementById('mod-frames').offsetWidth;
            var hW = Config.helpFrame.offsetWidth;
            var finalL = fW + (hW + 50);
            Config.helpFrame.style.left = finalL + "px";
            TweenMax.set(Config.helpFrame, {opacity:0});
        }
        break;
    }
    debugMsg("Set help frame for first time, helpType : " + Config.helpType);
    //Init Index List Frame
    Config.indexListFrame.style.display = 'none';
    if (Config.hypeIndexList != null) {
        Config.hypeIndexList.pauseTimelineNamed('Main Timeline');
        Config.hypeIndexList.goToTimeInTimelineNamed(0, 'Main Timeline');
    }
    switch(Config.indexListType) {
        case 0:
        {
            TweenMax.set(Config.indexListFrame, {opacity:0});
        }
        break;
        case 1:
        {
            var iW = Config.indexListFrame.offsetWidth;
            var finalL = 0 - (iW - (iW * 2)) - 50;
            Config.indexListFrame.style.left = finalL + "px";
            TweenMax.set(Config.indexListFrame, {opacity:0});
        }
        break;
        case 2:
        {
            var fW = document.getElementById('mod-frames').offsetWidth;
            var iW = Config.indexListFrame.offsetWidth;
            var finalL = fW + (iW + 50);
            Config.indexListFrame.style.left = finalL + "px";
            TweenMax.set(Config.indexListFrame, {opacity:0});
        }
        break;
    }
    debugMsg("Set index list frame for first time, indexListType : " + Config.indexListType);
    //Init Glossary Frame
    Config.glossaryFrame.style.display = 'none';
    if (Config.hypeGlossary != null) {
        Config.hypeGlossary.pauseTimelineNamed('Main Timeline');
        Config.hypeGlossary.goToTimeInTimelineNamed(0, 'Main Timeline');
    }
    switch(Config.glossaryType) {
        case 0:
        {
            TweenMax.set(Config.glossaryFrame, {opacity:0});
        }
        break;
        case 1:
        {
            var gW = Config.glossaryFrame.offsetWidth;
            var finalL = 0 - (gW - (gW * 2)) - 50;
            Config.glossaryFrame.style.left = finalL + "px";
            TweenMax.set(Config.glossaryFrame, {opacity:0});
        }
        break;
        case 2:
        {
            var fW = document.getElementById('mod-frames').offsetWidth;
            var gW = Config.glossaryFrame.offsetWidth;
            var finalL = fW + (gW + 50);
            Config.glossaryFrame.style.left = finalL + "px";
            TweenMax.set(Config.glossaryFrame, {opacity:0});
        }
        break;
    }
    debugMsg("Set glossary frame for first time, glossaryType : " + Config.glossaryType);
    //Init Help Frame
    Config.volumeFrame.style.display = 'none';
    TweenMax.set(Config.volumeFrame, {opacity:0});
    debugMsg("Set volume frame for first time");
};
Main.resetFrame = function() {
    if (Config.frameObject == 1) {
        Config.content2Frame.src = "about:blank";
    } else if (Config.frameObject == 2) {
        Config.content1Frame.src = "about:blank";
    }
    debugMsg("Reset content frame");
};
Main.startSlidingEff = function() {
    Main.hidePreload();
    if (Config.firstLoad) {
        Config.firstLoad = false;
        return;
    }
    if (Config.slidingType == 0) {
        EffectManager.horizontalSlideEff();
    } else if (Config.slidingType == 1) {
        EffectManager.verticalSlideEff();
    } else if (Config.slidingType == 2) {
        EffectManager.fadingSlideEff();
    } else if (Config.slidingType == 3) {
        EffectManager.zoomingSlideEff();
    }
    TweenMax.delayedCall(1.1, Main.resetFrame);
};
Main.setContentControlerButton = function() {
    if (!Config.useContentControl) return;
    document.getElementById("btn-control").innerHTML = '<img src="data/image/pause.png" />';
    if (document.getElementById("btn-control") != null && document.getElementById("btn-control") != undefined) {
        if (document.getElementById("btn-control").addEventListener) {
            document.getElementById("btn-control").addEventListener("click", Main.controlContent);
        } else if (document.getElementById("btn-control").attachEvent) {
            document.getElementById("btn-control").attachEvent("onclick", Main.controlContent);
        }
    }
    document.getElementById("btn-reload").innerHTML = '<img src="data/image/repeat.png" />';
    if (document.getElementById("btn-reload") != null && document.getElementById("btn-reload") != undefined) {
        if (document.getElementById("btn-reload").addEventListener) {
            document.getElementById("btn-reload").addEventListener("click", Main.controlReloadContent);
        } else if (document.getElementById("btn-reload").attachEvent) {
            document.getElementById("btn-reload").attachEvent("onclick", Main.controlReloadContent);
        }
    }
};
Main.showContentControllerButton = function() {
    if (!Config.useContentControl) return;
    var ccPosX = 0;
    if (!Config.dynamicBackgroundColor) {
        ccPosX = 0 + parseInt(document.getElementById("mod-header").offsetLeft);
        if (Config.navigationType == 0) {
            TweenMax.set(document.getElementById("mod-control"), { left: (ccPosX - 120) + 'px' });
        } else {
            TweenMax.set(document.getElementById("mod-control"), { left: (ccPosX - 60) + 'px' });
        }
    }
    TweenMax.to(document.getElementById("mod-control"), 0.3, { left: ccPosX + 'px', opacity: 1, display: "block" });
};
Main.setNaviButton = function() {
    if (!Config.useHelpWindow && !Config.useIndexListWindow && !Config.useVolumeWindow && !Config.useGlossaryWindow) return;
    //Init Navigation/Menu Button
    document.getElementById('mod-navi').className = 'nav-type-' + Config.navigationType;
    switch(Config.navigationType) {
        case 0:
        {
            var strNavDesign = "";
            strNavDesign = '<div id="btn-menu" class="btn-menu-' + Config.navigationType + ' full-height"><img id="ico-open-' + Config.navigationType + '" src="data/image/setting.png"><img id="ico-close-' + Config.navigationType + '" src="data/image/close_setting.png"></div><div id="box-menu-' + Config.navigationType + '" class="full-height"><div id="container-menu-' + Config.navigationType + '"><div class="nav-row-' + Config.navigationType + '" nomargin-nopadding">';
            var countCol = 0;
            if (Config.useIndexListWindow) {
                countCol++;
            }
            if (Config.useHelpWindow) {
                countCol++;
            }
            if (Config.useGlossaryWindow) {
                countCol++;
            }
            if (Config.useVolumeWindow) {
                countCol++;
            }
            if (Config.useSubtitle) {
                if (Config.subtitleType > 1) {
                    countCol++;
                }
            }
            Config.columnNavigation = (countCol + 1);
            Config.navigationWidth = (60 * Config.columnNavigation);
            if (Config.useIndexListWindow) {
                strNavDesign += '<div class="nav-col-' + Config.navigationType + '"><div class="box-ico-menu"><img class="ico-menu" src="data/image/indexlist.png"></div><div id="btn-indexlist" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
            }
            if (Config.useGlossaryWindow) {
                strNavDesign += '<div class="nav-col-' + Config.navigationType + '"><div class="box-ico-menu"><img class="ico-menu" src="data/image/glossary.png"></div><div id="btn-glossary" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
            }
            if (Config.useVolumeWindow) {
                strNavDesign += '<div class="nav-col-' + Config.navigationType + '"><div id="btn-volume-nav-' + Config.navigationType + '" class="box-ico-menu used"><img class="ico-menu" src="data/image/volume.png"></div><div id="btn-volume" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
            }
            if (Config.useSubtitle) {
                if (Config.subtitleType > 1) {
                    strNavDesign += '<div class="nav-col-' + Config.navigationType + '"><div id="btn-subtitle-nav-' + Config.navigationType + '" class="box-ico-menu unused"><img class="ico-menu" src="data/image/subtitle-off.png"></div><div id="btn-subtitle" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
                }
            }
            if (Config.useHelpWindow) {
                strNavDesign += '<div class="nav-col-' + Config.navigationType + '"><div class="box-ico-menu"><img class="ico-menu" src="data/image/help.png"></div><div id="btn-help" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
            }
            strNavDesign += '</div></div></div>';
            document.getElementById('mod-navi').innerHTML = strNavDesign;
            document.getElementById('box-menu-' + Config.navigationType).style.width = Config.navigationWidth + 'px';
            TweenMax.set(document.getElementById('ico-open-' + Config.navigationType), { transformOrigin:"50% 50%", scale: 1, opacity: 1, display: 'block' });
            TweenMax.set(document.getElementById('ico-close-' + Config.navigationType), { transformOrigin:"50% 50%", scale: 0, opacity: 0, display: 'none' });
            TweenMax.set(document.getElementById('container-menu-' + Config.navigationType), { backgroundColor: Config.navigationColor });
        }
        break;
        case 1:
        {
            var strNavDesign = "";
            strNavDesign = '<div><div id="nav-head-' + Config.navigationType + '"><div id="box-menu-title-' + Config.navigationType + '" class="nomargin-nopadding"><div class="nav-text"><p class="nomargin-nopadding">Menu</p></div></div><div id="box-menu-btn-' + Config.navigationType + '" class="nomargin-nopadding"><div id="btn-menu" class="btn-menu-' + Config.navigationType + '"><img id="ico-open-' + Config.navigationType + '" src="data/image/setting.png"><img id="ico-close-' + Config.navigationType + '" src="data/image/close_setting.png"></div></div></div><div class="nav-content"><div id="box-menu-' + Config.navigationType + '" class="full-height"><div id="container-menu-' + Config.navigationType + '">';
            var countCol = 0;
            if (Config.useIndexListWindow) {
                countCol++;
            }
            if (Config.useHelpWindow) {
                countCol++;
            }
            if (Config.useGlossaryWindow) {
                countCol++;
            }
            if (Config.useVolumeWindow) {
                countCol++;
            }
            if (Config.useSubtitle) {
                if (Config.subtitleType > 1) {
                    countCol++;
                }
            }
            Config.columnNavigation = countCol;
            if (Config.useIndexListWindow) {
                strNavDesign += '<div id="btn-indexlist" class="col-nav-' + Config.navigationType + ' col-padding"><div class="row nomargin-nopadding"><div class="col-xs-2 nomargin-nopadding"><div class="box-ico-menu-' + Config.navigationType + '"><img class="ico-menu-' + Config.navigationType + '" src="data/image/indexlist.png"></div></div><div class="col-xs-10 nomargin-nopadding"><div class="nav-text"><p class="nomargin-nopadding">Index List</p></div></div></div></div>';
            }
            if (Config.useGlossaryWindow) {
                strNavDesign += '<div id="btn-glossary" class="col-nav-' + Config.navigationType + ' col-padding"><div class="row nomargin-nopadding"><div class="col-xs-2 nomargin-nopadding"><div class="box-ico-menu-' + Config.navigationType + '"><img class="ico-menu-' + Config.navigationType + '" src="data/image/glossary.png"></div></div><div class="col-xs-10 nomargin-nopadding"><div class="nav-text"><p class="nomargin-nopadding">Glossary</p></div></div></div></div>';
            }
            if (Config.useVolumeWindow) {
                strNavDesign += '<div id="btn-volume" class="col-nav-' + Config.navigationType + ' col-padding"><div class="row nomargin-nopadding"><div class="col-xs-2 nomargin-nopadding"><div id="btn-volume-nav-' + Config.navigationType + '" class="box-ico-menu-' + Config.navigationType + ' used"><img class="ico-menu-' + Config.navigationType + '" src="data/image/volume.png"></div></div><div class="col-xs-10 nomargin-nopadding"><div class="nav-text"><p class="nomargin-nopadding">Volume</p></div></div></div></div>';
            }
            if (Config.useSubtitle) {
                if (Config.subtitleType > 1) {
                    strNavDesign += '<div id="btn-subtitle" class="col-nav-' + Config.navigationType + ' col-padding"><div class="row nomargin-nopadding"><div class="col-xs-2 nomargin-nopadding"><div id="btn-subtitle-nav-' + Config.navigationType + '" class="box-ico-menu-' + Config.navigationType + ' unused"><img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle-off.png"></div></div><div class="col-xs-10 nomargin-nopadding"><div class="nav-text"><p class="nomargin-nopadding">Subtitle</p></div></div></div></div>';
                }
            }
            if (Config.useHelpWindow) {
                strNavDesign += '<div id="btn-help" class="col-nav-' + Config.navigationType + ' col-padding"><div class="row nomargin-nopadding"><div class="col-xs-2 nomargin-nopadding"><div class="box-ico-menu-' + Config.navigationType + '"><img class="ico-menu-' + Config.navigationType + '" src="data/image/help.png"></div></div><div class="col-xs-10 nomargin-nopadding"><div class="nav-text"><p class="nomargin-nopadding">Help</p></div></div></div></div>';
            }
            strNavDesign += '</div></div></div>';
            document.getElementById('mod-navi').innerHTML = strNavDesign;
            TweenMax.set(document.getElementById('ico-open-' + Config.navigationType), { transformOrigin:"50% 50%", scale: 1, opacity: 1, display: 'block' });
            TweenMax.set(document.getElementById('ico-close-' + Config.navigationType), { transformOrigin:"50% 50%", scale: 0, opacity: 0, display: 'none' });
            TweenMax.set(document.getElementById('mod-navi'), { backgroundColor: Config.navigationColor });
        }
        break;
        case 2:
        {
            var strNavDesign = "";
            strNavDesign = '<div id="btn-menu" class="btn-menu-2"><img src="data/image/nav_btn_2.png" /></div><div id="box-menu-2-bg"></div><div id="box-menu-2">';
            var countCol = 0;
            if (Config.useIndexListWindow) {
                countCol++;
            }
            if (Config.useHelpWindow) {
                countCol++;
            }
            if (Config.useGlossaryWindow) {
                countCol++;
            }
            if (Config.useVolumeWindow) {
                countCol++;
            }
            if (Config.useSubtitle) {
                if (Config.subtitleType > 1) {
                    countCol++;
                }
            }
            var slotBtn = [];
            var slotInden = [], i;
            switch (countCol) {
                case 5:
                {
                    slotBtn = ["btn-menu-2-0", "btn-menu-2-1", "btn-menu-2-2", "btn-menu-2-3", "btn-menu-2-4"];
                }
                break;
                case 4:
                {
                    slotBtn = ["btn-menu-2-0", "btn-menu-2-1", "btn-menu-2-3", "btn-menu-2-4"];
                }
                break;
                case 3:
                {
                    slotBtn = ["btn-menu-2-0", "btn-menu-2-2", "btn-menu-2-4"];
                }
                break;
                case 2:
                {
                    slotBtn = ["btn-menu-2-1", "btn-menu-2-3"];
                }
                break;
                case 1:
                {
                    slotBtn = ["btn-menu-2-2"];
                }
                break;
            }
            for (i = 0; i < countCol; i++) {
                if (Config.useIndexListWindow) {
                    if (slotInden.indexOf("index") == -1) {
                        strNavDesign += '<div id="' + slotBtn[i] + '" class="btn-ico-menu-2"><div id="btn-indexlist"><img src="data/image/indexlist.png" /></div></div>';
                        slotInden.push("index");
                        continue;
                    }
                }
                if (Config.useGlossaryWindow) {
                    if (slotInden.indexOf("gloss") == -1) {
                        strNavDesign += '<div id="' + slotBtn[i] + '" class="btn-ico-menu-2"><div id="btn-glossary"><img src="data/image/glossary.png" /></div></div>';
                        slotInden.push("gloss");
                        continue;
                    }
                }
                if (Config.useVolumeWindow) {
                    if (slotInden.indexOf("vol") == -1) {
                        strNavDesign += '<div id="' + slotBtn[i] + '" class="btn-ico-menu-2"><div id="btn-volume" class="used"><img src="data/image/volume.png" /></div></div>';
                        slotInden.push("vol");
                        continue;
                    }
                }
                if (Config.useSubtitle) {
                    if (Config.subtitleType > 1) {
                        if (slotInden.indexOf("subtitle") == -1) {
                            strNavDesign += '<div id="' + slotBtn[i] + '" class="btn-ico-menu-2"><div id="btn-subtitle" class="unused"><img src="data/image/subtitle-off.png" /></div></div>';
                            slotInden.push("subtitle");
                            continue;
                        }
                    }
                }
                if (Config.useHelpWindow) {
                    if (slotInden.indexOf("help") == -1) {
                        strNavDesign += '<div id="' + slotBtn[i] + '" class="btn-ico-menu-2"><div id="btn-help"><img src="data/image/help.png" /></div></div>';
                        slotInden.push("help");
                        continue;
                    }
                }
            }
            strNavDesign += '</div>';
            document.getElementById('mod-navi').innerHTML = strNavDesign;
            document.getElementById("mod-navi").style.zIndex = 3;
            if (countCol == 4) {
                document.getElementById('btn-menu-2-1').style.top = '160px';
                document.getElementById('btn-menu-2-1').style.left = '50px';
                document.getElementById('btn-menu-2-3').style.top = '160px';
                document.getElementById('btn-menu-2-3').style.left = '130px';
            }
        }
        break;
        case 3:
        {
            var strNavDesign = "";
            strNavDesign = '<div id="btn-menu" class="btn-menu-3"><img src="data/image/nav_btn_3.png" /></div><div id="box-menu-' + Config.navigationType + '">';
            var countCol = 0;
            if (Config.useIndexListWindow) {
                countCol++;
            }
            if (Config.useHelpWindow) {
                countCol++;
            }
            if (Config.useGlossaryWindow) {
                countCol++;
            }
            if (Config.useVolumeWindow) {
                countCol++;
            }
            if (Config.useSubtitle) {
                if (Config.subtitleType > 1) {
                    countCol++;
                }
            }
            Config.columnNavigation = (countCol + 1);
            var currCount = 0;
            if (Config.useIndexListWindow) {
                strNavDesign += '<div class="btn-box-' + Config.navigationType + '" id="btn-menu-' + Config.navigationType + '-' + currCount +'"><div class="box-ico-menu-' + Config.navigationType + '"><img class="ico-menu-' + Config.navigationType + '" src="data/image/indexlist.png"></div><div id="btn-indexlist" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
                currCount++;
            }
            if (Config.useGlossaryWindow) {
                strNavDesign += '<div class="btn-box-' + Config.navigationType + '" id="btn-menu-' + Config.navigationType + '-' + currCount +'"><div class="box-ico-menu-' + Config.navigationType + '"><img class="ico-menu-' + Config.navigationType + '" src="data/image/glossary.png"></div><div id="btn-glossary" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
                currCount++;
            }
            if (Config.useVolumeWindow) {
                strNavDesign += '<div class="btn-box-' + Config.navigationType + '" id="btn-menu-' + Config.navigationType + '-' + currCount +'"><div id="btn-volume-nav-' + Config.navigationType + '" class="box-ico-menu-' + Config.navigationType + ' used"><img class="ico-menu-' + Config.navigationType + '" src="data/image/volume.png"></div><div id="btn-volume" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
                currCount++;
            }
            if (Config.useSubtitle) {
                if (Config.subtitleType > 1) {
                    strNavDesign += '<div class="btn-box-' + Config.navigationType + '" id="btn-menu-' + Config.navigationType + '-' + currCount +'"><div id="btn-subtitle-nav-' + Config.navigationType + '" class="box-ico-menu-' + Config.navigationType + ' unused"><img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle-off.png"></div><div id="btn-subtitle" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
                    currCount++;
                }
            }
            if (Config.useHelpWindow) {
                strNavDesign += '<div class="btn-box-' + Config.navigationType + '" id="btn-menu-' + Config.navigationType + '-' + currCount +'"><div class="box-ico-menu-' + Config.navigationType + '"><img class="ico-menu-' + Config.navigationType + '" src="data/image/help.png"></div><div id="btn-help" class="btn-ico-menu-' + Config.navigationType + '"></div></div>';
            }
            strNavDesign += '</div></div>';
            document.getElementById('mod-navi').innerHTML = strNavDesign;
            document.getElementById("mod-navi").style.zIndex = 3;
            TweenMax.set($(".btn-box-3"), { backgroundColor: Config.navigationColor });
        }
        break;
    }
    LayoutManager.resettingNavigation();
    //Init Navigation Button
    if (document.getElementById("btn-menu") != null && document.getElementById("btn-menu") != undefined) {
        if (document.getElementById("btn-menu").addEventListener) {
            document.getElementById("btn-menu").addEventListener("click", Main.showMenu);
        } else if (document.getElementById("btn-menu").attachEvent) {
            document.getElementById("btn-menu").attachEvent("onclick", Main.showMenu);
        }
    }
    if (document.getElementById("btn-indexlist") != null && document.getElementById("btn-indexlist") != undefined) {
        if (document.getElementById("btn-indexlist").addEventListener) {
            document.getElementById("btn-indexlist").addEventListener("click", function() { PopupManager.popupShow('indexlist'); });
        } else if (document.getElementById("btn-indexlist").attachEvent) {
            document.getElementById("btn-indexlist").attachEvent("onclick", function() { PopupManager.popupShow('indexlist'); });
        }
    }
    if (document.getElementById("btn-glossary") != null && document.getElementById("btn-glossary") != undefined) {
        if (document.getElementById("btn-glossary").addEventListener) {
            document.getElementById("btn-glossary").addEventListener("click", function() { PopupManager.popupShow('glossary'); });
        } else if (document.getElementById("btn-glossary").attachEvent) {
            document.getElementById("btn-glossary").attachEvent("onclick", function() { PopupManager.popupShow('glossary'); });
        }
    }
    if (document.getElementById("btn-volume") != null && document.getElementById("btn-volume") != undefined) {
        if (document.getElementById("btn-volume").addEventListener) {
            document.getElementById("btn-volume").addEventListener("click", function() { PopupManager.popupShow('volume'); });
        } else if (document.getElementById("btn-volume").attachEvent) {
            document.getElementById("btn-volume").attachEvent("onclick", function() { PopupManager.popupShow('volume'); });
        }
    }
    if (document.getElementById("btn-help") != null && document.getElementById("btn-help") != undefined) {
        if (document.getElementById("btn-help").addEventListener) {
            document.getElementById("btn-help").addEventListener("click", function() { PopupManager.popupShow('help'); });
        } else if (document.getElementById("btn-help").attachEvent) {
            document.getElementById("btn-help").attachEvent("onclick", function() { PopupManager.popupShow('help'); });
        }
    }
    if (Config.subtitleType > 1) {
        if (document.getElementById("btn-subtitle") != null && document.getElementById("btn-subtitle") != undefined) {
            if (document.getElementById("btn-subtitle").addEventListener) {
                document.getElementById("btn-subtitle").addEventListener("click", function() { Main.showSubtitle(); });
            } else if (document.getElementById("btn-subtitle").attachEvent) {
                document.getElementById("btn-subtitle").attachEvent("onclick", function() { Main.showSubtitle(); });
            }
        }
    }
};
Main.showNavigation = function() {
    if (!Config.useHelpWindow && !Config.useIndexListWindow && !Config.useVolumeWindow && !Config.useGlossaryWindow) return;
    switch (Config.navigationType) {
        case 0:
        {
            var navPosX = 0;
            if (Config.useContentControl) {
                navPosX = 60;
            }
            if (!Config.dynamicBackgroundColor) {
                navPosX += parseInt(document.getElementById("mod-header").offsetLeft);
                TweenMax.set(document.getElementById("mod-navi"), { left: (navPosX - 120) + 'px' });
            }
            TweenMax.to(document.getElementById("mod-navi"), 0.3, { left: navPosX + 'px', opacity: 1, display: "block" });
        }
        break;
        case 1:
        {
            var navPosX = -250;
            if (!Config.dynamicBackgroundColor) {
                navPosX = parseInt(document.getElementById("mod-header").offsetLeft) - 250;
                TweenMax.set(document.getElementById("mod-navi"), { left: (navPosX - 60) + 'px' });
            }
            TweenMax.to(document.getElementById("mod-navi"), 0.3, { left: navPosX + 'px', opacity: 1, display: "block" });
        }
        break;
        case 2:
        {
            TweenMax.set(document.getElementById("mod-navi"), { top: '-80px' });
            TweenMax.set(document.getElementById("btn-menu"), { rotation: 180, transformOrigin: "50% 50%"});
            TweenMax.set(document.getElementById("box-menu-2-bg"), { rotation: 180, transformOrigin: "50% 50%"});
            TweenMax.set(document.getElementById("box-menu-2"), { rotation: 180, transformOrigin: "50% 50%"});
            TweenMax.to(document.getElementById("mod-navi"), 0.3, { top: '-40px', opacity: 1, display: "block" });
        }
        break;
        case 3:
        {
            var headerH = document.getElementById('mod-header').offsetHeight;
            TweenMax.set(document.getElementById("mod-navi"), { height: headerH + 'px', right: '-70px', opacity: 0, display: "none" });
            TweenMax.set(document.getElementById("btn-menu"), { height: headerH + 'px' });
            for (var i = 0; i < (Config.columnNavigation - 1); i++) {
                TweenMax.set(document.getElementById("btn-menu-" + Config.navigationType + "-" + i), { top: '-70px', opacity:0, display:'none' });
            }
            TweenMax.to(document.getElementById("mod-navi"), 0.3, { right: '5px', opacity: 1, display: "block" });
            TweenMax.to(document.getElementById("header-row"), 0.3, { paddingRight: '70px' });
        }
        break;
    }
};
Main.controlContent = function() {
    if (Config.playpauseState) return;
    if (Config.helpOpen || Config.indexListOpen || Config.glossaryOpen || Config.volumeOpen) return;
    Config.playpauseState = true;
    setTimeout(function() {
        Config.playpauseState = false;
    }, 500);
    if (Config.contentPaused) {
        Main.resumeContent();
        document.getElementById('btn-control').innerHTML = '<img src="data/image/pause.png" />';
        $('#blocker').fadeOut(300);
        Config.pauseWithButton = false;
    } else {
        Main.pauseContent();
        document.getElementById('btn-control').innerHTML = '<img src="data/image/play.png" />';
        $('#blocker').fadeIn(300);
        Config.pauseWithButton = true;
    }
};
Main.controlReloadContent = function() {
    if (Config.contentPaused) return;

    if (Config.useSubtitle) {
        PopupManager.resetSubtitleText();
    }

    if (Config.useAudio) {
        AudioManager.pauseAudio();
    }
    
    Config.hypeContent.changeContentScene = true;
    Config.hypeContent.showSceneNamed('scene_' + (parseInt(Config.hypeContent.currentSceneName().substr(6))) /*, listAnimType[animType], animDur*/ );
       
};
Main.showMenu = function() {
    if (EffectManager.effectMenuStarting) return;
    if (!Config.menuOpen) {
        Config.menuOpen = true;
    } else {
        Config.menuOpen = false;
    }
    EffectManager.menuAnim(Config.menuOpen);
};
Main.setSubtitleDesign = function() {
    var modSub = document.getElementById('mod-subtitle');
    var boxSub = document.getElementById('box-subtitle');
    var textSub = document.getElementById('subTxt');
    var modSubBtn;
    if (Config.subtitleType < 2) {
        modSubBtn = document.getElementById('mod-sub-btn');
    }
    switch (Config.subtitleType) {
        case 0:
        case 2:
        {
            modSub.style.borderRadius = '5px';
            if (Config.subtitleBackgroundColor != '') {
                modSub.style.backgroundColor = Config.subtitleBackgroundColor;
            } else {
                modSub.style.backgroundColor = '#ffffff';
            }
            modSub.style.backgroundColor = '#ffffff';
            modSub.style.paddingTop = '5px';
            modSub.style.paddingLeft = '5px';
            boxSub.style.overflow = 'hidden';
            boxSub.style.paddingTop = '10px';
            boxSub.style.position = 'relative';
            textSub.style.height = 'auto';
            textSub.style.position = 'absolute';
            textSub.style.textAlign = Config.subtitleTextAlign;
            if (Config.subtitleTextColor != '') {
                textSub.style.color = Config.subtitleTextColor;
            } else {
                textSub.style.color = '#000000';
            }
            if (Config.subtitleType == 0) {
                modSubBtn.style.right = '-60px';
                modSubBtn.style.bottom = '0px';
                modSubBtn.style.position = 'absolute';
                document.getElementById('btn-sub-open').src = "data/image/btn_sub_open_" + Config.subtitleType + ".png";
                document.getElementById('btn-sub-close').src = "data/image/btn_sub_close_" + Config.subtitleType + ".png";
            }
            document.getElementById("btn-subtxt-prev").style.cursor = "pointer";
            document.getElementById("btn-subtxt-next").style.cursor = "pointer";
            if (document.getElementById("btn-subtxt-prev").addEventListener) {
                document.getElementById("btn-subtxt-prev").addEventListener("click", function() { PopupManager.subTextPrev(); });
            } else if (document.getElementById("btn-subtxt-prev").attachEvent) {
                document.getElementById("btn-subtxt-prev").attachEvent("onclick", function() { PopupManager.subTextPrev(); });
            }
            if (document.getElementById("btn-subtxt-next").addEventListener) {
                document.getElementById("btn-subtxt-next").addEventListener("click", function() { PopupManager.subTextNext(); });
            } else if (document.getElementById("btn-subtxt-next").attachEvent) {
                document.getElementById("btn-subtxt-next").attachEvent("onclick", function() { PopupManager.subTextNext(); });
            }
        }
        break;
        case 1:
        case 3:
        {
            modSub.style.background = "url('data/image/bg-sub.png')";
            modSub.style.background = 'rgba(0,0,0,0.8)';
            if (Config.subtitleBackgroundColor != '') {
                modSub.style.background = Config.subtitleBackgroundColor;
            } else {
                modSub.style.background = 'rgba(0,0,0,0.8)';
            }
            modSub.style.height = 'auto';
            modSub.style.paddingTop = '5px';
            modSub.style.paddingBottom = '5px';
            boxSub.style.width = '95%';
            boxSub.style.height = 'auto';
            textSub.style.margin = '0px';
            if (Config.subtitleTextColor != '') {
                textSub.style.color = Config.subtitleTextColor;
            } else {
                textSub.style.color = '#ffffff';
            }
            textSub.style.textAlign = Config.subtitleTextAlign;
            if (Config.subtitleType == 1) {
                var btnSubY = 0 - (modSubBtn.offsetHeight + 10);
                modSubBtn.style.bottom = btnSubY + 'px';
                modSubBtn.style.right = '-60px';
                modSubBtn.style.position = 'absolute';
                modSubBtn.style.cursor = 'pointer';
                modSubBtn.style.paddingTop = '10px';
                modSubBtn.style.paddingBottom = '0px';
                document.getElementById('btn-sub-open').style.top = '10px';
                document.getElementById('btn-sub-close').style.top = '10px';
                document.getElementById('btn-sub-open').src = "data/image/btn_sub_open_" + Config.subtitleType + ".png";
                document.getElementById('btn-sub-close').src = "data/image/btn_sub_close_" + Config.subtitleType + ".png";
            }
        }
        break;
    }
    TextHostManager.setAllText();
};
Main.initSubtitleBtn = function() {
    if (document.getElementById("mod-sub-btn") != null && document.getElementById("mod-sub-btn") != undefined) {
        if (document.getElementById("mod-sub-btn").addEventListener) {
            document.getElementById("mod-sub-btn").addEventListener("click", function() { Main.showSubtitle(); });
        } else if (document.getElementById("mod-sub-btn").attachEvent) {
            document.getElementById("mod-sub-btn").attachEvent("onclick", function() { Main.showSubtitle(); });
        }
    }
    switch (Config.subtitleType) {
        case 0:
        case 2:
        {
            if (Config.subtitleType == 0) {
                var subPosX = 0;
                if (!Config.dynamicBackgroundColor) {
                    subPosX = 0 + parseInt(document.getElementById("mod-header").offsetLeft);
                    TweenMax.set(document.getElementById("mod-sub-btn"), { right: (subPosX - 60) + 'px' });
                }
                TweenMax.to(document.getElementById("mod-sub-btn"), 0.3, { right: subPosX + 'px', opacity: 0.5, display: "block" });
                TweenMax.set($("#btn-sub-close"), { transformOrigin: "50% 50%", scale: 0 });
            }
            Draggable.create(document.getElementById('mod-subtitle'), {
                type: 'left,top',
                onDrag: function() {
                    if (this.target.offsetTop > (Config.maxSubtitleY - 10)) {
                        document.getElementById('mod-subtitle').style.top = (Config.maxSubtitleY - 10) + 'px';
                    }
                    if (this.target.offsetLeft > (Config.maxSubtitleX - 10)) {
                        document.getElementById('mod-subtitle').style.left = (Config.maxSubtitleX - 10) + 'px';
                    }
                    if (this.target.offsetTop < 90) {
                        document.getElementById('mod-subtitle').style.top = '80px';
                    }
                    if (this.target.offsetLeft < 10) {
                        document.getElementById('mod-subtitle').style.left = '0px';
                    }
                },
                onDragEnd: function() {
                    this.target.style.zIndex = 2;
                }
            });
            Draggable.get(document.getElementById('mod-subtitle')).disable();
        }
        break;
        case 1:
        case 3:
        {
            if (Config.subtitleType == 1) {
                var subPosX = 0;
                if (!Config.dynamicBackgroundColor) {
                    subPosX = 0 + parseInt(document.getElementById("mod-header").offsetLeft);
                    TweenMax.set(document.getElementById("mod-sub-btn"), { right: (subPosX - 60) + 'px' });
                }
                var subBtnY = 0 - (document.getElementById("mod-sub-btn").offsetHeight + 10);
                TweenMax.set(document.getElementById("mod-sub-btn"), { bottom: subBtnY + 'px' });
                TweenMax.to(document.getElementById("mod-sub-btn"), 0.3, { bottom: '0px', right: subPosX + 'px', opacity: 0.5, display: "block" });
                TweenMax.set($("#btn-sub-close"), { transformOrigin: "50% 50%", scale: 0 });
            }
        }
        break;
    }
    if (Config.openSubtitleAtStart) {
        Config.showSubtitleWindow = true;
        PopupManager.openSubtitleWindow();
        if (Config.subtitleType > 1) {
            var btnSubNav;
            switch (Config.navigationType) {
                case 0:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu" src="data/image/subtitle.png">';
                }
                break;
                case 1:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle.png">';
                }
                break;
                case 2:
                {
                    btnSubNav = document.getElementById('btn-subtitle');
                    btnSubNav.innerHTML = '<img src="data/image/subtitle.png" />';
                }
                break;
                case 3:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle.png">';
                }
                break;
            }
            btnSubNav.classList.remove('unused');
            btnSubNav.classList.add('used');
        }
    }
};
Main.showSubtitle = function() {
    if (Config.subtitleType > 1) {
        Main.showMenu();
    }
    if (Config.blockSubtitleBtn || Config.indexListOpen || Config.helpOpen || Config.glossaryOpen || Config.volumeOpen) return;
    if (Config.subtitleType == 0) {
        if (!Config.canOpenSubtitleWindow) return;
    }
    if (Config.showSubtitleWindow) {
        Config.showSubtitleWindow = false;
        PopupManager.closeSubtitleWindow();
        if (Config.subtitleType > 1) {
            var btnSubNav;
            switch (Config.navigationType) {
                case 0:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu" src="data/image/subtitle-off.png">';
                }
                break;
                case 1:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle-off.png">';
                }
                break;
                case 2:
                {
                    btnSubNav = document.getElementById('btn-subtitle');
                    btnSubNav.innerHTML = '<img src="data/image/subtitle-off.png" />';
                }
                break;
                case 3:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle-off.png">';
                }
                break;
            }
            btnSubNav.classList.remove('used');
            btnSubNav.classList.add('unused');
        }
    } else {
        Config.showSubtitleWindow = true;
        PopupManager.openSubtitleWindow();
        if (Config.subtitleType > 1) {
            var btnSubNav;
            switch (Config.navigationType) {
                case 0:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu" src="data/image/subtitle.png">';
                }
                break;
                case 1:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle.png">';
                }
                break;
                case 2:
                {
                    btnSubNav = document.getElementById('btn-subtitle');
                    btnSubNav.innerHTML = '<img src="data/image/subtitle.png" />';
                }
                break;
                case 3:
                {
                    btnSubNav = document.getElementById('btn-subtitle-nav-' + Config.navigationType);
                    btnSubNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/subtitle.png">';
                }
                break;
            }
            btnSubNav.classList.remove('unused');
            btnSubNav.classList.add('used');
        }
    }
};
Main.showPageNumber = function() {
    if (!Config.usePageNumber) return;
    document.getElementById('page-text').innerHTML = '1/' + Config.contentIds.length;
    document.getElementById('mod-page').style.display = 'block';
    LayoutManager.resettingPageNumber();
    TweenMax.to(document.getElementById('mod-page'), 0.3, { bottom: '0px' });
};
Main.setPageNumber = function() {
    document.getElementById('page-text').innerHTML = (Config.page + 1) + '/' + Config.contentIds.length;
};
Main.setModulTitle = function() {
    $("#modul-title").html("<b>" + Config.modulTitle + "</b>");
    setTimeout(function() {
        var boxH = document.getElementById('modul-title-box').offsetHeight;
        var txtH = document.getElementById('modul-title').offsetHeight;
        if (txtH < boxH) {
            document.getElementById('modul-title').style.marginTop = ((boxH - txtH) / 2) + 'px';
        } else {
            document.getElementById('modul-title').style.marginTop = '0px';
        }
    }, 110);
};
Main.setChapterTitle = function() {
    var c = Config.contentIds[Config.page].substr(4,2);
    $("#chapter-title").html(Config.chapterTitle[(c-1)]);
    setTimeout(function() {
        var boxH = document.getElementById('chapter-title-box').offsetHeight;
        var txtH = document.getElementById('chapter-title').offsetHeight;
        if (txtH < boxH) {
            document.getElementById('chapter-title').style.marginTop = ((boxH - txtH) / 2) + 'px';
        } else {
            document.getElementById('chapter-title').style.marginTop = '0px';
        }
    }, 110);
};
Main.checkIndexReady = function() {
    if (Config.useScreenWindow) {
        if (Config.hypeScreen == null) return;
    }
    if (Config.useHelpWindow) {
        if (Config.hypeHelp == null) return;
    }
    if (Config.useIndexListWindow) {
        if (Config.hypeIndexList == null) return;
    }
    if (Config.useGlossaryWindow) {
        if (Config.hypeGlossary == null) return;
    }
    if (Config.useVolumeWindow) {
        if (Config.hypeVolume == null) return;
    }
    clearInterval(Config.intervalFunction);
    Main.initModul();
};
Main.setIndexBaseDesign = function() {
    if (Config.logoBackgroundImage != "" && Config.logoBackgroundImage != " ") {
        document.getElementById("prev-bypass").style.background = "url('data/image/" + Config.logoBackgroundImage + ".png')";
        document.getElementById("prev-bypass").style.backgroundSize = "auto 100%";
        document.getElementById("prev-bypass").style.backgroundRepeat = "no-repeat";
        document.getElementById("prev-bypass").style.backgroundPosition = "left";
    }
    if (Config.titleBackgroundImage != "" && Config.titleBackgroundImage != " ") {
        document.getElementById("next-bypass").style.background = "url('data/image/" + Config.titleBackgroundImage + ".png')";
        document.getElementById("next-bypass").style.backgroundSize = "auto 100%";
        document.getElementById("next-bypass").style.backgroundRepeat = "no-repeat";
        document.getElementById("next-bypass").style.backgroundPosition = "right";
    }
    if (Config.headerBackgroundImage != "" && Config.headerBackgroundImage != " ") {
        document.getElementById("mod-header").style.background = "url('data/image/" + Config.headerBackgroundImage + ".png')";
        document.getElementById("mod-header").style.backgroundSize = "100% 100%";
        document.getElementById("mod-header").style.backgroundRepeat = "no-repeat";
        document.getElementById("mod-header").style.backgroundPosition = "left";
    }
    if (Config.headerBackgroundColor != "" && Config.headerBackgroundColor != " ") {
        document.getElementById('mod-header').style.backgroundColor = Config.headerBackgroundColor;
    }
    if (Config.modulTitleTextColor != "" && Config.modulTitleTextColor != " ") {
        document.getElementById("modul-title").style.color = Config.modulTitleTextColor;
    }
    if (Config.chapterTitleTextColor != "" && Config.chapterTitleTextColor != " ") {
        document.getElementById("chapter-title").style.color = Config.chapterTitleTextColor;
    }
    if (Config.usePageNumber) {
        if (Config.pageNumberBackgroundColor != "" && Config.pageNumberBackgroundColor != " ") {
            document.getElementById("mod-page").style.backgroundColor = Config.pageNumberBackgroundColor;
        }
        if (Config.pageNumberTextColor != "" && Config.pageNumberTextColor != " ") {
            document.getElementById("page-text").style.color = Config.pageNumberTextColor;
        }
    }
};
Main.setPreloaderColor = function() {
    var strTemp = "";
    switch (Config.preloaderType)
    {
        case 0:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object" id="object-' + Config.preloaderType + '"></div>';
            strTemp += '</div>';
        }
        break;
        case 1:
        case 6:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_one"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_two"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_three"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_four"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_five"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_six"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_seven"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_eight"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_big"></div>';
            strTemp += '</div>';
        }
        break;
        case 4:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_one"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_two"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_three"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_four"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_big"></div>';
            strTemp += '</div>';
        }
        break;
        case 10:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_one"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_two"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_three"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_four"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_five"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_six"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_seven"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_eight"></div>';
            strTemp += '</div>';
        }
        break;
        case 2:
        case 3:
        case 7:
        case 8:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_one"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_two"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_three"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_four"></div>';
            strTemp += '</div>';
        }
        break;
        case 5:
        case 11:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_one"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_two"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_three"></div>';
            strTemp += '</div>';
        }
        break;
        case 9:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_one"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_two" style="left:20px;"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_three" style="left:40px;"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_four" style="left:60px;"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_five" style="left:80px;"></div>';
            strTemp += '</div>';
        }
        break;
        case 12:
        case 13:
        {
            strTemp += '<div id="loading-center-absolute-' + Config.preloaderType + '">';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_four"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_one"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_two"></div>';
            strTemp += '<div class="loader-object object-' + Config.preloaderType + '" id="object_' + Config.preloaderType + '_three"></div>';
            strTemp += '</div>';
        }
        break;
    }
    document.getElementById('loading-center').innerHTML = strTemp;
    if (Config.preloaderBackgroundColor != "" || Config.preloaderBackgroundColor != " ") {
        Config.preloaderFrame.style.backgroundColor = Config.preloaderBackgroundColor;
    }
    if (Config.preloaderObjectColor != "" || Config.preloaderObjectColor != " ") {
        var preloaderObject = document.getElementsByClassName('loader-object');
        for (var i = 0; i < preloaderObject.length; i++) {
            preloaderObject[i].style.backgroundColor = 'transparent';
            preloaderObject[i].style.borderTopColor = 'transparent';
            preloaderObject[i].style.borderLeftColor = 'transparent';
            preloaderObject[i].style.borderRightColor = 'transparent';
        }
        if (Config.preloaderType < 12) {
            for (var j = 0; j < preloaderObject.length; j++) {
                preloaderObject[j].style.backgroundColor = Config.preloaderObjectColor;
            }
        } else {
            for (var j = 0; j < preloaderObject.length; j++) {
                if (Config.preloaderType == 12) {
                    preloaderObject[j].style.borderRightColor = Config.preloaderObjectColor;
                } else if (Config.preloaderType == 13) {
                    preloaderObject[j].style.borderTopColor = Config.preloaderObjectColor;
                }
                preloaderObject[j].style.borderLeftColor = Config.preloaderObjectColor;
            }
        }
    }
};
Main.initModul = function() {
    Main.showPreload();
    Config.firstLoad = true;
    Main.setFramesFirstTime();
    if (Config.navigationType == 2) {
        document.getElementById('title-box').style.paddingLeft = '30px';
    }
    Main.setIndexBaseDesign();
    Main.setModulTitle();
    Main.setNaviButton();
    Main.setContentControlerButton();
    if (Config.useSubtitle) {
        Main.setSubtitleDesign();
    }
    if (Config.useContentText) {
        ContentTextManager.setAllText();
    }
    if (Config.isScorm) {
        ScormManager.scromTracking();
    } else {
        if (Config.hypeScreen != null) {
            Main.hidePreload();
            PopupManager.screenShow();
        } else {
            if (Config.hypeHelp != null) {
                Main.hidePreload();
                PopupManager.popupShow('help');
            } else {
                if (Config.useBacksound) {
                    AudioManager.initBacksound();
                } else {
                    Main.checkLastPage();
                }
            }
        }
    }
    if (!Config.isLock) {
        if (document.getElementById('prev-bypass').addEventListener) {
            document.getElementById('prev-bypass').addEventListener("click", Main.prevContent);
        } else if (document.getElementById('prev-bypass').attachEvent) {
            document.getElementById('prev-bypass').attachEvent("onclick", Main.prevContent);
        }
        if (document.getElementById('next-bypass').addEventListener) {
            document.getElementById('next-bypass').addEventListener("click", Main.nextContent);
        } else if (document.getElementById('next-bypass').attachEvent) {
            document.getElementById('next-bypass').attachEvent("onclick", Main.nextContent);
        }
    }
    debugMsg("Initializing");
};
Main.checkLastPage = function() {
    var lp = 0;
    if (Config.lmsConnected) {
        for (var i = 0; i < Config.lessonLocation.length; i++) {
            if (Config.lessonLocation[i] == 0) {
                break;
            }
            lp += Config.lessonLocation[i];
        }
    }
    Config.page = lp;
    if (Config.page > Config.endPage) {
        Config.page = Config.endPage;
    }
    Config.lastPage = Config.page;
    Config.checkingLastPage = true;
    if (Config.lessonStatus == 'completed') {
        Config.page = Config.currentPage;
    } else {
        Config.currentPage = Config.page;
        Config.currentScene = Config.sceneLocation[Config.currentPage];
    }
    Main.showContentControllerButton();
    Main.showNavigation();
    if (Config.useSubtitle) {
        Main.initSubtitleBtn();
    }
    if (Config.usePageNumber) {
        Main.showPageNumber();
    }
    if (Config.useAudio) {
        AudioManager.initAudio();
    } else {
        Main.loadContent("", null);
    }
};
Main.loadContent = function(_strContent, _bNext) {
    Main.showPreload();
    if (_strContent != "") {
        for (var i = 0; i < Config.contentIds.length; i++) {
            if (Config.contentIds[i] == _strContent) {
                Config.page = i;
                break;
            }
        }
    }
    Config.currentPage = Config.page;
    if (_bNext == null) {
        Config.isNextContent = false;
        Config.content1Frame.src = "about:blank";
        Config.content2Frame.src = "about:blank";
        switch (Config.frameObject) {
            case 1:
            {
                Config.content1Frame.src = "content/modul/" + Config.contentIds[Config.page] + ".html";
            }
            break;
            case 2:
            {
                Config.content2Frame.src = "content/modul/" + Config.contentIds[Config.page] + ".html";
            }
            break;
        }
    } else if (_bNext == true) {
        Config.isNextContent = true;
        switch(Config.frameObject) {
            case 1:
            {
                TweenMax.set(Config.content2Frame, {display:"block"});
                switch(Config.slidingType) {
                    case 0:
                    case 1:
                    {
                        Config.content1Frame.style.zIndex = '0';
                        Config.content2Frame.style.zIndex = '-1';
                    }
                    break;
                    case 2:
                    case 3:
                    {
                        Config.content1Frame.style.zIndex = '-1';
                        Config.content2Frame.style.zIndex = '0';
                    }
                    break;
                }
                Config.content2Frame.src = "content/modul/" + Config.contentIds[Config.page] + ".html";
                Config.frameObject = 2;
            }
            break;
            case 2:
            {
                TweenMax.set(Config.content1Frame, {display:"block"});
                switch(Config.slidingType) {
                    case 0:
                    case 1:
                    {
                        Config.content1Frame.style.zIndex = '-1';
                        Config.content2Frame.style.zIndex = '0';
                    }
                    break;
                    case 2:
                    case 3:
                    {
                        Config.content1Frame.style.zIndex = '0';
                        Config.content2Frame.style.zIndex = '-1';
                    }
                    break;
                }
                Config.content1Frame.src = "content/modul/" + Config.contentIds[Config.page] + ".html";
                Config.frameObject = 1;
            }
            break;
        }
    } else if (_bNext == false) {
        Config.isNextContent = false;
        switch (Config.frameObject) {
            case 1:
            {
                TweenMax.set(Config.content2Frame, {display:"block"});
                switch(Config.slidingType) {
                    case 0:
                    case 1:
                    {
                        Config.content1Frame.style.zIndex = '-1';
                        Config.content2Frame.style.zIndex = '0';
                    }
                    break;
                    case 2:
                    case 3:
                    {
                        Config.content1Frame.style.zIndex = '0';
                        Config.content2Frame.style.zIndex = '-1';
                    }
                    break;
                }
                Config.content2Frame.src = "content/modul/" + Config.contentIds[Config.page] + ".html";
                Config.frameObject = 2;
            }
            break;
            case 2:
            {
                TweenMax.set(Config.content1Frame, {display:"block"});
                switch(Config.slidingType) {
                    case 0:
                    case 1:
                    {
                        Config.content1Frame.style.zIndex = '0';
                        Config.content2Frame.style.zIndex = '-1';
                    }
                    break;
                    case 2:
                    case 3:
                    {
                        Config.content1Frame.style.zIndex = '-1';
                        Config.content2Frame.style.zIndex = '0';
                    }
                    break;
                }
                Config.content1Frame.src = "content/modul/" + Config.contentIds[Config.page] + ".html";
                Config.frameObject = 1;
            }
            break;
        }
    }
    if (Config.usePageNumber) {
        Main.setPageNumber();
    }
    debugMsg("Load content without file name, content string : " + Config.contentIds[Config.page] + " content page : " + Config.page);
};
Main.checkContent = function() {
    Main.setChapterTitle();
    Config.hypeContent.setParentBGColor();
    if (Config.hypeContent.hypeContentChild != null && Config.hypeContent.hypeContentChild != undefined) {
        //Config.hypeContent.hypeContentChild.setParentBGColor();
    }
    if (Config.useContentText) {
        Config.hypeContent.setContentText(ContentTextManager.contentText[Config.page], Config.contentIds[Config.page]);  
        if (Config.hypeContent.hypeContentChild != null && Config.hypeContent.hypeContentChild != undefined) {
            Config.hypeContent.hypeContentChild.setContentText(ContentTextManager.contentText[AudioConfig.childPage], Config.contentIds[AudioConfig.childPage]);
        }
    }
    if (Config.useAudio) {
        if (Config.audio != null) {
            if (Config.layoutType == 0) {
                AudioManager.forceStopAudio();
            } else {
                if (Config.hypeContent.nextPrev) {
                    AudioManager.forceStopAudio();
                }
            }
        }
    }
};
Main.pauseContent = function() {
    if (Config.hypeContent == null) return;
    if (Config.contentPaused) return;
    var timelineName = "Main Timeline";
    var timelineKey, timelineInfo, i, videosObject;
    timelineKey = "";
    timelineKey = "timeline_" + Config.hypeContent.currentSceneName() + "_" + timelineName;
    timelineInfo = {};
    timelineInfo["time"] = Config.hypeContent.currentTimeInTimelineNamed(timelineName);
    timelineInfo["direction"] = Config.hypeContent.currentDirectionForTimelineNamed(timelineName);
    timelineInfo["state"] = Config.hypeContent.isPlayingTimelineNamed(timelineName);
    Config.contentAnim[timelineKey] = timelineInfo;
    Config.hypeContent.pauseTimelineNamed(timelineName);
    for (i = 0; i < Config.hypeContent.contentSymbol.length; i++) {
        timelineKey = "";
        timelineKey = "timeline_" + Config.hypeContent.contentSymbol[i].symbolName() + "_" + i + "_" + timelineName;
        timelineInfo = {};
        timelineInfo["time"] = Config.hypeContent.contentSymbol[i].currentTimeInTimelineNamed(timelineName);
        timelineInfo["direction"] = Config.hypeContent.contentSymbol[i].currentDirectionForTimelineNamed(timelineName);
        timelineInfo["state"] = Config.hypeContent.contentSymbol[i].isPlayingTimelineNamed(timelineName);
        Config.contentSymbolAnim[timelineKey] = timelineInfo;
        Config.hypeContent.contentSymbol[i].pauseTimelineNamed(timelineName);
    }
    for (i = 0; i < Config.hypeContent.contentVideo.length; i++) {
        videosObject = Config.hypeContent.contentVideo[i];
        Config.hypeContent.videoPaused[i] = false;
        if (videosObject.ended == false) {
            if (videosObject.paused == false) {
                videosObject.pause();
            }
        }
    }
    if (Config.hypeContent.hypeContentChild != null && Config.hypeContent.hypeContentChild != undefined) {
        var ctimelineName = "Main Timeline";
        var ctimelineKey, ctimelineInfo, ci;
        ctimelineKey = "";
        ctimelineKey = "timeline_" + Config.hypeContent.hypeContentChild.currentSceneName() + "_" + ctimelineName;
        ctimelineInfo = {};
        ctimelineInfo["time"] = Config.hypeContent.hypeContentChild.currentTimeInTimelineNamed(ctimelineName);
        ctimelineInfo["direction"] = Config.hypeContent.hypeContentChild.currentDirectionForTimelineNamed(ctimelineName);
        ctimelineInfo["state"] = Config.hypeContent.hypeContentChild.isPlayingTimelineNamed(ctimelineName);
        Config.contentChildAnim[ctimelineKey] = ctimelineInfo;
        Config.hypeContent.hypeContentChild.pauseTimelineNamed(ctimelineName);
        for (ci = 0; ci < Config.hypeContent.hypeContentChild.contentSymbol.length; ci++) {
            ctimelineKey = "";
            ctimelineKey = "timeline_" + Config.hypeContent.hypeContentChild.contentSymbol[ci].symbolName() + "_" + ci + "_" + ctimelineName;
            ctimelineInfo = {};
            ctimelineInfo["time"] = Config.hypeContent.hypeContentChild.contentSymbol[ci].currentTimeInTimelineNamed(ctimelineName);
            ctimelineInfo["direction"] = Config.hypeContent.hypeContentChild.contentSymbol[ci].currentDirectionForTimelineNamed(ctimelineName);
            ctimelineInfo["state"] = Config.hypeContent.hypeContentChild.contentSymbol[ci].isPlayingTimelineNamed(ctimelineName);
            Config.contentChildSymbolAnim[ctimelineKey] = ctimelineInfo;
            Config.hypeContent.hypeContentChild.contentSymbol[ci].pauseTimelineNamed(ctimelineName);
        }
    }
    if (Config.useAudio) {
        AudioManager.pauseAudio();
    }
    Config.contentPaused = true;
};
Main.resumeContent = function() {
    if (Config.hypeContent == null) return;
    if (!Config.contentPaused) return;
    var timelineName = "Main Timeline";
    var timelineKey, timelineInfo, i;
    timelineKey = "timeline_" + Config.hypeContent.currentSceneName() + "_" + timelineName;
    timelineInfo = Config.contentAnim[timelineKey];
    if (timelineInfo != null) {
        Config.hypeContent.goToTimeInTimelineNamed(timelineInfo["time"], timelineName);
        if (timelineInfo["state"] == true) {
            Config.hypeContent.continueTimelineNamed(timelineName, timelineInfo["direction"]);
        } else {
            Config.hypeContent.pauseTimelineNamed(timelineName);
        }
    }
    for (i = 0; i < Config.hypeContent.contentSymbol.length; i++) {
        timelineKey = "timeline_" + Config.hypeContent.contentSymbol[i].symbolName() + "_" + i + "_" + timelineName;
        timelineInfo = Config.contentSymbolAnim[timelineKey];
        if (timelineInfo != null) {
            Config.hypeContent.contentSymbol[i].goToTimeInTimelineNamed(timelineInfo["time"], timelineName);
            if (timelineInfo["state"] == true) {
                Config.hypeContent.contentSymbol[i].continueTimelineNamed(timelineName, timelineInfo["direction"]);
            } else {
                Config.hypeContent.contentSymbol[i].pauseTimelineNamed(timelineName);
            }
        }
    }
    /*for (i = 0; i < Config.hypeContent.contentVideo.length; i++) {
        videosObject = Config.hypeContent.contentVideo[i];
        if (videosObject.ended == false) {
            console.log(videosObject.paused);
            if (videosObject.paused == true) {
                console.log(Config.hypeContent.videoPaused[i]);
                if (Config.hypeContent.videoPaused[i] == false) {
                    if (videosObject.currentTime > 0) {
                        videosObject.play();
                    }
                }
            }
        }
    }*/
    if (Config.hypeContent.hypeContentChild != null && Config.hypeContent.hypeContentChild != undefined) {
        var ctimelineName = "Main Timeline";
        var ctimelineKey, ctimelineInfo, ci;
        ctimelineKey = "timeline_" + Config.hypeContent.hypeContentChild.currentSceneName() + "_" + ctimelineName;
        ctimelineInfo = Config.contentChildAnim[ctimelineKey];
        if (ctimelineInfo != null) {
            Config.hypeContent.hypeContentChild.goToTimeInTimelineNamed(ctimelineInfo["time"], ctimelineName);
            if (ctimelineInfo["state"] == true) {
                Config.hypeContent.hypeContentChild.continueTimelineNamed(ctimelineName, ctimelineInfo["direction"]);
            } else {
                Config.hypeContent.hypeContentChild.pauseTimelineNamed(ctimelineName);
            }
        }
        for (ci = 0; ci < Config.hypeContent.hypeContentChild.contentSymbol.length; ci++) {
            ctimelineKey = "timeline_" + Config.hypeContent.hypeContentChild.contentSymbol[ci].symbolName() + "_" + ci + "_" + ctimelineName;
            ctimelineInfo = Config.contentChildSymbolAnim[ctimelineKey];
            if (ctimelineInfo != null) {
                Config.hypeContent.hypeContentChild.contentSymbol[ci].goToTimeInTimelineNamed(ctimelineInfo["time"], ctimelineName);
                if (ctimelineInfo["state"] == true) {
                    Config.hypeContent.hypeContentChild.contentSymbol[ci].continueTimelineNamed(ctimelineName, ctimelineInfo["direction"]);
                } else {
                    Config.hypeContent.hypeContentChild.contentSymbol[ci].pauseTimelineNamed(ctimelineName);
                }
            }
        }
    }
    Config.contentAnim = {};
    Config.contentSymbolAnim = {};
    Config.contentChildAnim = {};
    Config.contentChildSymbolAnim = {};
    if (Config.useAudio) {
        AudioManager.resumeAudio();
    }
    Config.contentPaused = false;
};
Main.nextContent = function() {
    if (Config.nextState) return;
    Config.nextState = true;
    Config.lastPage = Config.page;
    Config.page++;
    if (Config.page > Config.endPage) {
        Config.page = Config.endPage;
        return;
    }
    if (Config.useAudio) {
        AudioManager.initAudio();
    } else {
        Main.loadContent("", true);
    }
};
Main.prevContent = function() {
    if (Config.prevState) return;
    Config.prevState = true;
    Config.lastPage = Config.page;
    Config.page--;
    if (Config.page < 0) {
        Config.page = 0;
        return;
    }
    if (Config.useAudio) {
        AudioManager.initAudio();
    } else {
        Main.loadContent("", false);
    }
};
Main.moveToContent = function(_strContent, _toScene, _toTimeline) {
    if (Config.jumpingState) return;
    if (_toScene != null && _toScene != undefined) {
        Config.moveToContentScene = _toScene;
    }
    if (_toTimeline != null && _toTimeline != undefined) {
        Config.moveToContentTimeline = _toTimeline;
    }
    Config.jumpingState = true;
    Config.lastPage = Config.page;
    for (var i = 0; i < Config.contentIds.length; i++) {
        if (Config.contentIds[i] == _strContent) {
            Config.page = i;
            break;
        }
    }
    if (Config.useAudio) {
        AudioManager.initAudio();
    } else {
        if (Config.lastPage < Config.page) {
            Main.loadContent("", true);
        } else {
            Main.loadContent("", false);
        }
    }
};
window.onload = function() {
    if (Config.useHelpWindow) {
        Config.helpFrame.src = "content/misc/help.html";
    }
    if (Config.useIndexListWindow) {
        Config.indexListFrame.src = "content/misc/indexlist.html";
    }
    if (Config.useGlossaryWindow) {
        Config.glossaryFrame.src = "content/misc/glossary.html";
    }
    if (Config.useVolumeWindow) {
        Config.volumeFrame.src = "content/misc/volume.html";
    }
    if (Config.useScreenWindow) {
        Config.screenFrame.src = "content/misc/screen.html";
    }
    Main.setPreloaderColor();
    Config.intervalFunction = null;
    Config.intervalFunction = setInterval(Main.checkIndexReady, 0.5);
};
$(window).on('resize', function() {
    TweenMax.delayedCall(0.1, LayoutManager.refreshFrame);
});
$(document).ready(function() {
    TweenMax.delayedCall(0.1, LayoutManager.refreshFrame);
});
//Browser state
ifvisible.on('blur', function() {
    if (Config.useBacksound) {
        AudioManager.pauseBacksound();
        AudioManager.pauseSoundFX();
    }
    if (!Config.pauseWithButton) {
        if (Config.useHelpWindow) {
            if (Config.helpOpen) return;
        }
        if (Config.useIndexListWindow) {
            if (Config.indexListOpen) return;
        }
        if (Config.useGlossaryWindow) {
            if (Config.glossaryOpen) return;
        }
        if (Config.useVolumeWindow) {
            if (Config.volumeOpen) return;
        }
        Main.pauseContent();
    }
});
ifvisible.on('focus', function() {
    if (Config.useBacksound) {
        AudioManager.resumeBacksound();
        AudioManager.resumeSoundFX();
    }
    if (!Config.pauseWithButton) {
        if (Config.useHelpWindow) {
            if (Config.helpOpen) return;
        }
        if (Config.useIndexListWindow) {
            if (Config.indexListOpen) return;
        }
        if (Config.useGlossaryWindow) {
            if (Config.glossaryOpen) return;
        }
        if (Config.useVolumeWindow) {
            if (Config.volumeOpen) return;
        }
        Main.resumeContent();
    }
});