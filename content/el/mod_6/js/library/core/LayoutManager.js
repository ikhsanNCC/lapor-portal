var LayoutManager = {
    fixWidth: 0,
    fixHeight: 0,
    mobileOrientation: 0,
};
LayoutManager.refreshFrame = function() {
    switch (Config.layoutType) {
        case 0:
        {
            LayoutManager.scalling();
        }
        break;
        case 1:
        {
            LayoutManager.scallingResponsive();
        }
        break;
    }
};
LayoutManager.scalling = function() {
    var wWidth, wHeight, wrapW, wrapH, headerW, headerH;
	Config.breakIdx = 0;
    if((md.mobile() != null) || (md.phone() != null) || (md.tablet() != null)) {
        wWidth = document.documentElement.clientWidth || document.body.clientWidth;
        wHeight = document.documentElement.clientHeight || document.body.clientHeight;
        if (LayoutManager.mobileOrientation != 1) {
            //Portrait
            if (LayoutManager.fixWidth != 0 && LayoutManager.fixWidth <= wWidth) {
                if (LayoutManager.fixHeight > wHeight) {
                    document.getElementById('mod-frames').style.overflowY = 'scroll';
                } else if (LayoutManager.fixHeight <= wHeight) {
                    document.getElementById('mod-frames').scrollTop = 0;
                    document.getElementById('mod-frames').style.overflowY = 'hidden';
                }
                return;
            }
            LayoutManager.mobileOrientation = 1;
            LayoutManager.fixWidth = wWidth;
            LayoutManager.fixHeight = wHeight;
        } else if (LayoutManager.mobileOrientation != 0) {
            //Landscape
            if (LayoutManager.fixWidth != 0 && LayoutManager.fixWidth >= wWidth) {
                if (LayoutManager.fixHeight > wHeight) {
                    document.getElementById('mod-frames').style.overflowY = 'scroll';
                } else if (LayoutManager.fixHeight <= wHeight) {
                    document.getElementById('mod-frames').scrollTop = 0;
                    document.getElementById('mod-frames').style.overflowY = 'hidden';
                }
                return;
            }
            LayoutManager.mobileOrientation = 0;
            LayoutManager.fixWidth = wWidth;
            LayoutManager.fixHeight = wHeight;
        }
    } else {
        wWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        wHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        LayoutManager.fixWidth = wWidth;
        LayoutManager.fixHeight = wHeight;
    }
    document.getElementById('mod-frames').style.width = LayoutManager.fixWidth + 'px';
    document.getElementById('mod-frames').style.height = LayoutManager.fixHeight + 'px';
    document.getElementById('mod-pdf').style.width = LayoutManager.fixWidth + 'px';
    document.getElementById('mod-pdf').style.height = LayoutManager.fixHeight + 'px';
    wrapH = LayoutManager.fixHeight;
    wrapW = (wrapH / 56.25) * 100;
    var squreScreen = false;
    var lessSqureScreen = false;
    if (LayoutManager.fixWidth < wrapW) {
        squreScreen = true;
    }
    if (squreScreen) {
        if ((LayoutManager.fixWidth / 78.125) * 100 < wrapW) {
            wrapW = (LayoutManager.fixWidth / 78.125) * 100;
            wrapH = (wrapW * 56.25) / 100;
            lessSqureScreen = true;
            var range = LayoutManager.fixHeight - wrapH;
            if (range >= 0 && range <= 60) {
                wrapH = LayoutManager.fixHeight;
                wrapW = (wrapH / 56.25) * 100;
            }
        }
    } else {
        var range = LayoutManager.fixWidth - wrapW;
        if (range >= 0 && range <= 60) {
            wrapW = LayoutManager.fixWidth;
            wrapH = (wrapW * 56.25) / 100;
        }
    }
    headerH = (wrapH * 13.888888888888889) / 100;
    if (headerH < 60) { headerH = 60; }
    else if (headerH > 80) { headerH = 80; }
    document.getElementById('mod-header').style.height = headerH + 'px';
    if (!Config.dynamicBackgroundColor) {
        if (lessSqureScreen || squreScreen) {
            headerW = LayoutManager.fixWidth;
            document.getElementById('mod-header').style.width = headerW + 'px';
            document.getElementById('mod-header').style.left = "0px";
        } else {
            headerW = wrapW;
            document.getElementById('mod-header').style.width = headerW + 'px';
            document.getElementById('mod-header').style.left = (0 - ((headerW - LayoutManager.fixWidth) / 2)) + "px";
        }
    }
    document.getElementById('mod-wrapper').style.width = wrapW + 'px';
    document.getElementById('mod-wrapper').style.height = wrapH + 'px';
    if (lessSqureScreen || squreScreen) {
        document.getElementById('mod-wrapper').style.top = (0 + ((LayoutManager.fixHeight - wrapH) / 2)) + 'px';
        document.getElementById('mod-wrapper').style.left = (0 - ((wrapW - LayoutManager.fixWidth) / 2)) + "px";
    } else {
        document.getElementById('mod-wrapper').style.top = (0 - ((wrapH - LayoutManager.fixHeight) / 2)) + 'px';
        document.getElementById('mod-wrapper').style.left = (0 + ((LayoutManager.fixWidth - wrapW) / 2)) + "px";
    }
    LayoutManager.resettingSubtitleLandscape(wrapW, wrapH, wWidth);
    document.getElementById('modul-title').style.fontSize = ((document.getElementById('mod-header').offsetHeight * Config.modulTitleFontRatio) / 100) + "px";
    setTimeout(function() {
        var mboxH = document.getElementById('modul-title-box').offsetHeight;
        var mtxtH = document.getElementById('modul-title').offsetHeight;
        if (mtxtH < mboxH) {
            document.getElementById('modul-title').style.marginTop = ((mboxH - mtxtH) / 2) + 'px';
        } else {
            document.getElementById('modul-title').style.margin = '0';
        }
    }, 110);
    document.getElementById('chapter-title').style.fontSize = ((document.getElementById('mod-header').offsetHeight * Config.chapterTitleFontRatio) / 100) + "px";
    setTimeout(function() {
        var cboxH = document.getElementById('chapter-title-box').offsetHeight;
        var ctxtH = document.getElementById('chapter-title').offsetHeight;
        if (ctxtH < cboxH) {
            document.getElementById('chapter-title').style.marginTop = ((cboxH - ctxtH) / 2) + 'px';
        } else {
            document.getElementById('chapter-title').style.margin = '0';
        }
    }, 110);
    if (Config.subtitleType == 1) {
        document.getElementById('mod-sub-btn').style.right = document.getElementById('mod-header').offsetLeft + 'px';
    }
    if (Config.useContentControl) {
        document.getElementById('mod-control').style.left = document.getElementById('mod-header').offsetLeft + 'px';
    }
    LayoutManager.resettingFrame();
    LayoutManager.resizePDFFrame();
    LayoutManager.resettingNavigation();
    if (Config.usePageNumber) {
        LayoutManager.resettingPageNumber();
    }
};
LayoutManager.scallingResponsive = function() {
	var wWidth, wHeight, wrapW, wrapH, headerW, headerH;
    if((md.mobile() != null) || (md.phone() != null) || (md.tablet() != null)) {
        wWidth = document.documentElement.clientWidth || document.body.clientWidth;
        wHeight = document.documentElement.clientHeight || document.body.clientHeight;
        if (wWidth < (wHeight + 50)) {
            //Portrait
            if (LayoutManager.fixWidth != 0 && LayoutManager.fixWidth <= wWidth) {
                if (LayoutManager.fixHeight > wHeight) {
                    document.getElementById('mod-frames').style.overflowY = 'scroll';
                } else if (LayoutManager.fixHeight <= wHeight) {
                    document.getElementById('mod-frames').scrollTop = 0;
                    document.getElementById('mod-frames').style.overflowY = 'hidden';
                }
                return;
            }
            if (Config.breakIdx != 1) {
                Config.breakIdx = 1;
                LayoutManager.fixWidth = wWidth;
                LayoutManager.fixHeight = wHeight;
            }
            document.getElementById('mod-frames').style.width = LayoutManager.fixWidth + 'px';
            document.getElementById('mod-frames').style.height = LayoutManager.fixHeight + 'px';
            document.getElementById('mod-pdf').style.width = LayoutManager.fixWidth + 'px';
            document.getElementById('mod-pdf').style.height = LayoutManager.fixHeight + 'px';
            wrapW = LayoutManager.fixWidth;
            wrapH = (wrapW / 56.25) * 100;
            headerH = (wrapH * 8) / 100;
            if (headerH < 60) { headerH = 60; }
            else if (headerH > 80) { headerH = 80; }
            document.getElementById('mod-header').style.height = headerH + 'px';
            if (!Config.dynamicBackgroundColor) {
                headerW = wrapW;
                document.getElementById('mod-header').style.width = headerW + 'px';
                document.getElementById('mod-header').style.left = "0px";
            }
            document.getElementById('mod-wrapper').style.width = wrapW + 'px';
            document.getElementById('mod-wrapper').style.height = wrapH + 'px';
            if (LayoutManager.fixHeight < wrapH) {
                document.getElementById('mod-wrapper').style.top = (0 - ((wrapH - LayoutManager.fixHeight) / 2)) + "px";
            } else {
                document.getElementById('mod-wrapper').style.top = (0 + ((LayoutManager.fixHeight - wrapH) / 2)) + "px";
            }
            document.getElementById('mod-wrapper').style.left = "0px";
            LayoutManager.resettingSubtitlePortrait(wrapW);
        } else if (wHeight < (wWidth + 50)) {
            //Landscape
            if (LayoutManager.fixWidth != 0 && LayoutManager.fixWidth >= wWidth) {
                if (LayoutManager.fixHeight > wHeight) {
                    document.getElementById('mod-frames').style.overflowY = 'scroll';
                } else if (LayoutManager.fixHeight <= wHeight) {
                    document.getElementById('mod-frames').scrollTop = 0;
                    document.getElementById('mod-frames').style.overflowY = 'hidden';
                }
                return;
            }
            if (Config.breakIdx != 0) {
                Config.breakIdx = 0;
                LayoutManager.fixWidth = wWidth;
                LayoutManager.fixHeight = wHeight;
            }
            document.getElementById('mod-frames').style.width = LayoutManager.fixWidth + 'px';
            document.getElementById('mod-frames').style.height = LayoutManager.fixHeight + 'px';
            document.getElementById('mod-pdf').style.width = LayoutManager.fixWidth + 'px';
            document.getElementById('mod-pdf').style.height = LayoutManager.fixHeight + 'px';
            wrapH = LayoutManager.fixHeight;
            wrapW = (wrapH / 56.25) * 100;
            headerH = (wrapH * 13.888888888888889) / 100;
            if (headerH < 60) { headerH = 60; }
            else if (headerH > 80) { headerH = 80; }
            document.getElementById('mod-header').style.height = headerH + 'px';
            if (!Config.dynamicBackgroundColor) {
                if (LayoutManager.fixWidth < wrapW) {
                    headerW = LayoutManager.fixWidth;
                    document.getElementById('mod-header').style.width = headerW + 'px';
                    document.getElementById('mod-header').style.left = "0px";
                } else {
                    headerW = wrapW;
                    document.getElementById('mod-header').style.width = headerW + 'px';
                    document.getElementById('mod-header').style.left = (0 - ((headerW - LayoutManager.fixWidth) / 2)) + "px";
                }
            }
            document.getElementById('mod-wrapper').style.width = wrapW + 'px';
            document.getElementById('mod-wrapper').style.height = wrapH + 'px';
            if (LayoutManager.fixWidth < wrapW) {
                document.getElementById('mod-wrapper').style.left = (0 - ((wrapW - LayoutManager.fixWidth) / 2)) + "px";
            } else {
                document.getElementById('mod-wrapper').style.left = (0 + ((LayoutManager.fixWidth - wrapW) / 2)) + "px";
            }
            document.getElementById('mod-wrapper').style.top = "0px";
            LayoutManager.resettingSubtitleLandscape(wrapW, wrapH, wWidth);
        }
    } else {
        wWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		wHeight = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        Config.breakIdx = 0;
        document.getElementById('mod-frames').style.width = wWidth + 'px';
        document.getElementById('mod-frames').style.height = wHeight + 'px';
        document.getElementById('mod-pdf').style.width = wWidth + 'px';
        document.getElementById('mod-pdf').style.height = wHeight + 'px';
        if (Config.debugLayout) {
            if (wWidth < (wHeight + 50)) {
                //Portrait
                Config.breakIdx = 1;
                LayoutManager.fixWidth = wWidth;
                LayoutManager.fixHeight = wHeight;
                wrapW = wWidth;
                wrapH = (wrapW / 56.25) * 100;
                headerH = (wrapH * 8) / 100;
                if (headerH < 60) { headerH = 60; }
                else if (headerH > 80) { headerH = 80; }
                document.getElementById('mod-header').style.height = headerH + 'px';
                if (!Config.dynamicBackgroundColor) {
                    headerW = wWidth;
                    document.getElementById('mod-header').style.width = headerW + 'px';
                    document.getElementById('mod-header').style.left = "0px";
                }
                document.getElementById('mod-wrapper').style.width = wrapW + 'px';
                document.getElementById('mod-wrapper').style.height = wrapH + 'px';
                if (wHeight < wrapH) {
                    document.getElementById('mod-wrapper').style.top = (0 - ((wrapH - wHeight) / 2)) + "px";
                } else {
                    document.getElementById('mod-wrapper').style.top = (0 + ((wHeight - wrapH) / 2)) + "px";
                }
                document.getElementById('mod-wrapper').style.left = "0px";
                LayoutManager.resettingSubtitlePortrait(wrapW);
            } else if (wHeight < (wWidth + 50)) {
                //Landscape
                Config.breakIdx = 0;
                LayoutManager.fixWidth = wWidth;
                LayoutManager.fixHeight = wHeight;
                wrapH = wHeight;
                wrapW = (wrapH / 56.25) * 100;
                headerH = (wrapH * 13.888888888888889) / 100;
                if (headerH < 60) { headerH = 60; }
                else if (headerH > 80) { headerH = 80; }
                document.getElementById('mod-header').style.height = headerH + 'px';
                if (!Config.dynamicBackgroundColor) {
                    if (wWidth < wrapW) {
                        headerW = wWidth;
                        document.getElementById('mod-header').style.width = headerW + 'px';
                        document.getElementById('mod-header').style.left = "0px";
                    } else {
                        headerW = wrapW;
                        document.getElementById('mod-header').style.width = headerW + 'px';
                        document.getElementById('mod-header').style.left = (0 - ((headerW - wWidth) / 2)) + "px";
                    }
                }
                document.getElementById('mod-wrapper').style.width = wrapW + 'px';
                document.getElementById('mod-wrapper').style.height = wrapH + 'px';
                if (wWidth < wrapW) {
                    document.getElementById('mod-wrapper').style.left = (0 - ((wrapW - wWidth) / 2)) + "px";
                } else {
                    document.getElementById('mod-wrapper').style.left = (0 + ((wWidth - wrapW) / 2)) + "px";
                }
                document.getElementById('mod-wrapper').style.top = "0px";
                LayoutManager.resettingSubtitleLandscape(wrapW, wrapH, wWidth);
            }
        } else {
            LayoutManager.fixWidth = wWidth;
            LayoutManager.fixHeight = wHeight;
            wrapH = LayoutManager.fixHeight;
            wrapW = (wrapH / 56.25) * 100;
            headerH = (wrapH * 13.888888888888889) / 100;
            if (headerH < 60) { headerH = 60; }
            else if (headerH > 80) { headerH = 80; }
            document.getElementById('mod-header').style.height = headerH + 'px';
            if (!Config.dynamicBackgroundColor) {
                if (wWidth < wrapW) {
                    headerW = wWidth;
                    document.getElementById('mod-header').style.width = headerW + 'px';
                    document.getElementById('mod-header').style.left = "0px";
                } else {
                    headerW = wrapW;
                    document.getElementById('mod-header').style.width = headerW + 'px';
                    document.getElementById('mod-header').style.left = (0 - ((headerW - wWidth) / 2)) + "px";
                }
            }
            document.getElementById('mod-wrapper').style.width = wrapW + 'px';
            document.getElementById('mod-wrapper').style.height = wrapH + 'px';
            if (wWidth < wrapW) {
                document.getElementById('mod-wrapper').style.left = (0 - ((wrapW - wWidth) / 2)) + "px";
            } else {
                document.getElementById('mod-wrapper').style.left = (0 + ((wWidth - wrapW) / 2)) + "px";
            }
            document.getElementById('mod-wrapper').style.top = "0px";
            LayoutManager.resettingSubtitleLandscape(wrapW, wrapH, wWidth);
        }
    }
    document.getElementById('modul-title').style.fontSize = ((document.getElementById('mod-header').offsetHeight * Config.modulTitleFontRatio) / 100) + "px";
    setTimeout(function() {
        var mboxH = document.getElementById('modul-title-box').offsetHeight;
        var mtxtH = document.getElementById('modul-title').offsetHeight;
        if (mtxtH < mboxH) {
            document.getElementById('modul-title').style.marginTop = ((mboxH - mtxtH) / 2) + 'px';
        } else {
            document.getElementById('modul-title').style.margin = '0';
        }
    }, 110);
    document.getElementById('chapter-title').style.fontSize = ((document.getElementById('mod-header').offsetHeight * Config.chapterTitleFontRatio) / 100) + "px";
    setTimeout(function() {
        var cboxH = document.getElementById('chapter-title-box').offsetHeight;
        var ctxtH = document.getElementById('chapter-title').offsetHeight;
        if (ctxtH < cboxH) {
            document.getElementById('chapter-title').style.marginTop = ((cboxH - ctxtH) / 2) + 'px';
        } else {
            document.getElementById('chapter-title').style.margin = '0';
        }
    }, 110);
    if (Config.subtitleType == 1) {
        document.getElementById('mod-sub-btn').style.right = document.getElementById('mod-header').offsetLeft + 'px';
    }
    if (Config.useContentControl) {
        document.getElementById('mod-control').style.left = document.getElementById('mod-header').offsetLeft + 'px';
    }
    LayoutManager.resettingFrame();
    LayoutManager.resizePDFFrame();
    LayoutManager.resettingNavigation();
    if (Config.usePageNumber) {
        LayoutManager.resettingPageNumber();
    }
};
LayoutManager.resettingFrame = function() {
    switch(Config.slidingType) {
        case 0:
        {
            var cW = document.getElementById('mod-wrapper').offsetWidth;
            switch(Config.frameObject) {
                case 1:
                {
                    Config.content1Frame.style.left = "0px";
                    if (Config.content2Frame.offsetLeft < 0) {
                        Config.content2Frame.style.left = "-" + cW + "px";
                    } else {
                        Config.content2Frame.style.left = cW + "px";
                    }
                }
                break;
                case 2:
                {
                    if (Config.content1Frame.offsetLeft < 0) {
                        Config.content1Frame.style.left = "-" + cW + "px";
                    } else {
                        Config.content1Frame.style.left = cW + "px";
                    }
                    Config.content2Frame.style.left = "0px";
                }
                break;
            }
        }
        break;
        case 1:
        {
            var cH = document.getElementById('mod-wrapper').offsetHeight;
            switch(Config.frameObject) {
                case 1:
                {
                    Config.content1Frame.style.top = "0px";
                    if (Config.content2Frame.offsetTop < 0) {
                        Config.content2Frame.style.top = "-" + cH + "px";
                    } else {
                        Config.content2Frame.style.top = cH + "px";
                    }
                }
                break;
                case 2:
                {
                    if (Config.content1Frame.offsetTop < 0) {
                        Config.content1Frame.style.top = "-" + cH + "px";
                    } else {
                        Config.content1Frame.style.top = cH + "px";
                    }
                    Config.content2Frame.style.top = "0px";
                }
                break;
            }
        }
        break;
    }
    //Init Help Frame
    switch(Config.helpType) {
        case 1:
        {
            var hW = Config.helpFrame.offsetWidth;
            var finalL = 0 - (hW - (hW * 2)) - 50;
            Config.helpFrame.style.left = finalL + "px";
        }
        break;
        case 2:
        {
            var fW = document.getElementById('mod-frames').offsetWidth;
            var hW = Config.helpFrame.offsetWidth;
            var finalL = fW + (hW + 50);
            Config.helpFrame.style.left = finalL + "px";
        }
        break;
    }
    //Init Index List Frame
    switch(Config.indexListType) {
        case 1:
        {
            var iW = Config.indexListFrame.offsetWidth;
            var finalL = 0 - (iW - (iW * 2)) - 50;
            Config.indexListFrame.style.left = finalL + "px";
        }
        break;
        case 2:
        {
            var fW = document.getElementById('mod-frames').offsetWidth;
            var iW = Config.indexListFrame.offsetWidth;
            var finalL = fW + (iW + 50);
            Config.indexListFrame.style.left = finalL + "px";
        }
        break;
    }
    //Init Glossary Frame
    switch(Config.glossaryType) {
       case 1:
        {
            var gW = Config.glossaryFrame.offsetWidth;
            var finalL = 0 - (gW - (gW * 2)) - 50;
            Config.glossaryFrame.style.left = finalL + "px";
        }
        break;
        case 2:
        {
            var fW = document.getElementById('mod-frames').offsetWidth;
            var gW = Config.glossaryFrame.offsetWidth;
            var finalL = fW + (gW + 50);
            Config.glossaryFrame.style.left = finalL + "px";
        }
        break;
    }
};
LayoutManager.resizePDFFrame = function() {
    var pH = document.getElementById('mod-pdf').offsetHeight;
    var pFH = pH - 45;
    document.getElementById('pdf-frame-body').style.height = pFH + 'px';
};
LayoutManager.resettingNavigation = function() {
    switch (Config.navigationType) {
        case 0:
        {
            if (Config.useContentControl) {
                document.getElementById('mod-navi').style.left = (document.getElementById('mod-header').offsetLeft + 60) + 'px';
            } else {
                document.getElementById('mod-navi').style.left = document.getElementById('mod-header').offsetLeft + 'px';
            }
        }
        break;
        case 1:
        {
            document.getElementById('mod-navi').style.paddingTop = document.getElementById('mod-header').offsetHeight + 'px';
            if (!Config.dynamicBackgroundColor) {
                if (Config.menuOpen) {
                    document.getElementById('mod-navi').style.left = document.getElementById("mod-header").offsetLeft + "px";
                } else {
                    document.getElementById('mod-navi').style.left = ((document.getElementById("mod-header").offsetLeft) - 250) + "px";
                }
            }
        }
        break;
        case 2:
        {
            var frameW = document.getElementById('mod-frames').offsetWidth;
            var leftN = (frameW / 2) - (80 / 2);
            if (document.getElementsByClassName('nav-type-2')[0] != null && document.getElementsByClassName('nav-type-2')[0] != undefined) {
                document.getElementsByClassName('nav-type-2')[0].style.left = leftN + "px";
            }
        }
        break;
        case 3:
        {
            var headerH = document.getElementById('mod-header').offsetHeight;
            var generalW = 0;
            var btnBoxNav = document.getElementsByClassName('btn-box-' + Config.navigationType);
            var boxIcoMenuNav = document.getElementsByClassName('box-ico-menu-' + Config.navigationType);
            var icoMenuNav = document.getElementsByClassName('ico-menu-' + Config.navigationType);
            var i = 0;
            if (document.getElementById('btn-menu') != null && document.getElementById('btn-menu') != undefined) {
                document.getElementById('btn-menu').style.height = headerH + 'px';
            }
            generalW = (headerH / 80) * 60;
            for (i = 0; i < btnBoxNav.length; i++) {
                btnBoxNav[i].style.width = generalW + "px";
                btnBoxNav[i].style.height = generalW + "px";
            }
            for (i = 0; i < boxIcoMenuNav.length; i++) {
                boxIcoMenuNav[i].style.width = generalW + "px";
            }
            for (i = 0; i < icoMenuNav.length; i++) {
                icoMenuNav[i].style.width = generalW + "px";
            }
            if (Config.menuOpen) {
                var totalH = parseInt(headerH) + (60 * Config.columnNavigation);
                document.getElementById('mod-navi').style.height = totalH + 'px';
                document.getElementById("box-menu-" + Config.navigationType).style.height = ((generalW + 10) * (Config.columnNavigation - 1)) + 'px';
                for (var i = 0; i < (Config.columnNavigation - 1); i++) {
                    document.getElementById("btn-menu-" + Config.navigationType + "-" + i).style.top = (((generalW + 5) * i) + 5) + "px";
                }
            } else {
                document.getElementById('mod-navi').style.height = headerH + 'px';
                if (document.getElementById("box-menu-" + Config.navigationType) != null && document.getElementById("box-menu-" + Config.navigationType) != undefined) {
                    document.getElementById("box-menu-" + Config.navigationType).style.height = '0px';
                }
                for (var i = 0; i < (Config.columnNavigation - 1); i++) {
                    document.getElementById("btn-menu-" + Config.navigationType + "-" + i).style.top = "-" + (generalW + 10) + "px";
                }
            }
        }
        break;
    }
};
LayoutManager.resettingSubtitlePortrait = function(_wrapW) {
    switch (Config.subtitleType) {
        case 0:
        case 2:
        {
            var subW = (_wrapW * 95) / 100;
            var subH = (subW * 26.666666666666667) / 100;
            document.getElementById('mod-subtitle').style.width = subW + "px";
            document.getElementById('mod-subtitle').style.height = subH + "px";
            document.getElementById('box-subtitle').style.width = (subW - 10) + 'px';
            document.getElementById('box-subtitle').style.height = (subH - 10) + 'px';
            Config.maxSubtitleX = document.getElementById('mod-frames').offsetWidth - subW;
            Config.maxSubtitleY = document.getElementById('mod-frames').offsetHeight - subH;
            if ($('#btn-subtxt-con').css('display') == 'block') {
                document.getElementById('subTxt').style.width = (document.getElementById('box-subtitle').offsetWidth - 30) + 'px';
                document.getElementById('subTxt').style.top = '0px';
            } else {
                document.getElementById('subTxt').style.width = '100%';
                var fPosY = (document.getElementById('box-subtitle').offsetHeight / 2) - (document.getElementById('subTxt').offsetHeight / 2);
                document.getElementById('subTxt').style.top = fPosY + 'px';
            }
            document.getElementById('subTxt').style.fontSize = ((subH * Config.subtitleFontRatio) / 100) + "px";
            if (Config.showSubtitleWindow) {
                var posY = (document.getElementById('mod-frames').offsetHeight - document.getElementById('mod-subtitle').offsetHeight);
                if ((subW + 150) > LayoutManager.fixWidth) {
                    posY = posY - 70;
                } else {
                    posY = posY - 10;
                }
                document.getElementById('mod-subtitle').style.top = posY + "px";
                posX = (document.getElementById('mod-frames').offsetWidth / 2) - (document.getElementById('mod-subtitle').offsetWidth / 2);
                document.getElementById('mod-subtitle').style.left = posX + "px";
            }
            var btnPosX = 0;
            if (!Config.dynamicBackgroundColor) {
                btnPosX = 0 + parseInt(document.getElementById("mod-header").offsetLeft);
            }
            if (Config.subtitleType == 0) {
                TweenMax.set(document.getElementById("mod-sub-btn"), { right: btnPosX + 'px' });
            }
        }
        break;
        case 1:
        case 3:
        {
            var subW = LayoutManager.fixWidth;
            document.getElementById('mod-subtitle').style.width = subW + "px";
            if (!Config.dynamicBackgroundColor) {
                document.getElementById('mod-subtitle').style.width = document.getElementById('mod-header').offsetWidth + 'px';
                document.getElementById('mod-subtitle').style.left = document.getElementById('mod-header').offsetLeft + 'px';
            }
            Config.maxSubtitleX = 0;
            Config.maxSubtitleY = 0;
            var btnPosX = 0;
            if (!Config.dynamicBackgroundColor) {
                btnPosX = 0 + parseInt(document.getElementById("mod-header").offsetLeft);
            }
            if (Config.subtitleType == 1) {
                TweenMax.set(document.getElementById("mod-sub-btn"), { right: btnPosX + 'px' });
            }
            var btnSubY = 0;
            if (Config.showSubtitleWindow) {
                document.getElementById('mod-subtitle').style.bottom = '0px';
                btnSubY = document.getElementById('mod-subtitle').offsetHeight;
            }
            if (Config.navigationType == 0) {
                document.getElementById('mod-navi').style.bottom = btnSubY + 'px';
            }
            if (Config.subtitleType == 1) {
                document.getElementById('mod-sub-btn').style.bottom = btnSubY + 'px';
            }
            if (Config.usePageNumber) {
                document.getElementById('mod-page').style.bottom = btnSubY + 'px';
            }
            if (Config.useContentControl) {
                document.getElementById('mod-control').style.bottom = btnSubY + 'px';
            }
            document.getElementById('subTxt').style.fontSize = ((document.getElementById('mod-header').offsetHeight * Config.subtitleFontRatio) / 100) + "px";
        }
        break;
    }
};
LayoutManager.resettingSubtitleLandscape = function(_wrapW, _wrapH, _wWidth) {
    switch (Config.subtitleType) {
        case 0:
        case 2:
        {
            var subW;
            if (Config.layoutType == 0) {
                if (_wrapH >= LayoutManager.fixHeight) {
                    subW = (_wrapW * 58.59375) / 100;
                } else {
                    subW = (LayoutManager.fixWidth * 95) / 100;
                }
            } else if (Config.layoutType == 1) {
                subW = (_wrapW * 58.59375) / 100;
            }
            var subH = (subW * 26.666666666666667) / 100;
            document.getElementById('mod-subtitle').style.width = subW + "px";
            document.getElementById('mod-subtitle').style.height = subH + "px";
            document.getElementById('box-subtitle').style.width = (subW - 10) + 'px';
            document.getElementById('box-subtitle').style.height = (subH - 10) + 'px';
            Config.maxSubtitleX = document.getElementById('mod-frames').offsetWidth - subW;
            Config.maxSubtitleY = document.getElementById('mod-frames').offsetHeight - subH;
            if ($('#btn-subtxt-con').css('display') == 'block') {
                document.getElementById('subTxt').style.width = (document.getElementById('box-subtitle').offsetWidth - 30) + 'px';
                document.getElementById('subTxt').style.top = '0px';
            } else {
                document.getElementById('subTxt').style.width = '100%';
                var fPosY = (document.getElementById('box-subtitle').offsetHeight / 2) - (document.getElementById('subTxt').offsetHeight / 2);
                document.getElementById('subTxt').style.top = fPosY + 'px';
            }
            document.getElementById('subTxt').style.fontSize = ((subH * Config.subtitleFontRatio) / 100) + "px";
            if (Config.showSubtitleWindow) {
                var posY = parseInt(document.getElementById('mod-frames').offsetHeight - document.getElementById('mod-subtitle').offsetHeight);
                if ((subW + 150) > LayoutManager.fixWidth) {
                    posY = posY - 70;
                } else {
                    posY = posY - 10;
                }
                document.getElementById('mod-subtitle').style.top = posY + "px";
                posX = parseInt(document.getElementById('mod-frames').offsetWidth / 2) - parseInt(document.getElementById('mod-subtitle').offsetWidth / 2);
                document.getElementById('mod-subtitle').style.left = posX + "px";
            }
            var btnPosX = 0;
            if (!Config.dynamicBackgroundColor) {
                btnPosX = 0 + parseInt(document.getElementById("mod-header").offsetLeft);
            }
            if (Config.subtitleType == 0) {
                TweenMax.set(document.getElementById("mod-sub-btn"), { right: btnPosX + 'px' });
            }
        }
        break;
        case 1:
        case 3:
        {
            var subW;
            if (Config.layoutType == 0) {
                subW = LayoutManager.fixWidth;
            }
            else if (Config.layoutType == 1) {
                subW = _wWidth;
            }
            document.getElementById('mod-subtitle').style.width = subW + "px";
            if (!Config.dynamicBackgroundColor) {
                document.getElementById('mod-subtitle').style.width = document.getElementById('mod-header').offsetWidth + 'px';
                document.getElementById('mod-subtitle').style.left = document.getElementById('mod-header').offsetLeft + 'px';
            }
            Config.maxSubtitleX = 0;
            Config.maxSubtitleY = 0;
            var btnPosX = 0;
            if (!Config.dynamicBackgroundColor) {
                btnPosX = 0 + parseInt(document.getElementById("mod-header").offsetLeft);
            }
            if (Config.subtitleType == 1) {
                TweenMax.set(document.getElementById("mod-sub-btn"), { right: btnPosX + 'px' });
            }
            var btnSubY = 0;
            if (Config.showSubtitleWindow) {
                document.getElementById('mod-subtitle').style.bottom = '0px';
                btnSubY = document.getElementById('mod-subtitle').offsetHeight;
            }
            if (Config.navigationType == 0) {
                document.getElementById('mod-navi').style.bottom = btnSubY + 'px';
            }
            if (Config.subtitleType == 1) {
                document.getElementById('mod-sub-btn').style.bottom = btnSubY + 'px';
            }
            if (Config.usePageNumber) {
                document.getElementById('mod-page').style.bottom = btnSubY + 'px';
            }
            if (Config.useContentControl) {
                document.getElementById('mod-control').style.bottom = btnSubY + 'px';
            }
            document.getElementById('subTxt').style.fontSize = ((document.getElementById('mod-header').offsetHeight * Config.subtitleFontRatio) / 100) + "px";
        }
        break;
    }
};
LayoutManager.resettingPageNumber = function() {
    var frameW = document.getElementById('mod-frames').offsetWidth;
    var pageW = 62;
    if (document.getElementById('mod-page') != null && document.getElementById('mod-page') != undefined) {
        pageW = document.getElementById('mod-page').offsetWidth;
    }
    var leftN = (frameW / 2) - (pageW / 2);
    if (document.getElementById('mod-page') != null && document.getElementById('mod-page') != undefined) {
        document.getElementById('mod-page').style.left = leftN + "px";
    }
};