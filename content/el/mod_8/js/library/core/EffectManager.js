var EffectManager = {
    effectMenuStarting: false,
};
EffectManager.horizontalSlideEff = function() {
    var maxcW = document.getElementById('mod-wrapper').offsetWidth;
    var mincW = 0 - maxcW;
	if(Config.isNextContent == true) {
		if(Config.frameObject == 2) {
            TweenMax.to(Config.content1Frame, 1.1, {left: mincW, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content1Frame.src = "about:blank"; }});
            TweenMax.set(Config.content2Frame, {left: maxcW});
            TweenMax.to(Config.content2Frame, 1.1, {left: 0, ease:Power1.easeInOut});
        } else if (Config.frameObject == 1) {
            TweenMax.set(Config.content1Frame, {left: maxcW});
            TweenMax.to(Config.content1Frame, 1.1, {left: 0, ease:Power1.easeInOut});
            TweenMax.to(Config.content2Frame, 1.1, {left: mincW, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content2Frame.src = "about:blank"; }});
        }
	} else {
		if (Config.frameObject == 1) {
            TweenMax.set(Config.content1Frame, {left: mincW});
            TweenMax.to(Config.content1Frame, 1.1, {left: 0, ease:Power1.easeInOut});
            TweenMax.to(Config.content2Frame, 1.1, {left: maxcW, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content2Frame.src = "about:blank"; }});
        } else if (Config.frameObject == 2) {
            TweenMax.to(Config.content1Frame, 1.1, {left: maxcW, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content1Frame.src = "about:blank"; }});
            TweenMax.set(Config.content2Frame, {left: mincW});
            TweenMax.to(Config.content2Frame, 1.1, {left: 0, ease:Power1.easeInOut});
        }
	}
    debugMsg("sliding effect done");
};
EffectManager.verticalSlideEff = function() {
    var maxcH = document.getElementById('mod-wrapper').offsetHeight;
    var mincH = 0 - maxcH;
    if(Config.isNextContent == true) {
		if(Config.frameObject == 2) {
            TweenMax.to(Config.content1Frame, 1.1, {top: mincH, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content1Frame.src = "about:blank"; }});
            TweenMax.set(Config.content2Frame, {top: maxcH});
            TweenMax.to(Config.content2Frame, 1.1, {top: 0, ease:Power1.easeInOut});
        } else if (Config.frameObject == 1) {
            TweenMax.set(Config.content1Frame, {top: maxcH});
            TweenMax.to(Config.content1Frame, 1.1, {top: 0, ease:Power1.easeInOut});
            TweenMax.to(Config.content2Frame, 1.1, {top: mincH, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content2Frame.src = "about:blank"; }});
        }
	} else {
		if (Config.frameObject == 1) {
            TweenMax.set(Config.content1Frame, {top: mincH});
            TweenMax.to(Config.content1Frame, 1.1, {top: 0, ease:Power1.easeInOut});
            TweenMax.to(Config.content2Frame, 1.1, {top: maxcH, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content2Frame.src = "about:blank"; }});
        } else if (Config.frameObject == 2) {
            TweenMax.to(Config.content1Frame, 1.1, {top: maxcH, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content1Frame.src = "about:blank"; }});
            TweenMax.set(Config.content2Frame, {top: mincH});
            TweenMax.to(Config.content2Frame, 1.1, {top: 0, ease:Power1.easeInOut});
        }
	}
    debugMsg("sliding effect done");
};
EffectManager.fadingSlideEff = function() {
    if (Config.frameObject == 1) {
        TweenMax.to(Config.content1Frame, 1.1, {opacity: 1, ease:Power1.easeInOut});
        TweenMax.to(Config.content2Frame, 1.1, {opacity: 0, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content2Frame.src = "about:blank"; }});
    } else if (Config.frameObject == 2) {
        TweenMax.to(Config.content1Frame, 1.1, {opacity: 0, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content1Frame.src = "about:blank"; }});
        TweenMax.to(Config.content2Frame, 1.1, {opacity: 1, ease:Power1.easeInOut});
    }
    debugMsg("sliding effect done");
};
EffectManager.zoomingSlideEff = function() {
    if (Config.frameObject == 1) {
        TweenMax.to(Config.content1Frame, 1.1, {transformOrigin:"50% 50%", scale: 1, opacity: 1, ease:Power1.easeInOut});
        TweenMax.to(Config.content2Frame, 1.1, {transformOrigin:"50% 50%", scale: 1.2, opacity: 0, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content2Frame.src = "about:blank"; }});
    } else if (Config.frameObject == 2) {
        TweenMax.to(Config.content1Frame, 1.1, {transformOrigin:"50% 50%", scale: 1.2, opacity: 0, display: 'none', ease:Power1.easeInOut, onComplete: function() { Config.content1Frame.src = "about:blank"; }});
        TweenMax.to(Config.content2Frame, 1.1, {transformOrigin:"50% 50%", scale: 1, opacity: 1, ease:Power1.easeInOut});
    }
    debugMsg("sliding effect done");
};
EffectManager.changeBGColor = function(_color) {
    if (!Config.dynamicBackgroundColor) return;
    if (_color != "") {
        TweenMax.to(document.getElementsByTagName("BODY")[0], 1.1, { backgroundColor: _color, ease:Power1.easeInOut });
    }
};
EffectManager.popupSlideEff = function(_type) {
    var popupOpened = false;
    var popupType = -1;
    var popupFrame = null;
    var popupHype = null;
    if (_type == null || _type == undefined) return;
    switch(_type) {
        case "help":
        {
            popupOpened = Config.helpOpen;
            popupType = Config.helpType;
            popupFrame = Config.helpFrame;
            popupHype = Config.hypeHelp;
        }
        break;
        case "indexlist":
        {
            popupOpened = Config.indexListOpen;
            popupType = Config.indexListType;
            popupFrame = Config.indexListFrame;
            popupHype = Config.hypeIndexList;
        }
        break;
        case "glossary":
        {
            popupOpened = Config.glossaryOpen;
            popupType = Config.glossaryType;
            popupFrame = Config.glossaryFrame;
            popupHype = Config.hypeGlossary;
        }
        break;
        case "volume":
        {
            popupOpened = Config.volumeOpen;
            popupType = null;
            popupFrame = Config.volumeFrame;
            popupHype = Config.hypeVolume;
        }
        break;
    }
    if (_type == "volume") {
        if (!popupOpened) {
            popupOpened = true;
            TweenMax.set(popupFrame, {display:"block"});
            TweenMax.to(popupFrame, .5, {opacity:1, onComplete: function() { popupHype.startTimelineNamed('Main Timeline', popupHype.kDirectionForward); }});
        } else {
            popupOpened = false;
            TweenMax.to(popupFrame, .5, {opacity:0, display:'none', onComplete: function() { popupHype.pauseTimelineNamed('Main Timeline'); popupHype.goToTimeInTimelineNamed(0, 'Main Timeline'); }});
        }
    } else {
        if (!popupOpened) {
            popupOpened = true;
            TweenMax.set(popupFrame, {display:"block"});
            switch (popupType) {
                case 0:
                {
                    TweenMax.to(popupFrame, .5, {opacity:1, onComplete: function() { popupHype.startTimelineNamed('Main Timeline', popupHype.kDirectionForward); }});
                }
                break;
                case 1:
                {
                    TweenMax.to(popupFrame, .5, {left:0, opacity:1, onComplete: function() { popupHype.startTimelineNamed('Main Timeline', popupHype.kDirectionForward); }});
                }
                break;
                case 2:
                {
                    var fW = document.getElementById('mod-frames').offsetWidth;
                    var pW = popupFrame.offsetWidth;
                    var showPos = fW - pW;
                    TweenMax.to(popupFrame, .5, {left:showPos, opacity:1, onComplete: function() { popupHype.startTimelineNamed('Main Timeline', popupHype.kDirectionForward); }});
                }
                break;
            }
        } else {
            popupOpened = false;
            switch (popupType) {
                case 0:
                {
                    TweenMax.to(popupFrame, .5, {opacity:0, display:'none', onComplete: function() { popupHype.pauseTimelineNamed('Main Timeline'); popupHype.goToTimeInTimelineNamed(0, 'Main Timeline'); }});
                }
                break;
                case 1:
                {
                    var pW = popupFrame.offsetWidth;
                    var hidePos = 0 - ((pW - (pW * 2)) - 50);
                    TweenMax.to(popupFrame, .5, {left:hidePos, opacity:0, display:'none', onComplete: function() { popupHype.pauseTimelineNamed('Main Timeline'); popupHype.goToTimeInTimelineNamed(0, 'Main Timeline'); }});
                }
                break;
                case 2:
                {
                    var fW = document.getElementById('mod-frames').offsetWidth;
                    var pW = popupFrame.offsetWidth;
                    var hidePos = fW + (pW + 50);
                    TweenMax.to(popupFrame, .5, {left:hidePos, opacity:0, display:'none', onComplete: function() { popupHype.pauseTimelineNamed('Main Timeline'); popupHype.goToTimeInTimelineNamed(0, 'Main Timeline'); }});
                }
                break;
            }
        }
    }
    switch(_type) {
        case "help":
        {
            Config.helpOpen = popupOpened;
        }
        break;
        case "indexlist":
        {
            Config.indexListOpen = popupOpened;
        }
        break;
        case "glossary":
        {
            Config.glossaryOpen = popupOpened;
        }
        break;
        case "volume":
        {
            Config.volumeOpen = popupOpened;
        }
        break;
    }
};
EffectManager.menuAnim = function(_state) {
    EffectManager.effectMenuStarting = true;
    switch(Config.navigationType) {
        case 0:
        {
            if (_state) {
                TweenMax.to(document.getElementById('ico-open-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 0, opacity: 0, display: 'none', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('ico-close-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 1, opacity: 1, display: 'block', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('mod-navi'), 0.3, { width: (Config.navigationWidth + 5) + "px", ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('box-menu-' + Config.navigationType), 0.3, { opacity: 1, ease:Power1.easeInOut });
            } else {
                TweenMax.to(document.getElementById('ico-open-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 1, opacity: 1, display: 'block', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('ico-close-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 0, opacity: 0, display: 'none', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('mod-navi'), 0.3, { width:"60px", ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('box-menu-' + Config.navigationType), 0.3, { opacity: 0, ease:Power1.easeInOut });
            }
            TweenMax.delayedCall(0.5, function() { EffectManager.effectMenuStarting = false; });
        }
        break;
        case 1:
        {
            var navPosXOpen = 0, navPosXClose = -250;
            if (_state) {
                if (!Config.dynamicBackgroundColor) {
                    navPosXOpen = parseInt(document.getElementById("mod-header").offsetLeft);
                }
                TweenMax.to(document.getElementById('ico-open-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 0, opacity: 0, display: 'none', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('ico-close-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 1, opacity: 1, display: 'block', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('mod-navi'), 0.3, { left:navPosXOpen + "px", ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('box-menu-' + Config.navigationType), 0.3, { opacity: 1, ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('box-menu-title-' + Config.navigationType), 0.3, { width: '195px', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('nav-head-' + Config.navigationType), 0.3, { width: '250px', ease:Power1.easeInOut });
            } else {
                if (!Config.dynamicBackgroundColor) {
                    navPosXClose = parseInt(document.getElementById("mod-header").offsetLeft) - 250;
                }
                TweenMax.to(document.getElementById('ico-open-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 1, opacity: 1, display: 'block', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('ico-close-' + Config.navigationType), 0.3, { transformOrigin:"50% 50%", scale: 0, opacity: 0, display: 'none', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('mod-navi'), 0.3, { left:navPosXClose + "px", ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('box-menu-' + Config.navigationType), 0.3, { opacity: 0, ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('box-menu-title-' + Config.navigationType), 0.3, { width: '250px', ease:Power1.easeInOut });
                TweenMax.to(document.getElementById('nav-head-' + Config.navigationType), 0.3, { width: '300px', ease:Power1.easeInOut });
            }
            TweenMax.delayedCall(0.5, function() { EffectManager.effectMenuStarting = false; });
        }
        break;
        case 2:
        {
            if (_state) {
                TweenMax.to(document.getElementById('btn-menu'), 0.3, { rotation: 0, transformOrigin: "50% 50%", ease: Back.easeOut});
                TweenMax.set(document.getElementById('box-menu-' + Config.navigationType), { display: 'block' }); 
                TweenMax.set(document.getElementById('box-menu-' + Config.navigationType + '-bg'), { display: 'block' }); 
                TweenMax.to(document.getElementById('box-menu-' + Config.navigationType + '-bg'), 0.3, { rotation: 0, transformOrigin: "50% 50%", opacity: 1, ease: Back.easeOut}); 
                TweenMax.delayedCall(0.15, function() {
                    TweenMax.to(document.getElementById('box-menu-' + Config.navigationType), 0.3, { rotation: 0, transformOrigin: "50% 50%", opacity: 1, ease: Back.easeOut});
                });
                TweenMax.delayedCall(0.7, function() {
                    TweenMax.set(document.getElementById("btn-menu"), { rotation: 360, transformOrigin: "50% 50%"});
                    TweenMax.set(document.getElementById("box-menu-2-bg"), { rotation: 360, transformOrigin: "50% 50%"});
                    TweenMax.set(document.getElementById("box-menu-2"), { rotation: 360, transformOrigin: "50% 50%"});
                });
            } else {
                TweenMax.to(document.getElementById('btn-menu'), 0.3, { rotation: 180, transformOrigin: "50% 50%", ease: Back.easeOut});
                TweenMax.to(document.getElementById('box-menu-' + Config.navigationType), 0.3, { rotation: 180, transformOrigin: "50% 50%", opacity: 0, ease: Back.easeOut, display: 'none'});
                TweenMax.delayedCall(0.15, function() {
                    TweenMax.to(document.getElementById('box-menu-' + Config.navigationType + '-bg'), 0.3, { rotation: 180, transformOrigin: "50% 50%", ease: Back.easeOut, opacity: 0, display: 'none'});
                });
            }
            TweenMax.delayedCall(0.8, function() { EffectManager.effectMenuStarting = false; });
        }
        break;
        case 3:
        {
            var heightHeader = document.getElementById('mod-header').offsetHeight;
            var generalW = (heightHeader / 80) * 60;
            if (_state) {
                var totalH = parseInt(heightHeader) + ((generalW + 10) * (Config.columnNavigation - 1));
                var delayAnim = 0.12;
                for (var i = 0; i < (Config.columnNavigation - 1); i++) {
                    document.getElementById("btn-menu-" + Config.navigationType + "-" + i).style.display = 'block';
                    TweenMax.delayedCall((delayAnim * i), function(_id) {
                        TweenMax.to(document.getElementById("btn-menu-" + Config.navigationType + "-" + _id), 0.3, { top: (((generalW + 5) * _id) + 5) + "px", opacity:1, ease: Back.easeOut });
                    }, [i]);
                }
                TweenMax.to(document.getElementById("box-menu-" + Config.navigationType), 0.8, { height: ((generalW + 10) * (Config.columnNavigation - 1)) + 'px', ease: Back.easeOut });
                TweenMax.to(document.getElementById("mod-navi"), 0.8, { height: totalH + 'px', ease: Back.easeOut });
            } else {
                var delayAnim = 0.12;
                var ii = 0;
                for (var i = (Config.columnNavigation - 2); i >= 0; i--) {
                    TweenMax.delayedCall((delayAnim * ii), function(_id) {
                        TweenMax.to(document.getElementById("btn-menu-" + Config.navigationType + "-" + _id), 0.3, { top: "-" + (generalW + 10) + "px", opacity:0, ease: Back.easeOut, display: 'none' });
                    }, [i]);
                    ii++;
                }
                TweenMax.delayedCall(delayAnim, function() {
                    TweenMax.to(document.getElementById("box-menu-" + Config.navigationType), 0.6, { height: '0px' });
                    TweenMax.to(document.getElementById("mod-navi"), 0.6, { height: heightHeader + 'px' });
                });
            }
            TweenMax.delayedCall(0.8, function() { EffectManager.effectMenuStarting = false; });
        }
        break;
    }
};