///////////////////////////////////////////////////////////////
//MultipleChoice.js
///////////////////////////////////////////////////////////////
//Created   : 16/03/2016
//Version   : 1.0
//Author    : Ikhsan Nurrahim (ikhsan@netpolitanteam.com)
///////////////////////////////////////////////////////////////
//TODO :
// - Continue count down when change layout
///////////////////////////////////////////////////////////////
//Multiple Choice
function MultipleChoice(_hDoc, _vars) {
    this.hype = _hDoc;
    this.scene = -1;
    this.layout = -1;
    this.num = 0;
    this.score = 0;
    this.timer = 0;
    this.timeCounter = 0;
    this.chance = 0;
    this.maxChance = 0;
    this.timerType = -1;
    this.timerStopType = -1;
    this.feedbackAnimType = -1;
    this.progressAnimType = -1;
    this.delayChooseTime = 0;
    this.delayFeedbackTime = 0;
    this.delayProgressTime = 0;
    this.timerBarWidth = 0;
    this.maxQuestion = 0;
    this.currAnswer = -1;
    this.currAnsCustom = -1;
    this.answerIsCorrect = false;
    this.multipleAnswer = false;
    this.useUserInput = false;
    this.useOnlyOneObj = false;
    this.useCorrection = false;
    this.sameAnswerInQuest = false;
    this.matchAnswerAndKeys = false;
    this.hideButtonWhenClicked = false;
    this.customChoiceAnswer = false;
    this.randomQuestion = false;
    this.questDone = false;
    this.keys = [];
    this.answers = [];
    this.btnColl = [];
    this.questColl = [];
    this.choiceColl = [];
    this.checkBtnColl = [];
    this.resetBtnColl = [];
    this.textQA = { qText: [], cText: [] };
    this.feedbackAnimColl = [];
    this.feedbackAnimSymTime = [];
    this.progressAnimColl = [];
    this.randomNum = [];
    this.onStart = null;
    this.onStartChoice = null;
    this.onChoose = null;
    this.onResetChoose = null;
    this.onCheck = null;
    this.onFeedback = null;
    this.onProgress = null;
    this.onComplete = null;
    if (_vars.scene !== undefined) {
        this.scene = _vars.scene;
    }
    if (_vars.layout !== undefined) {
        this.layout = _vars.layout;
    }
    if (_vars.timer !== undefined) {
        this.timer = _vars.timer;
    }
    if (_vars.maxChance !== undefined) {
        this.maxChance = _vars.maxChance;
        this.chance = this.maxChance;
    }
    if (_vars.timerType !== undefined) {
        this.timerType = _vars.timerType;
    }
    if (_vars.timerStopType !== undefined) {
        this.timerStopType = _vars.timerStopType;
    }
    if (_vars.feedbackAnimType !== undefined) {
        this.feedbackAnimType = _vars.feedbackAnimType;
    }
    if (_vars.progressAnimType !== undefined) {
        this.progressAnimType = _vars.progressAnimType;
    }
    if (_vars.delayChooseTime !== undefined) {
        this.delayChooseTime = _vars.delayChooseTime;
    }
    if (_vars.delayFeedbackTime !== undefined) {
        this.delayFeedbackTime = _vars.delayFeedbackTime;
    }
    if (_vars.delayProgressTime !== undefined) {
        this.delayProgressTime = _vars.delayProgressTime;
    }
    if (_vars.useUserInput !== undefined) {
        this.useUserInput = _vars.useUserInput;
    }
    if (_vars.useOnlyOneObj !== undefined) {
        this.useOnlyOneObj = _vars.useOnlyOneObj;
    }
    if (_vars.useCorrection !== undefined) {
        this.useCorrection = _vars.useCorrection;
    }
    if (_vars.matchAnswerAndKeys !== undefined) {
        this.matchAnswerAndKeys = _vars.matchAnswerAndKeys;
    }
    if (_vars.hideButtonWhenClicked !== undefined) {
        this.hideButtonWhenClicked = _vars.hideButtonWhenClicked;
    }
    if (_vars.customChoiceAnswer !== undefined) {
        this.customChoiceAnswer = _vars.customChoiceAnswer;
    }
    if (_vars.randomQuestion !== undefined) {
        this.randomQuestion = _vars.randomQuestion;
    }
    if (_vars.keys !== undefined) {
        this.keys = _vars.keys;
    }
    if (_vars.feedbackAnimSymTime !== undefined) {
        this.feedbackAnimSymTime = _vars.feedbackAnimSymTime;
    }
    if (_vars.textQA !== undefined) {
        this.textQA = _vars.textQA;
    }
    if (_vars.onStart !== undefined) {
        this.onStart = _vars.onStart;
    }
    if (_vars.onStartChoice !== undefined) {
        this.onStartChoice = _vars.onStartChoice;
    }
    if (_vars.onChoose !== undefined) {
        this.onChoose = _vars.onChoose;
    }
    if (_vars.onResetChoose !== undefined) {
        this.onResetChoose = _vars.onResetChoose;
    }
    if (_vars.onCheck !== undefined) {
        this.onCheck = _vars.onCheck;
    }
    if (_vars.onFeedback !== undefined) {
        this.onFeedback = _vars.onFeedback;
    }
    if (_vars.onProgress !== undefined) {
        this.onProgress = _vars.onProgress;
    }
    if (_vars.onComplete !== undefined) {
        this.onComplete = _vars.onComplete;
    }
    var i, j, rNum, tArr, tVar, qName, cName, bName;
    this.maxQuestion = this.keys.length;
    for (i = 0; i < this.maxQuestion; i++) {
        if ($.isArray(this.keys[i])) {
            tVar = [];
            for (j = 0; j < this.keys[i].length; j++) {
                tVar.push(-1);
            }
        } else {
            tVar = -1;
        }
        this.answers.push(tVar);
    }
    qName = "";
    for (i = 0; i < this.maxQuestion; i++) {
        if (this.useOnlyOneObj) {
            qName = "quest_";
        } else {
            qName = "quest_" + (i + 1) + "_";
        }
        tVar = {};
        if (this.hype.getElementById(qName + this.scene + "_" + this.layout) != null && this.hype.getElementById(qName + this.scene + "_" + this.layout) != undefined) {
            tVar.element = this.hype.getElementById(qName + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(qName + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(qName + this.scene + "_" + this.layout) != undefined) {
                tVar.symbol = this.hype.getSymbolInstanceById(qName + this.scene + "_" + this.layout);
            } else {
                tVar.symbol = null;
            }
            if (this.hype.getElementById(qName + "txt_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(qName + "txt_" + this.scene + "_" + this.layout) != undefined) {
                tVar.txtElement = this.hype.getElementById(qName + "txt_" + this.scene + "_" + this.layout);
            } else {
                tVar.txtElement = null;
            }
            this.questColl.push(tVar);
        } else {
            break;
        }
    }
    cName = "";
    for (i = 0; i < this.maxQuestion; i++) {
        tArr = [];
        for (j = 0; j < 100; j++) {
            if (this.useOnlyOneObj) {
                cName = "choice_" + (j+1) + "_";
            } else {
                cName = "choice_" + (i+1) + "_" + (j+1) + "_";
            }
            tVar = {};
            if (this.hype.getElementById(cName + this.scene + "_" + this.layout) != null && this.hype.getElementById(cName + this.scene + "_" + this.layout) != undefined) {
                tVar.element = this.hype.getElementById(cName + this.scene + "_" + this.layout);
                if (this.hype.getSymbolInstanceById(cName + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(cName + this.scene + "_" + this.layout) != undefined) {
                    tVar.symbol = this.hype.getSymbolInstanceById(cName + this.scene + "_" + this.layout);
                } else {
                    tVar.symbol = null;
                }
                if (this.hype.getElementById(cName + "txt_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(cName + "txt_" + this.scene + "_" + this.layout) != undefined) {
                    tVar.txtElement = this.hype.getElementById(cName + "txt_" + this.scene + "_" + this.layout);
                } else {
                    tVar.txtElement = null;
                }
                tArr.push(tVar);
            } else {
                break;
            }
        }
        if (tArr.length > 0) {
            this.choiceColl.push(tArr);
        } else {
            break;
        }
    }
    bName = "";
    for (i = 0; i < this.maxQuestion; i++) {
        tArr = [];
        for (j = 0; j < 100; j++) {
            if (this.useOnlyOneObj) {
                if (j < 10) {
                    bName = "btn_ans_0" + j + "_";
                } else {
                    bName = "btn_ans_" + j + "_";
                }
            } else {
                if (j < 10) {
                    bName = "btn_ans_" + (i+1) + "_0" + j + "_";
                } else {
                    bName = "btn_ans_" + (i+1) + "_" + j + "_";
                }
            }
            if (this.hype.getElementById(bName + this.scene + "_" + this.layout) != null && this.hype.getElementById(bName + this.scene + "_" + this.layout) != undefined) {
                tArr.push(this.hype.getElementById(bName + this.scene + "_" + this.layout));
            } else {
                break;
            }
        }
        if (tArr.length > 0) {
            this.btnColl.push(tArr);
        } else {
            break;
        }
    }
    if (this.feedbackAnimType == 0) {
        this.feedbackAnimColl = {};
        if (this.hype.getElementById("feed_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("feed_" + this.scene + "_" + this.layout) != undefined) {
            this.feedbackAnimColl.element = this.hype.getElementById("feed_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById("feed_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("feed_" + this.scene + "_" + this.layout) != undefined) {
                this.feedbackAnimColl.symbol = this.hype.getSymbolInstanceById("feed_" + this.scene + "_" + this.layout);
            } else {
                this.feedbackAnimColl.symbol = null;
            }
            if (this.feedbackAnimSymTime[0] != null && this.feedbackAnimSymTime[0] != undefined) {
                this.feedbackAnimColl.tTime = this.feedbackAnimSymTime[0];
            } else {
                this.feedbackAnimColl.tTime = -1;
            }
            if (this.feedbackAnimSymTime[1] != null && this.feedbackAnimSymTime[1] != undefined) {
                this.feedbackAnimColl.fTime = this.feedbackAnimSymTime[1];
            } else {
                this.feedbackAnimColl.fTime = -1;
            }
        } else {
            this.feedbackAnimColl.element = null;
        }
    } else if (this.feedbackAnimType == 1) {
        for (i = 0; i < this.maxQuestion; i++) {
            tVar = {};
            if (this.hype.getElementById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                tVar.element = this.hype.getElementById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout);
                if (this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                    tVar.symbol = this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout);
                } else {
                    tVar.symbol = null;
                }
                if (this.feedbackAnimSymTime.length > 0) {
                    if (this.feedbackAnimSymTime[i][0] != null && this.feedbackAnimSymTime[i][0] != undefined) {
                        tVar.tTime = this.feedbackAnimSymTime[i][0];
                    } else {
                        tVar.tTime = -1;
                    }
                    if (this.feedbackAnimSymTime[i][1] != null && this.feedbackAnimSymTime[i][1] != undefined) {
                        tVar.fTime = this.feedbackAnimSymTime[i][1];
                    } else {
                        tVar.fTime = -1;
                    }
                } else {
                    tVar.tTime = -1;
                    tVar.fTime = -1;
                }
                this.feedbackAnimColl.push(tVar);
            } else {
                break;
            }
        }
    } else if (this.feedbackAnimType == 2 || this.feedbackAnimType == 3 || this.feedbackAnimType == 4 || this.feedbackAnimType == 5) {
        for (i = 0; i < this.maxQuestion; i++) {
            tArr = [];
            for (j = 0; j < 100; j++) {
                tVar = {};
                if (this.hype.getElementById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                    tVar.element = this.hype.getElementById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout);
                    if (this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                        tVar.symbol = this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout);
                    } else {
                        tVar.symbol = null;
                    }
                    if (this.feedbackAnimSymTime.length > 0) {
                        if (this.feedbackAnimSymTime[i].length > 0) {
                            if (this.feedbackAnimSymTime[i][j][0] != null && this.feedbackAnimSymTime[i][j][0] != undefined) {
                                tVar.tTime = this.feedbackAnimSymTime[i][j][0];
                            } else {
                                tVar.tTime = -1;
                            }
                            if (this.feedbackAnimSymTime[i][j][1] != null && this.feedbackAnimSymTime[i][j][1] != undefined) {
                                tVar.fTime = this.feedbackAnimSymTime[i][j][1];
                            } else {
                                tVar.fTime = -1;
                            }
                        } else {
                            tVar.tTime = -1;
                            tVar.fTime = -1;
                        }
                    } else {
                        tVar.tTime = -1;
                        tVar.fTime = -1;
                    }
                    tArr.push(tVar);
                } else {
                    break;
                }
            }
            if (tArr.length > 0) {
                this.feedbackAnimColl.push(tArr);
            } else {
                break;
            }
        }
    }
    if (this.progressAnimType == 0) {
        this.progressAnimColl = {};
        if (this.hype.getElementById("prog_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("prog_" + this.scene + "_" + this.layout) != undefined) {
            this.progressAnimColl.element = this.hype.getElementById("prog_" + this.scene + "_" + this.layout);
        } else {
            this.progressAnimColl.element = null;
        }
        if (this.hype.getSymbolInstanceById("prog_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("prog_" + this.scene + "_" + this.layout) != undefined) {
            this.progressAnimColl.symbol = this.hype.getSymbolInstanceById("prog_" + this.scene + "_" + this.layout);
        } else {
            this.progressAnimColl.symbol = null;
        }
    } else if (this.progressAnimType == 1 || this.progressAnimType == 2) {
        for (i = 0; i < this.maxQuestion; i++) {
            tVar = {};
            if (this.hype.getElementById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                tVar.element = this.hype.getElementById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout);
                if (this.hype.getSymbolInstanceById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                    tVar.symbol = this.hype.getSymbolInstanceById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout);
                } else {
                    tVar.symbol = null;
                }
                this.progressAnimColl.push(tVar);
            } else {
                break;
            }
        }
    }
    for (i = 0; i < this.maxQuestion; i++) {
        if (this.hype.getElementById("check_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("check_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.checkBtnColl.push(this.hype.getElementById("check_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout));
        }
    }
    for (i = 0; i < this.maxQuestion; i++) {
        if (this.hype.getElementById("reset_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("reset_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.resetBtnColl.push(this.hype.getElementById("reset_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout));
        }
    }
    if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != undefined) {
        $("#next_q_btn_" + this.scene + "_" + this.layout).hide();
        $("#next_q_btn_" + this.scene + "_" + this.layout).css('cursor', 'pointer');
        if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).addEventListener) {
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).addEventListener('click', this.clickedNextQuestion);
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout)._base = this;
        } else if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).attachEvent) {
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).attachEvent('onclick', this.clickedNextQuestion);
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout)._base = this;
        }
    }
    if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#btn_next_" + this.scene + "_" + this.layout).hide();
    }
    if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#mc_next_" + this.scene + "_" + this.layout).hide();
    }
    if (this.timer > 0) {
        if (this.hype.getElementById("timer_bar_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("timer_bar_" + this.scene + "_" + this.layout) != undefined) {
            this.timerBarWidth = $("#timer_bar_" + this.scene + "_" + this.layout).width();
        }
    } 
    if (this.randomQuestion) {
        for (i = 0; i < this.maxQuestion; i++) {
            rNum = Math.floor((Math.random() * this.keys.length));
            if (this.randomNum.indexOf(rNum) == -1) {
                this.randomNum.push(rNum);
            } else {
                if (this.randomNum.length != this.keys.length) {
                    i--;
                } else {
                    break;
                }
            }
        }
    }
}
MultipleChoice.prototype.startQuestion = function() {
    if (!this.useUserInput) {
        TweenMax.killDelayedCallsTo(this.startQuestion);
    }
    var i, j, kID;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    this.answerIsCorrect = false;
    if (this.checkBtnColl.length > 0) {
        if (this.checkBtnColl[numID].removeEventListener) {
            this.checkBtnColl[numID].removeEventListener("click", this.clickedCheck);
        } else if (this.checkBtnColl[numID].detachEvent) {
            this.checkBtnColl[numID].detachEvent("onclick", this.clickedCheck);
        }
        $(this.checkBtnColl[numID]).css("cursor", "pointer");
        if (this.checkBtnColl[numID].addEventListener) {
            this.checkBtnColl[numID].addEventListener("click", this.clickedCheck);
            this.checkBtnColl[numID]._base = this;
        } else if (this.checkBtnColl[numID].attachEvent) {
            this.checkBtnColl[numID].attachEvent("onclick", this.clickedCheck);
            this.checkBtnColl[numID]._base = this;
        }
    }
    if (this.resetBtnColl.length > 0) {
        if (this.resetBtnColl[numID].removeEventListener) {
            this.resetBtnColl[numID].removeEventListener("click", this.clickedReset);
        } else if (this.resetBtnColl[numID].detachEvent) {
            this.resetBtnColl[numID].detachEvent("onclick", this.clickedReset);
        }
        $(this.resetBtnColl[numID]).css("cursor", "pointer");
        if (this.resetBtnColl[numID].addEventListener) {
            this.resetBtnColl[numID].addEventListener("click", this.clickedReset);
            this.resetBtnColl[numID]._base = this;
        } else if (this.resetBtnColl[numID].attachEvent) {
            this.resetBtnColl[numID].attachEvent("onclick", this.clickedReset);
            this.resetBtnColl[numID]._base = this;
        }
    }
    this.sameAnswerInQuest = false;
    this.multipleAnswer = ($.isArray(this.keys[numID]));
    if (this.multipleAnswer) {
        for(i = 0; i < this.keys[numID].length; i++) {
            kID = this.keys[numID][i];
            for (j = 0; j < this.keys[numID].length; j++) {
                if (i != j) {
                    if (kID == this.keys[numID][j]) {
                        this.sameAnswerInQuest = true;
                        break;
                    }
                }
            }
        }
    }
    if (this.questColl.length > 0) {
        if (this.questColl[numID].txtElement != null) {
            $(this.questColl[numID].txtElement).html(this.textQA.qText[numID]);
        }
        if (this.questColl[numID].symbol != null) {
            this.questColl[numID].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
        }
        $(this.questColl[numID].element).fadeIn(300);
    }
    if (this.choiceColl.length > 0) {
        if (numID < this.choiceColl.length) {
            for (i = 0; i < this.choiceColl[numID].length; i++) {
                if (this.choiceColl[numID][i].txtElement != null) {
                    $(this.choiceColl[numID][i].txtElement).html(this.textQA.cText[numID][i]);
                }
                if (this.choiceColl[numID][i].element != null) {
                    $(this.choiceColl[numID][i].element).show();
                    TweenMax.to($(this.choiceColl[numID][i].element), 0.3, { opacity: 1 });
                }
            }
        }
    }
    if (this.onStart != null) {
        this.onStart();
    }
};
MultipleChoice.prototype.startChoice = function() {
    var i;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    if (this.questDone) {
        if ($.isArray(this.answers[numID])) {
            for (i = 0; i < this.answers[numID].length; i++) {
                this.chooseAnswer(this.answers[numID][i]);
            }
        } else {
            this.chooseAnswer(this.answers[numID]);
        }
        return;
    }
    for (i = 0; i < this.btnColl[numID].length; i++) {
        $(this.btnColl[numID][i]).show();
        $(this.btnColl[numID][i]).css("cursor", "pointer");
        if (this.btnColl[numID][i].addEventListener) {
            this.btnColl[numID][i].addEventListener('click', this.clickedAnswers);
            this.btnColl[numID][i]._base = this;
        } else if (this.btnColl[numID][i].attachEvent) {
            this.btnColl[numID][i].attachEvent('onclick', this.clickedAnswers);
            this.btnColl[numID][i]._base = this;
        }
    }
    if (this.timer > 0) {
        this.timeCounter = this.timer;
        switch (this.timerType) {
            case 0:
            {
                $("#timer_text_" + this.scene + "_" + this.layout).html(this.timeCounter);
                TweenMax.delayedCall(1, this.startCountdown, null, this);
            }
            break;
            case 1:
            {
                TweenMax.delayedCall(1, this.countdownNoObject, null, this);
                TweenMax.set($("#timer_bar_" + this.scene + "_" + this.layout), { width: this.timerBarWidth });
                TweenMax.to($("#timer_bar_" + this.scene + "_" + this.layout), this.timer, { width: 0, ease:Linear.easeNone, onComplete: this.stopCountDown, onCompleteScope: this });
            }
            break;
            case 2:
            {
                $("#timer_text_" + this.scene + "_" + this.layout).html(this.timeCounter);
                TweenMax.delayedCall(1, this.startCountdown, null, this);
                TweenMax.set($("#timer_bar_" + this.scene + "_" + this.layout), { width: this.timerBarWidth });
                TweenMax.to($("#timer_bar_" + this.scene + "_" + this.layout), this.timer, { width: 0, ease:Linear.easeNone });
            }
            break;
        }
    }
    if (this.onStartChoice != null) {
        this.onStartChoice();
    }
};
MultipleChoice.prototype.clickedNextQuestion = function(_evt) {
    var _this = null;
    var tmpObj = _evt.target;
    while (tmpObj._base == null && tmpObj._base == undefined) {
        tmpObj = tmpObj.parentNode;
    }
    _this = tmpObj._base;
    var nNum = (_this.num + 1);
    _this.selectQuestion(nNum);
};
MultipleChoice.prototype.selectQuestion = function(_num) {
    this.num = _num;
    this.startQuestion();
};
MultipleChoice.prototype.resetQuestion = function() {
    if (this.delayProgressTime > 0) {
        TweenMax.killDelayedCallsTo(this.resetQuestion);
    }
    if (this.maxChance > 0) {
        this.chance = this.maxChance;
    }
    var i, j;
    var numID = -1;
    if (this.randomQuestion) {
        if (this.num != -1) {
            numID = this.randomNum[this.num];
        } else {
            numID = this.randomNum[0];
        }
    } else {
        numID = this.num;
    }
    this.currAnswer = -1;
    if (this.keys.length > 1) {
        if (numID != -1) {
            for (i = 0; i < this.btnColl[numID].length; i++) {
                $(this.btnColl[numID][i]).hide();
                if (this.btnColl[numID][i].removeEventListener) {
                    this.btnColl[numID][i].removeEventListener('click', this.clickedAnswers);
                } else if (this.btnColl[numID][i].detachEvent) {
                    this.btnColl[numID][i].detachEvent('onclick', this.clickedAnswers);
                }
            }
        }
        if (this.useUserInput) {
            var comp = true;
            for (i = 0; i < this.answers.length; i++) {
                if ($.isArray(this.answers[i])) {
                    for (j = 0; j < this.answers[i].length; j++) {
                        if (this.answers[i][j] == -1) {
                            comp = false;
                            break;
                        }
                    }
                    if (!comp) {
                        break;
                    }
                } else {
                    if (this.answers[i] == -1) {
                        comp = false;
                        break;
                    }
                }
            }
            if (comp) {
                this.questDone = true;
                if (this.checkBtnColl.length > 0) {
                    if (numID != -1) {
                        if (this.checkBtnColl[numID].removeEventListener) {
                            this.checkBtnColl[numID].removeEventListener("click", this.clickedCheck);
                        } else if (this.checkBtnColl[numID].detachEvent) {
                            this.checkBtnColl[numID].detachEvent("onclick", this.clickedCheck);
                        }
                    }
                }
		if (this.resetBtnColl.length > 0) {
                    if (numID != -1) {
                        if (this.resetBtnColl[numID].removeEventListener) {
                            this.resetBtnColl[numID].removeEventListener("click", this.clickedReset);
                        } else if (this.resetBtnColl[numID].detachEvent) {
                            this.resetBtnColl[numID].detachEvent("onclick", this.clickedReset);
                        }
                    }
                }
                if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != undefined) {
                    $("#next_q_btn_" + this.scene + "_" + this.layout).hide();
                    if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener) {
                        this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener('click', this.clickedNextQuestion);
                    } else if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent) {
                        this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent('onclick', this.clickedNextQuestion);
                    }
                }
                if (this.onComplete != null) {
                    this.onComplete();
                }
                if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
                    $("#btn_next_" + this.scene + "_" + this.layout).show();
                }
                if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
                    $("#mc_next_" + this.scene + "_" + this.layout).fadeIn(300);
                    this.hype.getSymbolInstanceById("mc_next_" + this.scene + "_" + this.layout).startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                }
            } else {
                if (this.questColl.length > 0) {
                    for (i = 0; i < this.questColl.length; i++) {
                        $(this.questColl[i].element).fadeOut(300);
                    }
                }
                if (this.choiceColl.length > 0) {
                    if (numID != -1) {
                        if (numID < this.choiceColl.length) {
                            for (i = 0; i < this.choiceColl[numID].length; i++) {
                                if (this.choiceColl[numID][i].element != null) {
                                    var cI = i;
                                    TweenMax.to($(this.choiceColl[numID][i].element), 0.3, { opacity: 0, onComplete: function() {
                                        $(this.choiceColl[numID][cI].element).hide();
                                        if (this.choiceColl[numID][cI].symbol != null) {
                                            this.choiceColl[numID][cI].symbol.pauseTimelineNamed('Main Timeline');
                                            this.choiceColl[numID][cI].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                        }
                                    }, onCompleteScope: this });
                                }
                            }
                        }
                    }
                }
                if (this.feedbackAnimType == 0) {
                    if (this.feedbackAnimColl.element != null) {
                        TweenMax.to($(this.feedbackAnimColl.element), 0.3, { opacity:0, onComplete: function() {
                            $(this.feedbackAnimColl.element).hide();
                            if (this.feedbackAnimColl.symbol != null) {
                                this.feedbackAnimColl.symbol.pauseTimelineNamed('Main Timeline');
                                this.feedbackAnimColl.symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            }
                        }, onCompleteScope: this });
                    }
                } else if (this.feedbackAnimType == 1) {
                    if (numID != -1) {
                        if (this.feedbackAnimColl[numID].element != null) {
                            TweenMax.to($(this.feedbackAnimColl[numID].element), 0.3, { opacity:0, onComplete: function() {
                                $(this.feedbackAnimColl[numID].element).hide();
                                if (this.feedbackAnimColl[numID].symbol != null) {
                                    this.feedbackAnimColl[numID].symbol.pauseTimelineNamed('Main Timeline');
                                    this.feedbackAnimColl[numID].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                }
                            }, onCompleteScope: this });
                        }
                    }
                } else if (this.feedbackAnimType == 2 || this.feedbackAnimType == 3 || this.feedbackAnimType == 4 || this.feedbackAnimType == 5) {
                    if (numID != -1) {
                        for (i = 0; i < this.feedbackAnimColl[numID].length; i++) {
                            if (this.feedbackAnimColl[numID][i].element != null) {
                                TweenMax.to($(this.feedbackAnimColl[numID][i].element), 0.3, { opacity:0, onCompleteParams: [i], onComplete: function(_id) {
                                    $(this.feedbackAnimColl[numID][_id].element).hide();
                                    if (this.feedbackAnimColl[numID][_id].symbol != null) {
                                        this.feedbackAnimColl[numID][_id].symbol.pauseTimelineNamed('Main Timeline');
                                        this.feedbackAnimColl[numID][_id].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                    }
                                }, onCompleteScope: this });
                            }
                        }
                    }
                }
            }
        } else {
            this.num++;
            if (this.num == this.keys.length) {
                this.questDone = true;
                if (this.checkBtnColl.length > 0) {
                    if (numID != -1) {
                        if (this.checkBtnColl[numID].removeEventListener) {
                            this.checkBtnColl[numID].removeEventListener("click", this.clickedCheck);
                        } else if (this.checkBtnColl[numID].detachEvent) {
                            this.checkBtnColl[numID].detachEvent("onclick", this.clickedCheck);
                        }
                    }
                }
		if (this.resetBtnColl.length > 0) {
                    if (numID != -1) {
                        if (this.resetBtnColl[numID].removeEventListener) {
                            this.resetBtnColl[numID].removeEventListener("click", this.clickedReset);
                        } else if (this.resetBtnColl[numID].detachEvent) {
                            this.resetBtnColl[numID].detachEvent("onclick", this.clickedReset);
                        }
                    }
                }
                if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != undefined) {
                    $("#next_q_btn_" + this.scene + "_" + this.layout).hide();
                    if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener) {
                        this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener('click', this.clickedNextQuestion);
                    } else if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent) {
                        this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent('onclick', this.clickedNextQuestion);
                    }
                }
                if (this.onComplete != null) {
                    this.onComplete();
                }
                if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
                    $("#btn_next_" + this.scene + "_" + this.layout).show();
                }
                if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
                    $("#mc_next_" + this.scene + "_" + this.layout).fadeIn(300);
                    this.hype.getSymbolInstanceById("mc_next_" + this.scene + "_" + this.layout).startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                }
            } else {
                if (this.questColl.length > 0) {
                    for (i = 0; i < this.questColl.length; i++) {
                        $(this.questColl[i].element).fadeOut(300);
                    }
                }
                if (this.choiceColl.length > 0) {
                    if (numID != -1) {
                        if (numID < this.choiceColl.length) {
                            for (i = 0; i < this.choiceColl[numID].length; i++) {
                                if (this.choiceColl[numID][i].element != null) {
                                    var cI = i;
                                    TweenMax.to($(this.choiceColl[numID][i].element), 0.3, { opacity: 0, onComplete: function() {
                                        $(this.choiceColl[numID][cI].element).hide();
                                        if (this.choiceColl[numID][cI].symbol != null) {
                                            this.choiceColl[numID][cI].symbol.pauseTimelineNamed('Main Timeline');
                                            this.choiceColl[numID][cI].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                        }
                                    }, onCompleteScope: this });
                                }
                            }
                        }
                    }
                }
                if (this.feedbackAnimType == 0) {
                    if (this.feedbackAnimColl.element != null) {
                        TweenMax.to($(this.feedbackAnimColl.element), 0.3, { opacity:0, onComplete: function() {
                            $(this.feedbackAnimColl.element).hide();
                            if (this.feedbackAnimColl.symbol != null) {
                                this.feedbackAnimColl.symbol.pauseTimelineNamed('Main Timeline');
                                this.feedbackAnimColl.symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            }
                        }, onCompleteScope: this });
                    }
                } else if (this.feedbackAnimType == 1) {
                    if (numID != -1) {
                        if (this.feedbackAnimColl[numID].element != null) {
                            TweenMax.to($(this.feedbackAnimColl[numID].element), 0.3, { opacity:0, onComplete: function() {
                                $(this.feedbackAnimColl[numID].element).hide();
                                if (this.feedbackAnimColl[numID].symbol != null) {
                                    this.feedbackAnimColl[numID].symbol.pauseTimelineNamed('Main Timeline');
                                    this.feedbackAnimColl[numID].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                }
                            }, onCompleteScope: this });
                        }
                    }
                } else if (this.feedbackAnimType == 2 || this.feedbackAnimType == 3 || this.feedbackAnimType == 4 || this.feedbackAnimType == 5) {
                    if (numID != -1) {
                        for (i = 0; i < this.feedbackAnimColl[numID].length; i++) {
                            if (this.feedbackAnimColl[numID][i].element != null) {
                                TweenMax.to($(this.feedbackAnimColl[numID][i].element), 0.3, { opacity:0, onCompleteParams: [i], onComplete: function(_id) {
                                    $(this.feedbackAnimColl[numID][_id].element).hide();
                                    if (this.feedbackAnimColl[numID][_id].symbol != null) {
                                        this.feedbackAnimColl[numID][_id].symbol.pauseTimelineNamed('Main Timeline');
                                        this.feedbackAnimColl[numID][_id].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                    }
                                }, onCompleteScope: this });
                            }
                        }
                    }
                }
                TweenMax.delayedCall(0.5, this.startQuestion, null, this);
            }
        }
    } else {
        this.questDone = true;
        if (this.checkBtnColl.length > 0) {
            if (numID != -1) {
                if (this.checkBtnColl[numID].removeEventListener) {
                    this.checkBtnColl[numID].removeEventListener("click", this.clickedCheck);
                } else if (this.checkBtnColl[numID].detachEvent) {
                    this.checkBtnColl[numID].detachEvent("onclick", this.clickedCheck);
                }
            }
        }
	if (this.resetBtnColl.length > 0) {
            if (numID != -1) {
                if (this.resetBtnColl[numID].removeEventListener) {
                    this.resetBtnColl[numID].removeEventListener("click", this.clickedReset);
                } else if (this.resetBtnColl[numID].detachEvent) {
                    this.resetBtnColl[numID].detachEvent("onclick", this.clickedReset);
                }
            }
        }
        if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != undefined) {
            $("#next_q_btn_" + this.scene + "_" + this.layout).hide();
            if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener) {
                this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener('click', this.clickedNextQuestion);
            } else if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent) {
                this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent('onclick', this.clickedNextQuestion);
            }
        }
        if (this.onComplete != null) {
            this.onComplete();
        }
        if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
            $("#btn_next_" + this.scene + "_" + this.layout).show();
        }
        if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
            $("#mc_next_" + this.scene + "_" + this.layout).fadeIn(300);
            this.hype.getSymbolInstanceById("mc_next_" + this.scene + "_" + this.layout).startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
        }
    }
};
MultipleChoice.prototype.resetChoice = function() {
    var i;
    $(".chance_txt").html(this.chance);
    if (this.chance == 0) {
        this.startProgress(false);
    } else {
        var numID = -1;
        if (this.randomQuestion) {
            numID = this.randomNum[this.num];
        } else {
            numID = this.num;
        }
        if (this.feedbackAnimType == 0) {
            if (this.feedbackAnimColl.element != null) {
                TweenMax.to($(this.feedbackAnimColl.element), 0.3, { opacity:0, onComplete: function() {
                    $(this.feedbackAnimColl.element).hide();
                    if (this.feedbackAnimColl.symbol != null) {
                        this.feedbackAnimColl.symbol.pauseTimelineNamed('Main Timeline');
                        this.feedbackAnimColl.symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                    }
                }, onCompleteScope: this });
            }
        } else if (this.feedbackAnimType == 1) {
            if (this.feedbackAnimColl[numID].element != null) {
                TweenMax.to($(this.feedbackAnimColl[numID].element), 0.3, { opacity:0, onComplete: function() {
                    $(this.feedbackAnimColl[numID].element).hide();
                    if (this.feedbackAnimColl[numID].symbol != null) {
                        this.feedbackAnimColl[numID].symbol.pauseTimelineNamed('Main Timeline');
                        this.feedbackAnimColl[numID].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                    }
                }, onCompleteScope: this });
            }
        }
        if ($.isArray(this.answers[numID])) {
            for(i = 0; i < this.answers[numID].length; i++) {
                if (this.matchAnswerAndKeys) {
                    if (this.keys[numID][i] == this.answers[numID][i]) {
                        this.answers[numID][i] = -1;
                    }
                } else {
                    if (this.keys[numID].indexOf(this.answers[numID][i]) == -1) {
                        this.answers[numID][i] = -1;
                    }
                }
            }
            if (this.feedbackAnimType == 3 || this.feedbackAnimType == 5) {
                for (i = 0; i < this.feedbackAnimColl[numID].length; i++) {
                    if (this.answers[numID].indexOf(i) == -1) {
                        if (this.feedbackAnimColl[numID][i].element != null) {
                            TweenMax.to($(this.feedbackAnimColl[numID][i].element), 0.3, { opacity:0, onCompleteParams: [i], onComplete: function(_id) {
                                $(this.feedbackAnimColl[numID][_id].element).hide();
                                if (this.feedbackAnimColl[numID][_id].symbol != null) {
                                    this.feedbackAnimColl[numID][_id].symbol.pauseTimelineNamed('Main Timeline');
                                    this.feedbackAnimColl[numID][_id].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                }
                            }, onCompleteScope: this });
                        }
                    }
                }
            }
            if (this.choiceColl.length > 0) {
                if (numID < this.choiceColl.length) {
                    for (i = 0; i < this.choiceColl[numID].length; i++) {
                        if (this.answers[numID].indexOf(i) == -1) {
                            if (this.choiceColl[numID][i].symbol != null) {
                                this.choiceColl[numID][i].symbol.pauseTimelineNamed('Main Timeline');
                                this.choiceColl[numID][i].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            }
                        }
                    }
                }
            }
            for (i = 0; i < this.btnColl[numID].length; i++) {
                if (this.answers[numID].indexOf(i) == -1) {
                    $(this.btnColl[numID][i]).show();
                }
            }
        } else {
            this.answers[numID] = -1;
            if (this.feedbackAnimType == 3 || this.feedbackAnimType == 5) {
                for (i = 0; i < this.feedbackAnimColl[numID].length; i++) {
                    if (this.feedbackAnimColl[numID][i].element != null) {
                        TweenMax.to($(this.feedbackAnimColl[numID][i].element), 0.3, { opacity:0, onCompleteParams: [i], onComplete: function(_id) {
                            $(this.feedbackAnimColl[numID][_id].element).hide();
                            if (this.feedbackAnimColl[numID][_id].symbol != null) {
                                this.feedbackAnimColl[numID][_id].symbol.pauseTimelineNamed('Main Timeline');
                                this.feedbackAnimColl[numID][_id].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            }
                        }, onCompleteScope: this });
                    }
                }
            }
            if (this.choiceColl.length > 0) {
                if (numID < this.choiceColl.length) {
                    for (i = 0; i < this.choiceColl[numID].length; i++) {
                        if (this.choiceColl[numID][i].symbol != null) {
                            this.choiceColl[numID][i].symbol.pauseTimelineNamed('Main Timeline');
                            this.choiceColl[numID][i].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                        }
                    }
                }
            }
            for (i = 0; i < this.btnColl[numID].length; i++) {
                $(this.btnColl[numID][i]).show();
            }
        }
    }
    if (this.onResetChoose != null) {
        this.onResetChoose();
    }
};
MultipleChoice.prototype.clickedAnswers = function(_evt) {
    var nGet = (this.id.length - 6);
    var idx = parseInt(this.id.substr(nGet, 2));
    var _this = null;
    var tmpObj = _evt.target;
    while (tmpObj._base == null && tmpObj._base == undefined) {
        tmpObj = tmpObj.parentNode;
    }
    _this = tmpObj._base;
    _this.chooseAnswer(idx);
};
MultipleChoice.prototype.chooseAnswer = function(_idx) {
    var i;
    if (this.timer > 0) {
        switch (this.timerType) {
            case 0:
            {
                TweenMax.killDelayedCallsTo(this.startCountdown);
            }
            break;
            case 1:
            {
                TweenMax.killTweensOf($("#timer_bar_" + this.scene + "_" + this.layout));
            }
            break;
            case 2:
            {
                TweenMax.killDelayedCallsTo(this.startCountdown);
                TweenMax.killTweensOf($("#timer_bar_" + this.scene + "_" + this.layout));
            }
            break;
        }
    }
    if (this.customChoiceAnswer) {
        this.currAnsCustom = _idx;
        this.onChoose();
    } else {
        var numID = -1;
        if (this.randomQuestion) {
            numID = this.randomNum[this.num];
        } else {
            numID = this.num;
        }
        this.currAnswer = _idx;
        if (this.checkBtnColl.length > 0) {
            if ($.isArray(this.answers[numID])) {
                if (this.sameAnswerInQuest) {
                    if (this.answers[numID].indexOf(-1) != -1) {
                        var empAns = this.answers[numID].indexOf(-1);
                        this.answers[numID][empAns] = _idx;
                        if (this.answers[numID].indexOf(-1) == -1) {
                            if (this.delayChooseTime > 0) {
                                TweenMax.delayedCall(this.delayChooseTime, this.showCheckBtn, null, this);
                            } else {
                                this.showCheckBtn();
                            }
                            for (i = 0; i < this.btnColl[numID].length; i++) {
                                $(this.btnColl[numID][i]).hide();
                            }
                        }
                        this.showResetBtn();
                        var answerEmpty = true;
                        for (i = 0; i < this.answers[numID].length; i++) {
                            if (this.answers[numID] != -1) {
                                answerEmpty = false;
                            }
                        }
                        if (answerEmpty) {
                            if (this.resetBtnColl.length > 0) {
                                $(this.resetBtnColl[numID]).hide();
                            }
                        }
                    }
                } else {
                    if (this.answers[numID].indexOf(_idx) != -1) {
                        var sameAns = this.answers[numID].indexOf(_idx);
                        this.answers[numID][sameAns] = -1;
                        for (i = 0; i < this.btnColl[numID].length; i++) {
                            $(this.btnColl[numID][i]).show();
                        }
                        if (this.choiceColl.length > 0) {
                            if (numID < this.choiceColl.length) {
                                if (this.choiceColl[numID][_idx].symbol != null) {
                                    this.choiceColl[numID][_idx].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                }
                            }
                        }
                        if ($(this.checkBtnColl[numID]).css("display") == "block") {
                            $(this.checkBtnColl[numID]).hide();
                        }
                    } else {
                        if (this.answers[numID].indexOf(-1) != -1) {
                            var empAns = this.answers[numID].indexOf(-1);
                            this.answers[numID][empAns] = _idx;
                            if (this.hideButtonWhenClicked) {
                                $(this.btnColl[numID][_idx]).hide();
                            }
                            if (this.answers[numID].indexOf(-1) == -1) {
                                for (i = 0; i < this.btnColl[numID].length; i++) {
                                    if (this.answers[numID].indexOf(i) == -1) {
                                        $(this.btnColl[numID][i]).hide();
                                    }
                                }
                                if (this.delayChooseTime > 0) {
                                    TweenMax.delayedCall(this.delayChooseTime, this.showCheckBtn, null, this);
                                } else {
                                    this.showCheckBtn();
                                }
                            }
                            if (this.choiceColl.length > 0) {
                                if (numID < this.choiceColl.length) {
                                    if (this.choiceColl[numID][_idx].symbol != null) {
                                        this.choiceColl[numID][_idx].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                                    }
                                }
                            }
                        }
                    }
                    this.showResetBtn();
                    var answerEmpty = true;
                    for (i = 0; i < this.answers[numID].length; i++) {
                        if (this.answers[numID] != -1) {
                            answerEmpty = false;
                        }
                    }
                    if (answerEmpty) {
                        if (this.resetBtnColl.length > 0) {
                            $(this.resetBtnColl[numID]).hide();
                        }
                    }
                }
            } else {
                if (this.answers[numID] == -1) {
                    this.answers[numID] = _idx;
                    $(this.btnColl[numID][_idx]).hide();
                    if (this.choiceColl.length > 0) {
                        if (numID < this.choiceColl.length) {
                            if (this.choiceColl[numID][_idx].symbol != null) {
                                this.choiceColl[numID][_idx].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                            }
                        }
                    }
                    if (this.resetBtnColl.length > 0) {
                        $(this.resetBtnColl[numID]).hide();
                    }
                } else {
                    for (i = 0; i < this.btnColl[numID].length; i++) {
                        $(this.btnColl[numID][i]).show();
                    }
                    if (this.choiceColl.length > 0) {
                        if (numID < this.choiceColl.length) {
                            for (i = 0; i < this.choiceColl[numID].length; i++) {
                                if (this.choiceColl[numID][i].symbol != null) {
                                    this.choiceColl[numID][i].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                }
                            }
                            if (this.choiceColl[numID][_idx].symbol != null) {
                                this.choiceColl[numID][_idx].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                            }
                        }
                    }
                    this.answers[numID] = _idx;
                    $(this.btnColl[numID][_idx]).hide();
                }
                if (this.delayChooseTime > 0) {
                    TweenMax.delayedCall(this.delayChooseTime, this.showCheckBtn, null, this);
                } else {
                    this.showCheckBtn();
                }
                this.showResetBtn();
            }
            if (this.onChoose != null) {
                this.onChoose();
            }
        } else {
            if ($.isArray(this.answers[numID])) {
                if (this.sameAnswerInQuest) {
                    if (this.answers[numID].indexOf(-1) != -1) {
                        var empAns = this.answers[numID].indexOf(-1);
                        this.answers[numID][empAns] = _idx;
                        if (this.onChoose != null) {
                            this.onChoose();
                        }
                        if (this.answers[numID].indexOf(-1) == -1) {
                            for (i = 0; i < this.btnColl[numID].length; i++) {
                                $(this.btnColl[numID][i]).hide();
                            }
                            if (this.delayChooseTime > 0) {
                                TweenMax.delayedCall(this.delayChooseTime, this.checkAnswer, null, this);
                            } else {
                                this.checkAnswer();
                            }
                        }
                        this.showResetBtn();
                        var answerEmpty = true;
                        for (i = 0; i < this.answers[numID].length; i++) {
                            if (this.answers[numID] != -1) {
                                answerEmpty = false;
                            }
                        }
                        if (answerEmpty) {
                            if (this.resetBtnColl.length > 0) {
                                $(this.resetBtnColl[numID]).hide();
                            }
                        }
                    }
                } else {
                    if (this.answers[numID].indexOf(-1) != -1) {
                        var empAns = this.answers[numID].indexOf(-1);
                        this.answers[numID][empAns] = _idx;
                        if (this.onChoose != null) {
                            this.onChoose();
                        }
                        if (this.hideButtonWhenClicked) {
                            $(this.btnColl[numID][_idx]).hide();
                        }
                        if (this.answers[numID].indexOf(-1) == -1) {
                            for (i = 0; i < this.btnColl[numID].length; i++) {
                                $(this.btnColl[numID][i]).hide();
                            }
                            if (this.delayChooseTime > 0) {
                                TweenMax.delayedCall(this.delayChooseTime, this.checkAnswer, null, this);
                            } else {
                                this.checkAnswer();
                            }
                        }
                    } else if (this.answers[numID].indexOf(_idx) != -1) {
                        var sameAns = this.answers[numID].indexOf(_idx);
                        this.answers[numID][sameAns] = -1;
                        if (this.onChoose != null) {
                            this.onChoose();
                        }
                    }
                    this.showResetBtn();
                    var answerEmpty = true;
                    for (i = 0; i < this.answers[numID].length; i++) {
                        if (this.answers[numID] != -1) {
                            answerEmpty = false;
                        }
                    }
                    if (answerEmpty) {
                        if (this.resetBtnColl.length > 0) {
                            $(this.resetBtnColl[numID]).hide();
                        }
                    }
                }
            } else {
                this.answers[numID] = _idx;
                for (i = 0; i < this.btnColl[numID].length; i++) {
                    $(this.btnColl[numID][i]).hide();
                }
                if (this.onChoose != null) {
                    this.onChoose();
                }
                if (this.delayChooseTime > 0) {
                    TweenMax.delayedCall(this.delayChooseTime, this.checkAnswer, null, this);
                } else {
                    this.checkAnswer();
                }
            }
            if (this.choiceColl.length > 0) {
                if (numID < this.choiceColl.length) {
                    if (this.choiceColl[numID][_idx].symbol != null) {
                        this.choiceColl[numID][_idx].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                    }
                }
            }
        }
    }
};
MultipleChoice.prototype.showResetBtn = function() {
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    if (this.resetBtnColl.length > 0) {
        $(this.resetBtnColl[numID]).show();
    }
};
MultipleChoice.prototype.clickedReset = function(_evt) {
    var _this = null;
    var tmpObj = _evt.target;
    while (tmpObj._base == null && tmpObj._base == undefined) {
        tmpObj = tmpObj.parentNode;
    }
    _this = tmpObj._base;
    _this.resetAnswer();
};
MultipleChoice.prototype.resetAnswer = function() {
    var i, tmpID;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    if (this.checkBtnColl.length > 0) {
        $(this.checkBtnColl[numID]).hide();
    }
    if (this.resetBtnColl.length > 0) {
        $(this.resetBtnColl[numID]).hide();
    }
    if ($.isArray(this.answers[numID])) {
        for(i = 0; i < this.answers[numID].length; i++) {
            this.answers[numID][i] = -1;
        }
        if (this.choiceColl.length > 0) {
            for (i = 0; i < this.choiceColl[numID].length; i++) {
                if (this.choiceColl[numID][i].symbol != null) {
                    this.choiceColl[numID][i].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                }
            }
        }
        for (i = 0; i < this.btnColl[numID].length; i++) {
            $(this.btnColl[numID][i]).show();
        }
    } else {
        this.answers[numID] = -1;
        if (this.choiceColl.length > 0) {
            for (i = 0; i < this.choiceColl[numID].length; i++) {
                if (this.choiceColl[numID][i].symbol != null) {
                    this.choiceColl[numID][i].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                }
            }
        }
        for (i = 0; i < this.btnColl[numID].length; i++) {
            $(this.btnColl[numID][i]).show();
        }
    }
};
MultipleChoice.prototype.showCheckBtn = function() {
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    if (this.checkBtnColl.length > 0) {
        $(this.checkBtnColl[numID]).show();
    }
};
MultipleChoice.prototype.clickedCheck = function(_evt) {
    var _this = null;
    var tmpObj = _evt.target;
    while (tmpObj._base == null && tmpObj._base == undefined) {
        tmpObj = tmpObj.parentNode;
    }
    _this = tmpObj._base;
    _this.checkAnswer();
};
MultipleChoice.prototype.checkAnswer = function() {
    if (this.delayChooseTime > 0) {
        TweenMax.killDelayedCallsTo(this.checkAnswer);
    }
    var i;
    this.answerIsCorrect = true;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    if (this.checkBtnColl.length > 0) {
        $(this.checkBtnColl[numID]).hide();
    }
    if (this.resetBtnColl.length > 0) {
        $(this.resetBtnColl[numID]).hide();
    }
    if ($.isArray(this.answers[numID])) {
        if (this.matchAnswerAndKeys) {
            for (i = 0; i < this.answers[numID].length; i++) {
                if (this.keys[numID][i] != this.answers[numID][i]) {
                    this.answerIsCorrect = false;
                    break;
                }
            }
        } else {
            for (i = 0; i < this.answers[numID].length; i++) {
                if (this.keys[numID].indexOf(this.answers[numID][i]) == -1) {
                    this.answerIsCorrect = false;
                    break;
                }
            }
        }
    } else {
        if (this.keys[numID] != this.answers[numID]) {
            this.answerIsCorrect = false;
        }
    }
    if (this.answerIsCorrect) {
        this.score = this.score + (100/this.keys.length);
        if (this.score > 100) {
            this.score = 100;
        }
    }
    if (this.onCheck != null) {
        this.onCheck();
    }
    this.startFeedback(this.answerIsCorrect);
};
MultipleChoice.prototype.startFeedback = function(correct) {
    var i, j;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    switch(this.feedbackAnimType) {
        case 0:
            {
                if (this.feedbackAnimColl.symbol != null) {
                    if (this.feedbackAnimColl.tTime != -1 && this.feedbackAnimColl.fTime != -1) {
                        if (correct) {
                            this.feedbackAnimColl.symbol.goToTimeInTimelineNamed(this.feedbackAnimColl.tTime, 'Main Timeline');
                            this.feedbackAnimColl.symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        } else {
                            this.feedbackAnimColl.symbol.goToTimeInTimelineNamed(this.feedbackAnimColl.fTime, 'Main Timeline');
                            this.feedbackAnimColl.symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        }
                    } else {
                        this.feedbackAnimColl.symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                        this.feedbackAnimColl.symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                    }
                }
                $(this.feedbackAnimColl.element).show();
                TweenMax.to($(this.feedbackAnimColl.element), 0.3, { opacity: 1 });
            }
            break;
        case 1:
            {
                if (this.feedbackAnimColl[numID].symbol != null) {
                    if (this.feedbackAnimColl[numID].tTime != -1 && this.feedbackAnimColl[numID].fTime != -1) {
                        if (correct) {
                            this.feedbackAnimColl[numID].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID].tTime, 'Main Timeline');
                            this.feedbackAnimColl[numID].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        } else {
                            this.feedbackAnimColl[numID].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID].fTime, 'Main Timeline');
                            this.feedbackAnimColl[numID].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        }
                    } else {
                        this.feedbackAnimColl[numID].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                        this.feedbackAnimColl[numID].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                    }
                }
                $(this.feedbackAnimColl[numID].element).show();
                TweenMax.to($(this.feedbackAnimColl[numID].element), 0.3, { opacity: 1 });
            }
            break;
        case 2:
            {
                for (i = 0; i < this.feedbackAnimColl[numID].length; i++) {
                    if (this.feedbackAnimColl[numID][i].symbol != null) {
                        if (this.feedbackAnimColl[numID][i].tTime != -1 && this.feedbackAnimColl[numID][i].fTime != -1) {
                            if (correct) {
                                this.feedbackAnimColl[numID][i].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][i].tTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            } else {
                                this.feedbackAnimColl[numID][i].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][i].fTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            }
                        } else {
                            this.feedbackAnimColl[numID][i].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            this.feedbackAnimColl[numID][i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        }
                    }
                    $(this.feedbackAnimColl[numID][i].element).show();
                    TweenMax.to($(this.feedbackAnimColl[numID][i].element), 0.3, { opacity: 1 });
                }
            }
            break;
        case 3:
            {
                if ($.isArray(this.answers[numID])) {
                    for (i = 0; i < this.answers[numID].length; i++) {
                        if (this.feedbackAnimColl[numID][this.answers[numID][i]].symbol != null) {
                            if (this.feedbackAnimColl[numID][this.answers[numID][i]].tTime != -1 && this.feedbackAnimColl[numID][this.answers[numID][i]].fTime != -1) {
                                if (correct) {
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID][i]].tTime, 'Main Timeline');
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                                } else {
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID][i]].fTime, 'Main Timeline');
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                                }
                            } else {
                                this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            }
                        }
                        $(this.feedbackAnimColl[numID][this.answers[numID][i]].element).show();
                        TweenMax.to($(this.feedbackAnimColl[numID][this.answers[numID][i]].element), 0.3, { opacity: 1 });
                    }
                } else {
                    if (this.feedbackAnimColl[numID][this.answers[numID]].symbol != null) {
                        if (this.feedbackAnimColl[numID][this.answers[numID]].tTime != -1 && this.feedbackAnimColl[numID][this.answers[numID]].fTime != -1) {
                            if (correct) {
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID]].tTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            } else {
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID]].fTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            }
                        } else {
                            this.feedbackAnimColl[numID][this.answers[numID]].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            this.feedbackAnimColl[numID][this.answers[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        }
                    }
                    $(this.feedbackAnimColl[numID][this.answers[numID]].element).show();
                    TweenMax.to($(this.feedbackAnimColl[numID][this.answers[numID]].element), 0.3, { opacity: 1 });
                }
            }
            break;
        case 4:
            {
                var kT = false;
                for (i = 0; i < this.feedbackAnimColl[numID].length; i++) {
                    if (this.feedbackAnimColl[numID][i].symbol != null) {
                        if (this.feedbackAnimColl[numID][i].tTime != -1 && this.feedbackAnimColl[numID][i].fTime != -1) {
                            kT = false;
                            if ($.isArray(this.keys[numID])) {
                                kT = this.keys[numID].indexOf(i) != -1;
                            } else {
                                kT = i == this.keys[numID];
                            }
                            if (kT) {
                                this.feedbackAnimColl[numID][i].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][i].tTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            } else {
                                this.feedbackAnimColl[numID][i].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][i].fTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            }
                        }
                    }
                    $(this.feedbackAnimColl[numID][i].element).show();
                    TweenMax.to($(this.feedbackAnimColl[numID][i].element), 0.3, { opacity: 1 });
                }
            }
            break;
        case 5:
            {
                var corrTemp = false;
                if ($.isArray(this.answers[numID])) {
                    for (i = 0; i < this.answers[numID].length; i++) {
                        corrTemp = (this.keys[numID].indexOf(this.answers[numID][i]) != -1);
                        if (this.feedbackAnimColl[numID][this.answers[numID][i]].symbol != null) {
                            if (this.feedbackAnimColl[numID][this.answers[numID][i]].tTime != -1 && this.feedbackAnimColl[numID][this.answers[numID][i]].fTime != -1) {
                                if (corrTemp) {
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID][i]].tTime, 'Main Timeline');
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                                } else {
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID][i]].fTime, 'Main Timeline');
                                    this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                                }
                            } else {
                                this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.answers[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            }
                        }
                        $(this.feedbackAnimColl[numID][this.answers[numID][i]].element).show();
                        TweenMax.to($(this.feedbackAnimColl[numID][this.answers[numID][i]].element), 0.3, { opacity: 1 });
                    }
                } else {
                    corrTemp = (this.keys[numID] == this.answers[numID]);
                    if (this.feedbackAnimColl[numID][this.answers[numID]].symbol != null) {
                        if (this.feedbackAnimColl[numID][this.answers[numID]].tTime != -1 && this.feedbackAnimColl[numID][this.answers[numID]].fTime != -1) {
                            if (corrTemp) {
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID]].tTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            } else {
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.answers[numID]].fTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.answers[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            }
                        } else {
                            this.feedbackAnimColl[numID][this.answers[numID]].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            this.feedbackAnimColl[numID][this.answers[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        }
                    }
                    $(this.feedbackAnimColl[numID][this.answers[numID]].element).show();
                    TweenMax.to($(this.feedbackAnimColl[numID][this.answers[numID]].element), 0.3, { opacity: 1 });
                }
            }
            break;
    }
    if (this.chance > 0) {
        if (!correct){
            this.chance--;
            if (this.delayFeedbackTime > 0) {
                TweenMax.delayedCall(this.delayFeedbackTime, this.resetChoice, null, this);
            } else {
                this.resetChoice();
            }
        } else {
            if (this.delayFeedbackTime > 0) {
                TweenMax.delayedCall(this.delayFeedbackTime, this.startProgress, [correct], this);
            } else {
                this.startProgress(correct);
            }
        }
    } else {
        if (this.delayFeedbackTime > 0) {
            TweenMax.delayedCall(this.delayFeedbackTime, this.startProgress, [correct], this);
        } else {
            this.startProgress(correct);
        }
    }
    if (this.onFeedback != null) {
        this.onFeedback();
    }
};
MultipleChoice.prototype.startFeedbackCorrection = function() {
    var i, j;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    switch(this.feedbackAnimType) {
        case 3:
        case 5:
        {
            if ($.isArray(this.keys[numID])) {
                for (i = 0; i < this.keys[numID].length; i++) {
                    if ($(this.feedbackAnimColl[numID][this.keys[numID][i]].element).css("display") == "none") {
                        if (this.feedbackAnimColl[numID][this.keys[numID][i]].symbol != null) {
                            if (this.feedbackAnimColl[numID][this.keys[numID][i]].tTime != -1) {
                                this.feedbackAnimColl[numID][this.keys[numID][i]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.keys[numID][i]].tTime, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.keys[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            } else {
                                this.feedbackAnimColl[numID][this.keys[numID][i]].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                                this.feedbackAnimColl[numID][this.keys[numID][i]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                            }
                        }
                        $(this.feedbackAnimColl[numID][this.keys[numID][i]].element).show();
                        TweenMax.to($(this.feedbackAnimColl[numID][this.keys[numID][i]].element), 0.3, { opacity: 1 });
                    }
                }
            } else {
                if ($(this.feedbackAnimColl[numID][this.keys[numID]].element).css("display") == "none") {
                    if (this.feedbackAnimColl[numID][this.keys[numID]].symbol != null) {
                        if (this.feedbackAnimColl[numID][this.keys[numID]].tTime != -1) {
                            this.feedbackAnimColl[numID][this.keys[numID]].symbol.goToTimeInTimelineNamed(this.feedbackAnimColl[numID][this.keys[numID]].tTime, 'Main Timeline');
                            this.feedbackAnimColl[numID][this.keys[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        } else {
                            this.feedbackAnimColl[numID][this.keys[numID]].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                            this.feedbackAnimColl[numID][this.keys[numID]].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                        }
                    }
                    $(this.feedbackAnimColl[numID][this.keys[numID]].element).show();
                    TweenMax.to($(this.feedbackAnimColl[numID][this.keys[numID]].element), 0.3, { opacity: 1 });
                }
            }
        }
        break;
    }
};
MultipleChoice.prototype.startProgress = function (correct) {
    if (this.delayFeedbackTime > 0) {
        TweenMax.killDelayedCallsTo(this.startProgress);
    }
    var i, j;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    if (this.progressAnimType == 2) {
        if (this.progressAnimColl[numID].symbol != null) {
            this.progressAnimColl[numID].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
        }
        $(this.progressAnimColl[numID].element).fadeIn(300);
    }
    if (correct) {
        switch(this.progressAnimType) {
            case 0:
                {
                    if (this.progressAnimColl.symbol != null) {
                        this.progressAnimColl.symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                    }
                    $(this.progressAnimColl.element).fadeIn(300);
                }
                break;
            case 1:
                {
                    if (this.progressAnimColl[numID].symbol != null) {
                        this.progressAnimColl[numID].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionForward, false);
                    }
                    $(this.progressAnimColl[numID].element).fadeIn(300);
                }
                break;
        }
    } else {
        if (this.useCorrection) {
            if (this.feedbackAnimType == 0) {
                if (this.feedbackAnimColl.element != null) {
                    TweenMax.to($(this.feedbackAnimColl.element), 0.3, { opacity:0, onComplete: function() {
                        $(this.feedbackAnimColl.element).hide();
                        if (this.feedbackAnimColl.symbol != null) {
                            this.feedbackAnimColl.symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                        }
                    }, onCompleteScope: this });
                }
            } else if (this.feedbackAnimType == 1) {
                if (this.feedbackAnimColl[numID].element != null) {
                    TweenMax.to($(this.feedbackAnimColl[numID].element), 0.3, { opacity:0, onComplete: function() {
                        $(this.feedbackAnimColl[numID].element).hide();
                        if (this.feedbackAnimColl[numID].symbol != null) {
                            this.feedbackAnimColl[numID].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                        }
                    }, onCompleteScope: this });
                }
            }
            if (this.feedbackAnimType == 3 || this.feedbackAnimType == 5) {
                this.startFeedbackCorrection();
            }
        }
    }
    if (this.onProgress != null) {
        this.onProgress();
    }
    if (this.timer > 0) {
        switch (this.timerStopType) {
            case 0:
            {
                if (this.delayProgressTime > 0) {
                    TweenMax.delayedCall(this.delayProgressTime, this.resetQuestion, null, this);
                } else {
                    this.resetQuestion();
                }
            }
            break;
            case 1:
            {
                var _tC = true;
                for (i = 0; i < this.maxQuestion; i++) {
                    if ($.isArray(this.answers[i])) {
                        if (this.answers[i].indexOf(-1) != -1) {
                            _tC = false;
                            break;
                        }
                    } else {
                        if (this.answers[i] == -1) {
                            _tC = false;
                            break;
                        }
                    }
                }
                if (!_tC) {
                    var tNum = (this.num + 1);
                    if (tNum == this.maxQuestion) {
                        tNum = 0;
                    }
                    for (i = tNum; i < this.maxQuestion; i++) {
                        if ($.isArray(this.answers[i])) {
                            if (this.answers[i].indexOf(-1) != -1) {
                                tNum = i;
                                tNum--;
                                break;
                            }
                        } else {
                            if (this.answers[i] == -1) {
                                tNum = i;
                                tNum--;
                                break;
                            }
                        }
                        if (i == (this.maxQuestion - 1)) {
                            i = 0;
                        }
                    }
                    this.num = tNum;
                }
                this.resetQuestion();
            }
            break;
        }
    } else {
        if (this.delayProgressTime > 0) {
            TweenMax.delayedCall(this.delayProgressTime, this.resetQuestion, null, this);
        } else {
            this.resetQuestion();
        }
    }
};
MultipleChoice.prototype.startCountdown = function() {
    this.timeCounter--;
    $("#timer_text_" + this.scene + "_" + this.layout).html(this.timeCounter);
    if (this.timeCounter == 0) {
        TweenMax.killDelayedCallsTo(this.startCountdown);
        this.stopCountDown();
    } else {
        TweenMax.delayedCall(1, this.startCountdown, null, this);
    }
};
MultipleChoice.prototype.countdownNoObject = function() {
    this.timeCounter--;
    if (this.timeCounter == 0) {
        TweenMax.killDelayedCallsTo(this.countdownNoObject);
    } else {
        TweenMax.delayedCall(1, this.countdownNoObject, null, this);
    }
};
MultipleChoice.prototype.stopCountDown = function() {
    var i;
    var numID = -1;
    if (this.randomQuestion) {
        numID = this.randomNum[this.num];
    } else {
        numID = this.num;
    }
    switch (this.timerStopType) {
        case 0:
        {
            if ($.isArray(this.answers[numID])) {
                for (i = 0; i < this.answers[numID].length; i++) {
                    this.answers[numID][i] = this.btnColl[numID].length;
                }
            } else {
                this.answers[numID] = this.btnColl[numID].length;
            }
            this.checkAnswer();
        }
        break;
        case 1:
        {
            var tNum = (this.num + 1);
            if (tNum == this.maxQuestion) {
                tNum = 0;
            }
            for (i = tNum; i < this.maxQuestion; i++) {
                if ($.isArray(this.answers[i])) {
                    if (this.answers[i].indexOf(-1) != -1) {
                        tNum = i;
                        tNum--;
                        break;
                    }
                } else {
                    if (this.answers[i] == -1) {
                        tNum = i;
                        tNum--;
                        break;
                    }
                }
                if (i == (this.maxQuestion - 1)) {
                    i = 0;
                }
            }
            this.num = tNum;
            this.resetQuestion();
        }
        break;
    }
};
MultipleChoice.prototype.reinitVariables = function (_vars) {
    var i, j, qName, cName, bName, numID;
    for (i = 0; i < this.maxQuestion; i++) {
        for (j = 0; j < this.btnColl[i].length; j++) {
            $(this.btnColl[i][j]).hide();
            if (this.btnColl[i][j].removeEventListener) {
                this.btnColl[i][j].removeEventListener('click', this.clickedAnswers);
            } else if (this.btnColl[i][j].detachEvent) {
                this.btnColl[i][j].detachEvent('onclick', this.clickedAnswers);
            }
        }
        if (this.checkBtnColl.length > 0) {
            $(this.checkBtnColl[i]).hide();
            if (this.checkBtnColl[i].removeEventListener) {
                this.checkBtnColl[i].removeEventListener('click', this.clickedCheck);
            } else if (this.checkBtnColl[i].detachEvent) {
                this.checkBtnColl[i].detachEvent('onclick', this.clickedCheck);
            }
        }
        if (this.resetBtnColl.length > 0) {
            $(this.resetBtnColl[i]).hide();
            if (this.resetBtnColl[i].removeEventListener) {
                this.resetBtnColl[i].removeEventListener('click', this.clickedReset);
            } else if (this.resetBtnColl[i].detachEvent) {
                this.resetBtnColl[i].detachEvent('onclick', this.clickedReset);
            }
        }
    }
    if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != undefined) {
        $("#next_q_btn_" + this.scene + "_" + this.layout).hide();
        if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener) {
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).removeEventListener('click', this.clickedNextQuestion);
        } else if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent) {
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).detachEvent('onclick', this.clickedNextQuestion);
        }
    }
    if (_vars.scene !== undefined) {
        this.scene = _vars.scene;
    }
    if (_vars.layout !== undefined) {
        this.layout = _vars.layout;
    }
    if (_vars.onStart !== undefined) {
        this.onStart = _vars.onStart;
    }
    if (_vars.onChoose !== undefined) {
        this.onChoose = _vars.onChoose;
    }
    if (_vars.onCheck !== undefined) {
        this.onCheck = _vars.onCheck;
    }
    if (_vars.onFeedback !== undefined) {
        this.onFeedback = _vars.onFeedback;
    }
    if (_vars.onProgress !== undefined) {
        this.onProgress = _vars.onProgress;
    }
    if (_vars.onComplete !== undefined) {
        this.onComplete = _vars.onComplete;
    }
    qName = "";
    for (i = 0; i < this.maxQuestion; i++) {
        if (this.useOnlyOneObj) {
            qName = "quest_";
        } else {
            qName = "quest_" + (i + 1) + "_";
        }
        if (this.hype.getElementById(qName + this.scene + "_" + this.layout) != null && this.hype.getElementById(qName + this.scene + "_" + this.layout) != undefined) {
            this.questColl[i].element = this.hype.getElementById(qName + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(qName + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(qName + this.scene + "_" + this.layout) != undefined) {
                this.questColl[i].symbol = this.hype.getSymbolInstanceById(qName + this.scene + "_" + this.layout);
            } else {
                this.questColl[i].symbol = null;
            }
            if (this.hype.getElementById(qName + "txt_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(qName + "txt_" + this.scene + "_" + this.layout) != undefined) {
                this.questColl[i].txtElement = this.hype.getElementById(qName + "txt_" + this.scene + "_" + this.layout);
            } else {
                this.questColl[i].txtElement = null;
            }
        } else {
            this.questColl[i].element = null;
        }
    }
    cName = "";
    if (this.choiceColl.length > 0) {
        for (i = 0; i < this.maxQuestion; i++) {
            for (j = 0; j < 100; j++) {
                if (this.useOnlyOneObj) {
                    cName = "choice_" + (j+1) + "_";
                } else {
                    cName = "choice_" + (i+1) + "_" + (j+1) + "_";
                }
                if (this.hype.getElementById(cName + this.scene + "_" + this.layout) != null && this.hype.getElementById(cName + this.scene + "_" + this.layout) != undefined) {
                    this.choiceColl[i][j].element = this.hype.getElementById(cName + this.scene + "_" + this.layout);
                    if (this.hype.getSymbolInstanceById(cName + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(cName + this.scene + "_" + this.layout) != undefined) {
                        this.choiceColl[i][j].symbol = this.hype.getSymbolInstanceById(cName + this.scene + "_" + this.layout);
                    } else {
                        this.choiceColl[i][j].symbol = null;
                    }
                    if (this.hype.getElementById(cName + "txt_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(cName + "txt_" + this.scene + "_" + this.layout) != undefined) {
                        this.choiceColl[i][j].txtElement = this.hype.getElementById(cName + "txt_" + this.scene + "_" + this.layout);
                    } else {
                        this.choiceColl[i][j].txtElement = null;
                    }
                } else {
                    break;
                }
            }
        }
    }
    bName = "";
    for (i = 0; i < this.maxQuestion; i++) {
        for (j = 0; j < 100; j++) {
            if (this.useOnlyOneObj) {
                if (j < 10) {
                    bName = "btn_ans_0" + j + "_";
                } else {
                    bName = "btn_ans_" + j + "_";
                }
            } else {
                if (j < 10) {
                    bName = "btn_ans_" + (i+1) + "_0" + j + "_";
                } else {
                    bName = "btn_ans_" + (i+1) + "_" + j + "_";
                }
            }
            if (this.hype.getElementById(bName + this.scene + "_" + this.layout) != null && this.hype.getElementById(bName + this.scene + "_" + this.layout) != undefined) {
                this.btnColl[i][j] = this.hype.getElementById(bName + this.scene + "_" + this.layout);
            } else {
                break;
            }
        }
    }
    if (this.feedbackAnimType == 0) {
        if (this.hype.getElementById("feed_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("feed_" + this.scene + "_" + this.layout) != undefined) {
            this.feedbackAnimColl.element = this.hype.getElementById("feed_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById("feed_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("feed_" + this.scene + "_" + this.layout) != undefined) {
                this.feedbackAnimColl.symbol = this.hype.getSymbolInstanceById("feed_" + this.scene + "_" + this.layout);
            } else {
                this.feedbackAnimColl.symbol = null;
            }
        } else {
            this.feedbackAnimColl.element = null;
        }
    } else if (this.feedbackAnimType == 1) {
        for (i = 0; i < this.maxQuestion; i++) {
            if (this.hype.getElementById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                this.feedbackAnimColl[i].element = this.hype.getElementById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout);
                if (this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                    this.feedbackAnimColl[i].symbol = this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + this.scene + "_" + this.layout);
                } else {
                    this.feedbackAnimColl[i].symbol = null;
                }
            } else {
                this.feedbackAnimColl[i].element = null;
            }
        }
    } else if (this.feedbackAnimType == 2 || this.feedbackAnimType == 3 || this.feedbackAnimType == 4 || this.feedbackAnimType == 5) {
        for (i = 0; i < this.maxQuestion; i++) {
            for (j = 0; j < 100; j++) {
                if (this.hype.getElementById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                    this.feedbackAnimColl[i][j].element = this.hype.getElementById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout);
                    if (this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                        this.feedbackAnimColl[i][j].symbol = this.hype.getSymbolInstanceById("feed_" + (i+1) + "_" + (j+1) + "_" + this.scene + "_" + this.layout);
                    } else {
                        this.feedbackAnimColl[i][j].symbol = null;
                    }
                } else {
                    break;
                }
            }
        }
    }
    if (this.progressAnimType == 0) {
        if (this.hype.getElementById("prog_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("prog_" + this.scene + "_" + this.layout) != undefined) {
            this.progressAnimColl.element = this.hype.getElementById("prog_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById("prog_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("prog_" + this.scene + "_" + this.layout) != undefined) {
                this.progressAnimColl.symbol = this.hype.getSymbolInstanceById("prog_" + this.scene + "_" + this.layout);
            } else {
                this.progressAnimColl.symbol = null;
            }
        } else {
            this.progressAnimColl.element = null;
        }
    } else if (this.progressAnimType == 1 || this.progressAnimType == 2) {
        for (i = 0; i < this.maxQuestion; i++) {
            if (this.hype.getElementById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                this.progressAnimColl[i].element = this.hype.getElementById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout);
                if (this.hype.getSymbolInstanceById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout) != undefined) {
                    this.progressAnimColl[i].symbol = this.hype.getSymbolInstanceById("prog_" + (i+1) + "_" + this.scene + "_" + this.layout);
                } else {
                    this.progressAnimColl[i].symbol = null;
                }
            } else {
                this.progressAnimColl[i].element = null;
            }
        }
    }
    for (i = 0; i < this.maxQuestion; i++) {
        if (this.hype.getElementById("check_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("check_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.checkBtnColl[i] = this.hype.getElementById("check_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout);
        }
    }
    for (i = 0; i < this.maxQuestion; i++) {
        if (this.hype.getElementById("reset_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("reset_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.resetBtnColl[i] = this.hype.getElementById("reset_btn_" + (i + 1) + "_" + this.scene + "_" + this.layout);
        }
    }
    if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout) != undefined) {
        $("#next_q_btn_" + this.scene + "_" + this.layout).hide();
        $("#next_q_btn_" + this.scene + "_" + this.layout).css('cursor', 'pointer');
        if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).addEventListener) {
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).addEventListener('click', this.clickedNextQuestion);
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout)._base = this;
        } else if (this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).attachEvent) {
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout).attachEvent('onclick', this.clickedNextQuestion);
            this.hype.getElementById("next_q_btn_" + this.scene + "_" + this.layout)._base = this;
        }
    }
    if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#btn_next_" + this.scene + "_" + this.layout).hide();
    }
    if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#mc_next_" + this.scene + "_" + this.layout).hide();
    }
    if (this.timer > 0) {
        if (this.hype.getElementById("timer_bar_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("timer_bar_" + this.scene + "_" + this.layout) != undefined) {
            this.timerBarWidth = $("#timer_bar_" + this.scene + "_" + this.layout).width();
        }
    }
    if (this.num == 0) {
        this.startQuestion();
    } else {
        if (this.questDone) {
            if (!this.useUserInput) {
                this.num--;
            }
            this.startQuestion();
        } else {
            if (this.randomQuestion) {
                numID = this.randomNum[this.num];
            } else {
                numID = this.num;
            }
            if ($.isArray(this.answers[numID])) {
                for (i = 0; i < this.answers[numID].length; i++) {
                    this.answers[numID][i] = -1;
                }
            } else {
                this.answers[numID] = -1;
            }
            if (!this.useUserInput) {
                this.num--;
            }
            this.resetQuestion();
        }
    }
};