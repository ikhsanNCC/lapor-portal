///////////////////////////////////////////////////////////////
//ClickAndShowInfo.js
///////////////////////////////////////////////////////////////
//Created   : 16/03/2016
//Version   : 1.0
//Author    : Ikhsan Nurrahim (ikhsan@netpolitanteam.com)
///////////////////////////////////////////////////////////////
//Click And Show Info
function ClickAndShowInfo(_hDoc, _vars) {
    this.hype = _hDoc;
    this.scene = 0;
    this.layout = 0;
    this.delayProgressTime = 0;
    this.delayNextInfoTime = 0;
    this.currClicked = -1;
    this.useOrder = false;
    this.firstAuto = false;
    this.manualClosing = false;
    this.alwaysShowInfo = false;
    this.hideInfoWhenNext = false;
    this.keepBtnWhenShow = false;
    this.hideBtnAfterShow = false;
    this.hideAllBtnWhenShow = false;
    this.showInfoAtStart = false;
    this.reversePlayInfo = false;
    this.infoLoc = [];
    this.btnColl = [];
    this.chooseColl = [];
    this.infoColl = [];
    this.infoCloseColl = [];
    this.clueColl = [];
    this.checkInfoColl = [];
    this.onStart = null;
    this.onChoose = null;
    this.onClose = null;
    this.onComplete = null;
    if (_vars.scene !== undefined) {
        this.scene = _vars.scene;
    }
    if (_vars.layout !== undefined) {
        this.layout = _vars.layout;
    }
    if (_vars.delayProgressTime !== undefined) {
        this.delayProgressTime = _vars.delayProgressTime;
    }
    if (_vars.delayNextInfoTime !== undefined) {
        this.delayNextInfoTime = _vars.delayNextInfoTime;
    }
    if (_vars.useOrder !== undefined) {
        this.useOrder = _vars.useOrder;
    }
    if (_vars.firstAuto !== undefined) {
        this.firstAuto = _vars.firstAuto;
    }
    if (_vars.manualClosing !== undefined) {
        this.manualClosing = _vars.manualClosing;
    }
    if (_vars.alwaysShowInfo !== undefined) {
        this.alwaysShowInfo = _vars.alwaysShowInfo;
    }
    if (_vars.hideInfoWhenNext !== undefined) {
        this.hideInfoWhenNext = _vars.hideInfoWhenNext;
    }
    if (_vars.keepBtnWhenShow !== undefined) {
        this.keepBtnWhenShow = _vars.keepBtnWhenShow;
    }
    if (_vars.hideBtnAfterShow !== undefined) {
        this.hideBtnAfterShow = _vars.hideBtnAfterShow;
    }
    if (_vars.hideAllBtnWhenShow !== undefined) {
        this.hideAllBtnWhenShow = _vars.hideAllBtnWhenShow;
    }
    if (_vars.showInfoAtStart !== undefined) {
        this.showInfoAtStart = _vars.showInfoAtStart;
    }
    if (_vars.reversePlayInfo !== undefined) {
        this.reversePlayInfo = _vars.reversePlayInfo;
    }
    if (_vars.onStart !== undefined) {
        this.onStart = _vars.onStart;
    }
    if (_vars.onChoose !== undefined) {
        this.onChoose = _vars.onChoose;
    }
    if (_vars.onClose !== undefined) {
        this.onClose = _vars.onClose;
    }
    if (_vars.onComplete !== undefined) {
        this.onComplete = _vars.onComplete;
    }
    var i, tArr, tVar, tmpName;
    tmpName = "btn_info_";
    for (i = 0; i < 100; i++) {
        if (i < 10) {
            if (this.hype.getElementById(tmpName + "0" + i + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + "0" + i + "_" + this.scene + "_" + this.layout) != undefined) {
                this.btnColl.push(this.hype.getElementById(tmpName + "0" + i + "_" + this.scene + "_" + this.layout));
            } else {
                break;
            }
        } else {
            if (this.hype.getElementById(tmpName + i + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + i + "_" + this.scene + "_" + this.layout) != undefined) {
                this.btnColl.push(this.hype.getElementById(tmpName + i + "_" + this.scene + "_" + this.layout));
            } else {
                break;
            }
        }
    }
    tmpName = "choice_info_";
    for (i = 0; i < 100; i++) {
        tVar = {};
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            tVar.element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                tVar.symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                tVar.symbol = null;
            }
            this.chooseColl.push(tVar);
        } else {
            break;
        }
    }
    tmpName = "info_";
    for (i = 0; i < 100; i++) {
        tVar = {};
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            tVar.element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                tVar.symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                tVar.symbol = null;
            }
            this.infoColl.push(tVar);
        } else {
            break;
        }
    }
    tmpName = "clue_";
    for (i = 0; i < 100; i++) {
        tVar = {};
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            tVar.element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                tVar.symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                tVar.symbol = null;
            }
            this.clueColl.push(tVar);
        } else {
            break;
        }
    }
    tmpName = "check_";
    for (i = 0; i < 100; i++) {
        tVar = {};
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            tVar.element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                tVar.symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                tVar.symbol = null;
            }
            this.checkInfoColl.push(tVar);
        } else {
            break;
        }
    }
    tmpName = "close_btn_";
    for (i = 0; i < 100; i++) {
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.infoCloseColl.push(this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout));
        } else {
            break;
        }
    }
    if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#btn_next_" + this.scene + "_" + this.layout).hide();
    }
    if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#mc_next_" + this.scene + "_" + this.layout).hide();
    }
}
ClickAndShowInfo.prototype.startInfo = function() {
    var i;
    for (i = 0; i < this.infoColl.length; i++) {
        this.infoLoc.push(0);
        if (!this.showInfoAtStart) {
            $(this.infoColl[i].element).hide();
        }
    }
    for (i = 0; i < this.btnColl.length; i++) {
        if (!this.useOrder) {
            $(this.btnColl[i]).show();
        } else {
            $(this.btnColl[i]).hide();
        }
        $(this.btnColl[i]).css("cursor", "pointer");
        if (this.btnColl[i].addEventListener) {
            this.btnColl[i].addEventListener("click", this.clickedInfo);
            this.btnColl[i]._base = this;
            this.btnColl[i]._idx = i;
        } else if (this.btnColl[i].attachEvent) {
            this.btnColl[i].attachEvent("onclick", this.clickedInfo);
            this.btnColl[i]._base = this;
            this.btnColl[i]._idx = i;
        }
    }
    if (this.infoCloseColl.length > 0) {
        for (i = 0; i < this.infoCloseColl.length; i++) {
            $(this.infoCloseColl[i]).css("cursor", "pointer");
            if (this.infoCloseColl[i].addEventListener) {
                this.infoCloseColl[i].addEventListener("click", this.clickedClose);
                this.infoCloseColl[i]._base = this;
            } else if (this.infoCloseColl[i].attachEvent) {
                this.infoCloseColl[i].attachEvent("onclick", this.clickedClose);
                this.infoCloseColl[i]._base = this;
            }
        }
    }
    if (this.useOrder) {
        if (this.clueColl.length > 0) {
            for (i = 0; i < this.clueColl.length; i++) {
                $(this.clueColl[i].element).hide();
            }
        }
    }
    if (this.onStart != null) {
        this.onStart();
    }
    if (this.firstAuto) {
        this.chooseInfo(0);
    } else {
        if (this.useOrder) {
            $(this.btnColl[0]).show();
            if (this.clueColl.length > 0) {
                $(this.clueColl[0].element).fadeIn(300);
                if (this.clueColl[0].symbol != null) {
                    this.clueColl[0].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                }
            }
        }
    }
};
ClickAndShowInfo.prototype.clickedInfo = function(_evt) {
    var _this = null;
    var tmpObj = _evt.target;
    while (tmpObj._base == null && tmpObj._base == undefined) {
        tmpObj = tmpObj.parentNode;
    }
    _this = tmpObj._base;
    var _idx = -1;
    tmpObj = _evt.target;
    while (tmpObj._idx == null && tmpObj._idx == undefined) {
        tmpObj = tmpObj.parentNode;
    }
    _idx = tmpObj._idx;
    _this.chooseInfo(_idx);
};
ClickAndShowInfo.prototype.clickedClose = function(_evt) {
    var _this = null;
    var tmpObj = _evt.target;
    while (tmpObj._base == null && tmpObj._base == undefined) {
        tmpObj = tmpObj.parentNode;
    }
    _this = tmpObj._base;
    _this.currClicked = -1;
    _this.closingInfo();
};
ClickAndShowInfo.prototype.chooseInfo = function(_idx) {
    var i, j, tmpID;
    this.currClicked = _idx;
    this.infoLoc[_idx] = 1;
    if (this.hideInfoWhenNext) {
        for (i = 0; i < this.infoColl.length; i++) {
            if (i != _idx) {
                $(this.infoColl[i].element).fadeOut(300);
            }
        }
    }
    if (this.reversePlayInfo) {
        for (i = 0; i < this.infoColl.length; i++) {
            if (i != _idx) {
                if (this.infoColl[i].symbol != null) {
                    if (this.infoColl[i].symbol.currentTimeInTimelineNamed('Main Timeline') > 0) {
                        this.infoColl[i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionReverse, false);
                    }
                }
            }
        }
    }
    if (!this.showInfoAtStart) {
        $(this.infoColl[_idx].element).fadeIn(300);
    }
    if (this.infoColl[_idx].symbol != null) {
        this.infoColl[_idx].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
    }
    if (this.chooseColl.length > 0) {
        for (i = 0; i < this.chooseColl.length; i++) {
            if (this.chooseColl[i].symbol != null) {
                if (this.chooseColl[i].symbol.currentTimeInTimelineNamed('Main Timeline') > 0) {
                    this.chooseColl[i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionReverse, false);
                }
            }
        }
    }
    if (this.useOrder) {
        if (this.clueColl.length > 0) {
            for (i = 0; i < this.clueColl.length; i++) {
                if ($(this.clueColl[i].element).css("display") == "block") {
                    tmpID = i;
                    TweenMax.to(this.clueColl[i].element, 0.3, { opacity: 0, onComplete: function() {
                        if (this.clueColl[tmpID].symbol != null) {
                            this.clueColl[tmpID].symbol.pauseTimelineNamed('Main Timeline');
                            this.clueColl[tmpID].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                        }
                        $(this.clueColl[tmpID].element).hide();
                    }, onCompleteScope: this });
                    break;
                }
            }
        }
        if (!this.hideBtnAfterShow) {
            for (i = 0; i < this.infoLoc.length; i++) {
                if (this.infoLoc[i] == 1) {
                    $(this.btnColl[i]).show();
                }
            }
        }
    } else {
        if (this.hideAllBtnWhenShow) {
            for (i = 0; i < this.btnColl.length; i++) {
                $(this.btnColl[i]).hide();
            }
        } else {
            for (i = 0; i < this.btnColl.length; i++) {
                $(this.btnColl[i]).show();
            }
        }
    }
    if (!this.keepBtnWhenShow) {
        $(this.btnColl[_idx]).hide();
    }
    if (this.chooseColl.length > 0) {
        if (this.chooseColl[_idx].symbol != null) {
            this.chooseColl[_idx].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
        }
    }
    if (this.onChoose != null) {
        this.onChoose();
    }
    if (!this.manualClosing) {
        if (this.infoCloseColl.length <= 0) {
            if (this.delayProgressTime > 0) {
                TweenMax.delayedCall(this.delayProgressTime, this.closingInfo, null, this);
            } else {
                this.closingInfo();
            }
        }
    }
};
ClickAndShowInfo.prototype.closingInfo = function() {
    var i;
    if (this.delayProgressTime > 0) {
        TweenMax.killDelayedCallsTo(this.closingInfo);
    }
    if (!this.alwaysShowInfo) {
        for (i = 0; i < this.infoColl.length; i++) {
            $(this.infoColl[i].element).fadeOut(300);
        }        
    }
    if (this.onClose != null) {
        this.onClose();
    }
    if (this.delayNextInfoTime > 0) {
        TweenMax.delayedCall(this.delayNextInfoTime, this.checkingComplete, null, this);
    } else {
        this.checkingComplete();
    }
};
ClickAndShowInfo.prototype.checkingComplete = function() {
    var i, nextID;
    nextID = -1;
    if (this.delayNextInfoTime > 0) {
        TweenMax.killDelayedCallsTo(this.checkingComplete);
    }
    if (this.useOrder) {
        if (!this.hideBtnAfterShow) {
            for (i = 0; i < this.infoLoc.length; i++) {
                if (this.infoLoc[i] == 1) {
                    $(this.btnColl[i]).show();
                }
            }
        }
        for (i = 0; i < this.infoLoc.length; i++) {
            if (this.infoLoc[i] == 0) {
                nextID = i;
                break;
            }
        }
        if (nextID != -1) {
            $(this.btnColl[nextID]).show();
            if (this.clueColl.length > 0) {
                $(this.clueColl[nextID].element).show();
                TweenMax.to(this.clueColl[nextID].element, 0.3, { opacity: 1 });
                if (this.clueColl[nextID].symbol != null) {
                    this.clueColl[nextID].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                }
            }
        }
    } else {
        if (this.hideAllBtnWhenShow) {
            for (i = 0; i < this.btnColl.length; i++) {
                $(this.btnColl[i]).show();
            }
        }
    }
    if (this.checkInfoColl.length > 0) {
        for (i = 0; i < this.infoLoc.length; i++) {
            if (this.infoLoc[i] == 1) {
                $(this.checkInfoColl[i].element).fadeIn(300);
                if (this.checkInfoColl[i].symbol != null) {
                    this.checkInfoColl[i].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                }
            }
        }
    }
    if (this.infoLoc.indexOf(0) == -1) {
        if (this.hideBtnAfterShow) {
            for (i = 0; i < this.btnColl.length; i++) {
                $(this.btnColl[i]).show();
            }
        }
        if (this.onComplete != null) {
            this.onComplete();
        }
        if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
            $("#btn_next_" + this.scene + "_" + this.layout).show();
        }
        if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
            $("#mc_next_" + this.scene + "_" + this.layout).fadeIn(300);
            this.hype.getSymbolInstanceById('mc_next_' + this.scene + '_' + this.layout).startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
        }
    }
};
ClickAndShowInfo.prototype.reinitVariables = function (_vars) {
    var i, tArr, tVar, tmpName;
    for (i = 0; i < this.btnColl.length; i++) {
        if (this.btnColl[i].removeEventListener) {
            this.btnColl[i].removeEventListener("click", this.clickedInfo);
        } else if (this.btnColl[i].detachEvent) {
            this.btnColl[i].detachEvent("onclick", this.clickedInfo);
        }
    }
    if (this.infoCloseColl.length > 0) {
        for (i = 0; i < this.infoCloseColl.length; i++) {
            if (this.infoCloseColl[i].removeEventListener) {
                this.infoCloseColl[i].removeEventListener("click", this.clickedClose);
            } else if (this.infoCloseColl[i].detachEvent) {
                this.infoCloseColl[i].detachEvent("onclick", this.clickedClose);
            }
        }
    }
    if (_vars.scene !== undefined) {
        this.scene = _vars.scene;
    }
    if (_vars.layout !== undefined) {
        this.layout = _vars.layout;
    }
    if (_vars.onStart !== undefined) {
        this.onStart = _vars.onStart;
    }
    if (_vars.onChoose !== undefined) {
        this.onChoose = _vars.onChoose;
    }
    if (_vars.onClose !== undefined) {
        this.onClose = _vars.onClose;
    }
    if (_vars.onComplete !== undefined) {
        this.onComplete = _vars.onComplete;
    }
    tmpName = "btn_info_";
    for (i = 0; i < 100; i++) {
        if (i < 10) {
            if (this.hype.getElementById(tmpName + "0" + i + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + "0" + i + "_" + this.scene + "_" + this.layout) != undefined) {
                this.btnColl[i] = this.hype.getElementById(tmpName + "0" + i + "_" + this.scene + "_" + this.layout);
            } else {
                break;
            }
        } else {
            if (this.hype.getElementById(tmpName + i + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + i + "_" + this.scene + "_" + this.layout) != undefined) {
                this.btnColl[i] = this.hype.getElementById(tmpName + i + "_" + this.scene + "_" + this.layout);
            } else {
                break;
            }
        }
    }
    tmpName = "choice_info_";
    for (i = 0; i < 100; i++) {
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.chooseColl[i].element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                this.chooseColl[i].symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                this.chooseColl[i].symbol = null;
            }
        } else {
            break;
        }
    }
    tmpName = "info_";
    for (i = 0; i < 100; i++) {
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.infoColl[i].element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                this.infoColl[i].symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                this.infoColl[i].symbol = null;
            }
        } else {
            break;
        }
    }
    tmpName = "clue_";
    for (i = 0; i < 100; i++) {
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.clueColl[i].element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                this.clueColl[i].symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                this.clueColl[i].symbol = null;
            }
        } else {
            break;
        }
    }
    tmpName = "check_";
    for (i = 0; i < 100; i++) {
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.checkInfoColl[i].element = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            if (this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
                this.checkInfoColl[i].symbol = this.hype.getSymbolInstanceById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
            } else {
                this.checkInfoColl[i].symbol = null;
            }
        } else {
            break;
        }
    }
    tmpName = "close_btn_";
    for (i = 0; i < 100; i++) {
        if (this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != null && this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout) != undefined) {
            this.infoCloseColl[i] = this.hype.getElementById(tmpName + (i + 1) + "_" + this.scene + "_" + this.layout);
        } else {
            break;
        }
    }
    if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#btn_next_" + this.scene + "_" + this.layout).hide();
    }
    if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
        $("#mc_next_" + this.scene + "_" + this.layout).hide();
    }
    if (this.infoLoc.indexOf(1) == -1) {
        this.startInfo();
    } else {
        for (i = 0; i < this.btnColl.length; i++) {
            $(this.btnColl[i]).css("cursor", "pointer");
            if (this.btnColl[i].addEventListener) {
                this.btnColl[i].addEventListener("click", this.clickedInfo);
                this.btnColl[i]._base = this;
                this.btnColl[i]._idx = i;
            } else if (this.btnColl[i].attachEvent) {
                this.btnColl[i].attachEvent("onclick", this.clickedInfo);
                this.btnColl[i]._base = this;
                this.btnColl[i]._idx = i;
            }
        }
        if (this.infoCloseColl.length > 0) {
            for (i = 0; i < this.infoCloseColl.length; i++) {
                $(this.infoCloseColl[i]).css("cursor", "pointer");
                if (this.infoCloseColl[i].addEventListener) {
                    this.infoCloseColl[i].addEventListener("click", this.clickedClose);
                    this.infoCloseColl[i]._base = this;
                } else if (this.infoCloseColl[i].attachEvent) {
                    this.infoCloseColl[i].attachEvent("onclick", this.clickedClose);
                    this.infoCloseColl[i]._base = this;
                }
            }
        }
        for (i = 0; i < this.btnColl.length; i++) {
            $(this.btnColl[i]).hide();
        }
        if (this.useOrder) {
            for (i = 0; i < this.infoLoc.length; i++) {
                if (this.infoLoc[i] == 1) {
                    if (!this.hideBtnAfterShow) {
                        $(this.btnColl[i]).show();
                    }
                } else if (this.infoLoc[i] == 0) {
                    $(this.btnColl[i]).show();
                    break;
                }
            }
        } else {
            for (i = 0; i < this.infoLoc.length; i++) {
                if (this.infoLoc[i] == 1) {
                    if (!this.hideBtnAfterShow) {
                        $(this.btnColl[i]).show();
                    }
                } else if (this.infoLoc[i] == 0) {
                    $(this.btnColl[i]).show();
                }
            }
        }
        if (this.clueColl.length > 0) {
            for (i = 0; i < this.clueColl.length; i++) {
                TweenMax.set(this.clueColl[i].element, { opacity: 0 });
                if (this.clueColl[i].symbol != null) {
                    this.clueColl[i].symbol.pauseTimelineNamed('Main Timeline');
                    this.clueColl[i].symbol.goToTimeInTimelineNamed(0, 'Main Timeline');
                }
                $(this.clueColl[i].element).hide();
            }
            if (this.useOrder) {
                for (i = 0; i < this.infoLoc.length; i++) {
                    if (this.infoLoc[i] == 0) {
                        $(this.clueColl[i].element).show();
                        TweenMax.to(this.clueColl[i].element, 0.3, { opacity: 1 });
                        if (this.clueColl[i].symbol != null) {
                            this.clueColl[i].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                        }
                        break;
                    }
                }
            } else {
                for (i = 0; i < this.infoLoc.length; i++) {
                    if (this.infoLoc[i] == 0) {
                        $(this.clueColl[i].element).show();
                        TweenMax.to(this.clueColl[i].element, 0.3, { opacity: 1 });
                        if (this.clueColl[i].symbol != null) {
                            this.clueColl[i].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                        }
                    }
                }
            }
        }
        for (i = 0; i < this.infoColl.length; i++) {
            $(this.infoColl[i].element).fadeOut(300);
        }
        if (this.alwaysShowInfo) {
            if (this.hideInfoWhenNext) {
                $(this.infoColl[this.currClicked].element).fadeIn(300);
                if (this.infoColl[this.currClicked].symbol != null) {
                    this.infoColl[this.currClicked].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                }
            } else {
                for (i = 0; i < this.infoLoc.length; i++) {
                    if (this.infoLoc[i] == 1) {
                        $(this.infoColl[i].element).fadeIn(300);
                        if (this.infoColl[i].symbol != null) {
                            this.infoColl[i].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                        }
                    }
                }
            }
            if (this.chooseColl.length > 0) {
                for (i = 0; i < this.chooseColl.length; i++) {
                    if (this.chooseColl[i].symbol != null) {
                        if (this.chooseColl[i].symbol.currentTimeInTimelineNamed('Main Timeline') > 0) {
                            this.chooseColl[i].symbol.continueTimelineNamed('Main Timeline', this.hype.kDirectionReverse, false);
                        }
                    }
                }
                if (this.chooseColl[this.currClicked].symbol != null) {
                    this.chooseColl[this.currClicked].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                }
            }
        }
        if (this.checkInfoColl.length > 0) {
            for (i = 0; i < this.infoLoc.length; i++) {
                if (this.infoLoc[i] == 1) {
                    $(this.checkInfoColl[i].element).fadeIn(300);
                    if (this.checkInfoColl[i].symbol != null) {
                        this.checkInfoColl[i].symbol.startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
                    }
                }
            }
        }
        if (this.infoLoc.indexOf(0) == -1) {
            if (!this.hideBtnAfterShow) {
                for (i = 0; i < this.btnColl.length; i++) {
                    $(this.btnColl[i]).show();
                }
            }
            if (this.onComplete != null) {
                this.onComplete();
            }
            if (this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("btn_next_" + this.scene + "_" + this.layout) != undefined) {
                $("#btn_next_" + this.scene + "_" + this.layout).show();
            }
            if (this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != null && this.hype.getElementById("mc_next_" + this.scene + "_" + this.layout) != undefined) {
                $("#mc_next_" + this.scene + "_" + this.layout).fadeIn(300);
                this.hype.getSymbolInstanceById('mc_next_' + this.scene + '_' + this.layout).startTimelineNamed('Main Timeline', this.hype.kDirectionForward);
            }
        }
    }
};