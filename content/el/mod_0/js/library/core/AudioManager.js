var AudioManager = {};
AudioManager.initBacksound = function() {
    if (!Config.preloaderState) {
        Main.showPreload();
    }
    var volBGM;
    if (AudioConfig.bgmMuteState) {
        volBGM = 0;
    } else {
        volBGM = AudioConfig.bgmVolume;
    }
    Config.audioBGM = new Howl({
        src: ["data/audio/bgm.mp3","data/audio/bgm.ogg"],
        sprite: AudioConfig.bgmSprite,
        volume: volBGM,
        onload: function() {
            if (AudioConfig.bgmSize > 1) {
                AudioConfig.BGMID = [];
                for (var i = 0; i < AudioConfig.bgmSize; i++) {
                    AudioConfig.BGMID.push(null);
                }
                AudioConfig.BGMID[0] = Config.audioBGM.play('bgm_0');
            } else {
                AudioConfig.BGMID = null;
                AudioConfig.BGMID = Config.audioBGM.play('bgm_0');
            }
            if (AudioConfig.sfxSize > 1) {
                AudioConfig.SFXID = [];
                for (var i = 0; i < AudioConfig.sfxSize; i++) {
                    AudioConfig.SFXID.push(null);
                }
            } else {
                AudioConfig.SFXID = null;
            }
            //Main.checkLastPage();
            if (Config.hypeScreen != null) {
                Main.hidePreload();
                PopupManager.screenShow();
            }
        }
    });
};
AudioManager.playBacksound = function(_id) {
    if (AudioConfig.playedIDBGM == _id) return;
    AudioManager.stopBacksound();
    if (AudioConfig.bgmSize > 1) {
        AudioConfig.playedIDBGM = _id;
        AudioConfig.BGMID[_id] = Config.audioBGM.play('bgm_' + _id);
    } else {
        AudioConfig.BGMID = Config.audioBGM.play('bgm_0');
    }
};
AudioManager.stopBacksound = function() {
    if (AudioConfig.bgmSize > 1) {
        for (var i = 0; i < AudioConfig.bgmSize; i++) {
            if (AudioConfig.BGMID[i] != null) {
                Config.audioBGM.stop(AudioConfig.BGMID[i]);
            }
        }
    } else {
        Config.audioBGM.stop(AudioConfig.BGMID);
    }
    AudioConfig.playedIDBGM = -1;
};
AudioManager.pauseBacksound = function() {
    if (AudioConfig.bgmMuteState) return;
    if (AudioConfig.allMuteState) return;
    if (AudioConfig.bgmSize > 1) {
        if (AudioConfig.playedIDBGM == -1) return;
        Config.audioBGM.pause(AudioConfig.BGMID[AudioConfig.playedIDBGM]);
    } else {
        if (AudioConfig.BGMID == null) return;
        Config.audioBGM.pause(AudioConfig.BGMID);
    }
};
AudioManager.resumeBacksound = function() {
    if (AudioConfig.bgmMuteState) return;
    if (AudioConfig.allMuteState) return;
    if (AudioConfig.bgmSize > 1) {
        if (AudioConfig.playedIDBGM == -1) return;
        Config.audioBGM.play(AudioConfig.BGMID[AudioConfig.playedIDBGM]);
    } else {
        if (AudioConfig.BGMID == null) return;
        Config.audioBGM.play(AudioConfig.BGMID);
    }
};
AudioManager.muteBacksound = function(_state) {
    AudioConfig.bgmMuteState = _state;
    if (AudioConfig.bgmMuteState) {
        if (Config.audioBGM != null) {
            Config.audioBGM.volume(0);
        }
        if (AudioConfig.audioMuteState) {
            if (Config.useVolumeWindow) {
                var btnVolNav;
                switch (Config.navigationType) {
                    case 0:
                    {
                        btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                        btnVolNav.innerHTML = '<img class="ico-menu" src="data/image/volume-off.png">';
                    }
                    break;
                    case 1:
                    {
                        btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                        btnVolNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/volume-off.png">';
                    }
                    break;
                    case 2:
                    {
                        btnVolNav = document.getElementById('btn-volume');
                        btnVolNav.innerHTML = '<img src="data/image/volume-off.png" />';
                    }
                    break;
                }
                btnVolNav.classList.remove('used');
                btnVolNav.classList.add('unused');
            }
        }
    } else {
        if (Config.audioBGM != null) {
            Config.audioBGM.volume(AudioConfig.bgmVolume);
        }
        if (Config.useVolumeWindow) {
            var btnVolNav;
            switch (Config.navigationType) {
                case 0:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu" src="data/image/volume.png">';
                }
                break;
                case 1:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/volume.png">';
                }
                break;
                case 2:
                {
                    btnVolNav = document.getElementById('btn-volume');
                    btnVolNav.innerHTML = '<img src="data/image/volume.png" />';
                }
                break;
            }
            btnVolNav.classList.remove('unused');
            btnVolNav.classList.add('used');
        }
    }
};
AudioManager.decBacksoundVolume = function() {
    TweenMax.killDelayedCallsTo(AudioManager.incBacksoundVolume);
    if (AudioConfig.bgmMuteState) return;
    AudioConfig.incDescState = true;
    AudioConfig.bgmVolume = Config.audioBGM.volume();
    var minBgmVol = 0.2;
    if (AudioConfig.maxBgmVolume < minBgmVol)
    	minBgmVol = AudioConfig.maxBgmVolume;
    if (AudioConfig.bgmVolume > minBgmVol) {
        AudioConfig.bgmVolume -= 0.05;
        TweenMax.delayedCall(0.1, AudioManager.decBacksoundVolume, null, AudioManager);
    } else {
        TweenMax.killDelayedCallsTo(AudioManager.decBacksoundVolume);
        AudioConfig.bgmVolume = minBgmVol;
    }
    Config.audioBGM.volume(AudioConfig.bgmVolume);
};
AudioManager.incBacksoundVolume = function() {
    TweenMax.killDelayedCallsTo(AudioManager.decBacksoundVolume);
    if (AudioConfig.bgmMuteState) return;
    AudioConfig.bgmVolume = Config.audioBGM.volume();
    if (AudioConfig.bgmVolume < AudioConfig.maxBgmVolume) {
        AudioConfig.bgmVolume += 0.05;
        TweenMax.delayedCall(0.1, AudioManager.incBacksoundVolume, null, AudioManager);
    } else {
        TweenMax.killDelayedCallsTo(AudioManager.incBacksoundVolume);
        AudioConfig.bgmVolume = AudioConfig.maxBgmVolume;
    	AudioConfig.incDescState = false;
    }
    Config.audioBGM.volume(AudioConfig.bgmVolume);
};
AudioManager.setBGMSFXVolume = function(_vol) {
    if (Config.audioBGM != null) {
        AudioConfig.maxBgmVolume = (_vol / 1) * 0.7;
        if (!AudioConfig.incDescState)
        {
        	AudioConfig.bgmVolume = AudioConfig.maxBgmVolume;
        	Config.audioBGM.volume(AudioConfig.bgmVolume);
        }
    }
};
//Sound FX
AudioManager.playSoundFX = function(_id) {
    if (AudioConfig.sfxSize > 1) {
        AudioConfig.playedIDSFX = _id;
        AudioConfig.SFXID[_id] = Config.audioBGM.play('sfx_' + _id);
        Config.audioBGM.once('end', function() {
            AudioConfig.playedIDSFX = -1;
        }, AudioConfig.SFXID[_id]);
    } else {
        AudioConfig.SFXID = Config.audioBGM.play('sfx_' + _id);
    }
};
AudioManager.stopSoundFX = function(_id) {
    if (AudioConfig.sfxSize > 1) {
        Config.audioBGM.stop(AudioConfig.SFXID[_id]);
    } else {
        Config.audioBGM.stop(AudioConfig.SFXID);
    }
    AudioConfig.playedIDSFX = -1;
};
AudioManager.pauseSoundFX = function() {
    if (AudioConfig.bgmMuteState) return;
    if (AudioConfig.allMuteState) return;
    if (AudioConfig.sfxSize > 1) {
        if (AudioConfig.playedIDSFX == -1) return;
        Config.audioBGM.pause(AudioConfig.SFXID[AudioConfig.playedIDSFX]);
    } else {
        if (AudioConfig.SFXID == null) return;
        Config.audioBGM.pause(AudioConfig.SFXID);
    }
};
AudioManager.resumeSoundFX = function() {
    if (AudioConfig.bgmMuteState) return;
    if (AudioConfig.allMuteState) return;
    if (AudioConfig.sfxSize > 1) {
        if (AudioConfig.playedIDSFX == -1) return;
        Config.audioBGM.play(AudioConfig.SFXID[AudioConfig.playedIDSFX]);
    } else {
        if (AudioConfig.SFXID == null) return;
        Config.audioBGM.play(AudioConfig.SFXID);
    }
};
//General Audio
AudioManager.initAudio = function(_audioOnly) {
    var i;
    var audioOnly = false;
    if (_audioOnly != null && _audioOnly != undefined) {
        audioOnly = _audioOnly;
    }
    if (Config.audio != null) {
        AudioManager.forceStopAudio();
        Config.audio.unload();
        Config.audio = null;
    }
    if (!audioOnly) {
        if (!Config.preloaderState) {
            Main.showPreload();
        }
    }
    var volAudio;
    if (AudioConfig.audioMuteState) {
        volAudio = 0;
    } else {
        volAudio = AudioConfig.audioVolume;
    }
    Config.audio = new Howl({
        src: ["data/audio/" + Config.contentIds[Config.page] + ".mp3","data/audio/" + Config.contentIds[Config.page] + ".ogg"],
        sprite: AudioConfig.audioSprite[Config.page],
        volume: volAudio,
        onload: function() {
            if (!audioOnly) {
                if (Config.firstLoad) {
                    Main.loadContent("", null);
                } else {
                    if (Config.lastPage < Config.page) {
                        Main.loadContent("", true);
                    } else {
                        Main.loadContent("", false);
                    }
                }
            }
            Config.audio.play(Config.contentIds[Config.page] + '_0');
        }
    });
    Config.audio.once("loaderror", function() {
        if (!audioOnly) {
            if (Config.firstLoad) {
                Main.loadContent("", null);
            } else {
                if (Config.lastPage < Config.page) {
                    Main.loadContent("", true);
                } else {
                    Main.loadContent("", false);
                }
            }
        }
        Config.audio.unload();
        Config.audio = null;
    });
};
AudioManager.playAudio = function(_id,_useContinue,_symbolID,_callback) {
    Config.audio.stop();
    AudioConfig.useContinue = false;
    if (_useContinue != null && _useContinue != undefined) {
        AudioConfig.useContinue = _useContinue;
    }
    AudioConfig.callBack = null;
    if (_callback != null && _callback != undefined) {
        AudioConfig.callBack = _callback;
    }
    AudioConfig.symbolID = "";
    if (_symbolID != null && _symbolID != undefined && _symbolID != "") {
        AudioConfig.symbolID = _symbolID;
    }
    Config.audio.once('end', function() {
        if (AudioConfig.useContinue) {
            if (AudioConfig.symbolID != "") {
                if (Config.hypeContent.getSymbolInstanceById(AudioConfig.symbolID) != null && Config.hypeContent.getSymbolInstanceById(AudioConfig.symbolID) != undefined) {
                    Config.hypeContent.getSymbolInstanceById(AudioConfig.symbolID).continueTimelineNamed('Main Timeline', Config.hypeContent.kDirectionForward, false);
                } else {
                    Config.hypeContent.continueTimelineNamed('Main Timeline', Config.hypeContent.kDirectionForward, false);
                }
            } else {
                Config.hypeContent.continueTimelineNamed('Main Timeline', Config.hypeContent.kDirectionForward, false);
            }
        }
        if (Config.useBacksound) {
            AudioManager.incBacksoundVolume();
        }
        if (AudioConfig.callBack != null) {
            AudioConfig.callBack();
        }
        AudioConfig.playedIDAudio = null;
        if (Config.useSubtitle) {
            PopupManager.resetSubtitleText();
        }
    });
    AudioConfig.playedIDAudio = Config.audio.play(Config.contentIds[Config.page] + '_' + _id);
    if (Config.useBacksound) {
        AudioManager.decBacksoundVolume();
    }
};
AudioManager.stopAudio = function() {
    if (Config.audio == null) return;
    if (AudioConfig.useContinue) {
        if (AudioConfig.symbolID != "") {
            if (Config.hypeContent.getSymbolInstanceById(AudioConfig.symbolID) != null && Config.hypeContent.getSymbolInstanceById(AudioConfig.symbolID) != undefined) {
                Config.hypeContent.getSymbolInstanceById(AudioConfig.symbolID).continueTimelineNamed('Main Timeline', Config.hypeContent.kDirectionForward, false);
            } else {
                Config.hypeContent.continueTimelineNamed('Main Timeline', Config.hypeContent.kDirectionForward, false);
            }
        } else {
            Config.hypeContent.continueTimelineNamed('Main Timeline', Config.hypeContent.kDirectionForward, false);
        }
    }
    if (Config.useBacksound) {
        AudioManager.incBacksoundVolume();
    }
    if (AudioConfig.callBack != null) {
        AudioConfig.callBack();
    }
    Config.audio.stop();
    AudioConfig.playedIDAudio = null;
};
AudioManager.forceStopAudio = function() {
    if (Config.audio == null) return;
    if (Config.useBacksound) {
        AudioManager.incBacksoundVolume();
    }
    if (AudioConfig.callBack != null) {
        AudioConfig.callBack = null;
    }
    Config.audio.stop();
    AudioConfig.playedIDAudio = null;
    if (Config.useSubtitle) {
        PopupManager.resetSubtitleText();
    }
};
AudioManager.pauseAudio = function() {
    if (AudioConfig.audioMuteState) return;
    if (AudioConfig.allMuteState) return;
    if (AudioConfig.playedIDAudio == null) return;
    Config.audio.pause(AudioConfig.playedIDAudio);
};
AudioManager.resumeAudio = function() {
    if (AudioConfig.audioMuteState) return;
    if (AudioConfig.allMuteState) return;
    if (AudioConfig.playedIDAudio == null) return;
    Config.audio.play(AudioConfig.playedIDAudio);
};
AudioManager.muteAudio = function(_state) {
    AudioConfig.audioMuteState = _state;
    if (AudioConfig.audioMuteState) {
        if (Config.audio != null) {
            Config.audio.volume(0);
        }
        if (AudioConfig.bgmMuteState) {
            if (Config.useVolumeWindow) {
                var btnVolNav;
                switch (Config.navigationType) {
                    case 0:
                    {
                        btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                        btnVolNav.innerHTML = '<img class="ico-menu" src="data/image/volume-off.png">';
                    }
                    break;
                    case 1:
                    {
                        btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                        btnVolNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/volume-off.png">';
                    }
                    break;
                    case 2:
                    {
                        btnVolNav = document.getElementById('btn-volume');
                        btnVolNav.innerHTML = '<img src="data/image/volume-off.png" />';
                    }
                    break;
                }
                btnVolNav.classList.remove('used');
                btnVolNav.classList.add('unused');
            }
        }
    } else {
        if (Config.audio != null) {
            Config.audio.volume(AudioConfig.audioVolume);
        }
        if (Config.useVolumeWindow) {
            var btnVolNav;
            switch (Config.navigationType) {
                case 0:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu" src="data/image/volume.png">';
                }
                break;
                case 1:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/volume.png">';
                }
                break;
                case 2:
                {
                    btnVolNav = document.getElementById('btn-volume');
                    btnVolNav.innerHTML = '<img src="data/image/volume.png" />';
                }
                break;
            }
            btnVolNav.classList.remove('unused');
            btnVolNav.classList.add('used');
        }
    }
};
//All Audio
AudioManager.setAudioVolume = function(_vol, _allAudio) {
    if (Config.audio != null) {
        AudioConfig.audioVolume = _vol;
        Config.audio.volume(AudioConfig.audioVolume);
    }
    if (_allAudio) {
        if (Config.audioBGM != null) {
            AudioConfig.bgmVolume = (_vol / 1) * 0.7;
            Config.audioBGM.volume(AudioConfig.bgmVolume);
        }
    }
};
AudioManager.muteAllAudio = function(_state) {
    AudioConfig.allMuteState = _state;
    if (AudioConfig.allMuteState) {
        if (Config.audio != null) {
            Config.audio.volume(0);
        }
        if (Config.audioBGM != null) {
            Config.audioBGM.volume(0);
        }
        if (Config.useVolumeWindow) {
            var btnVolNav;
            switch (Config.navigationType) {
                case 0:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu" src="data/image/volume-off.png">';
                }
                break;
                case 1:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/volume-off.png">';
                }
                break;
                case 2:
                {
                    btnVolNav = document.getElementById('btn-volume');
                    btnVolNav.innerHTML = '<img src="data/image/volume-off.png" />';
                }
                break;
            }
            btnVolNav.classList.remove('used');
            btnVolNav.classList.add('unused');
        }
    } else {
        if (Config.audio != null) {
            Config.audio.volume(AudioConfig.audioVolume);
        }
        if (Config.audioBGM != null) {
            Config.audioBGM.volume(AudioConfig.bgmVolume);
        }
        if (Config.useVolumeWindow) {
            var btnVolNav;
            switch (Config.navigationType) {
                case 0:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu" src="data/image/volume.png">';
                }
                break;
                case 1:
                {
                    btnVolNav = document.getElementById('btn-volume-nav-' + Config.navigationType);
                    btnVolNav.innerHTML = '<img class="ico-menu-' + Config.navigationType + '" src="data/image/volume.png">';
                }
                break;
                case 2:
                {
                    btnVolNav = document.getElementById('btn-volume');
                    btnVolNav.innerHTML = '<img src="data/image/volume.png" />';
                }
                break;
            }
            btnVolNav.classList.remove('unused');
            btnVolNav.classList.add('used');
        }
    }
    AudioConfig.bgmMuteState = AudioConfig.allMuteState;
    AudioConfig.audioMuteState = AudioConfig.allMuteState;
};
//audio only without syncronize to content page (for child content) 
AudioManager.loadChildAudio = function(_audioID, _iFrameElement) {
    if (_audioID == "" || _audioID == " " || _audioID == null || _audioID == undefined) return;
    var i;
    if (Config.audio != null) {
        AudioManager.forceStopAudio();
        Config.audio.unload();
        Config.audio = null;
    }
    if (!Config.preloaderState) {
        Main.showPreload();
    }
    var fileName = "";
    var volAudio;
    if (AudioConfig.audioMuteState) {
        volAudio = 0;
    } else {
        volAudio = AudioConfig.audioVolume;
    }
    AudioConfig.childPage = -1;
    for (var i = 0; i < Config.contentIds.length; i++) {
        if (Config.contentIds[i] == _audioID) {
            AudioConfig.childPage = i;
            break;
        }
    }
    Config.audio = new Howl({
        src: ["data/audio/" + Config.contentIds[AudioConfig.childPage] + ".mp3","data/audio/" + Config.contentIds[AudioConfig.childPage] + ".ogg"],
        sprite: AudioConfig.audioSprite[AudioConfig.childPage],
        volume: volAudio,
        onload: function() {
            $(_iFrameElement).html('<iframe style="width: 100%; height: 100%; min-width: 100%; min-height: 100%;" frameborder="0" scrolling="no" src="../modul/' + Config.contentIds[AudioConfig.childPage] + '.html"></iframe>');
            Config.audio.play(Config.contentIds[AudioConfig.childPage] + '_0');
            Main.hidePreload();
        }
    });
    Config.audio.once("loaderror", function() {
        $(_iFrameElement).html('<iframe style="width: 100%; height: 100%; min-width: 100%; min-height: 100%;" frameborder="0" scrolling="no" src="../modul/' + Config.contentIds[AudioConfig.childPage] + '.html"></iframe>');
        Config.audio.unload();
        Config.audio = null;
        Main.hidePreload();
    });
};
AudioManager.playChildAudio = function(_id,_useContinue,_symbolID,_callback) {
    Config.audio.stop();
    AudioConfig.useContinue = false;
    if (_useContinue != null && _useContinue != undefined) {
        AudioConfig.useContinue = _useContinue;
    }
    AudioConfig.callBack = null;
    if (_callback != null && _callback != undefined) {
        AudioConfig.callBack = _callback;
    }
    AudioConfig.symbolID = "";
    if (_symbolID != null && _symbolID != undefined && _symbolID != "") {
        AudioConfig.symbolID = _symbolID;
    }
    Config.audio.once('end', function() {
        if (AudioConfig.useContinue) {
            if (AudioConfig.symbolID != "") {
                if (Config.hypeContent.hypeContentChild.getSymbolInstanceById(AudioConfig.symbolID) != null && Config.hypeContent.hypeContentChild.getSymbolInstanceById(AudioConfig.symbolID) != undefined) {
                    Config.hypeContent.hypeContentChild.getSymbolInstanceById(AudioConfig.symbolID).continueTimelineNamed('Main Timeline', Config.hypeContent.hypeContentChild.kDirectionForward, false);
                } else {
                    Config.hypeContent.hypeContentChild.continueTimelineNamed('Main Timeline', Config.hypeContent.hypeContentChild.kDirectionForward, false);
                }
            } else {
                Config.hypeContent.hypeContentChild.continueTimelineNamed('Main Timeline', Config.hypeContent.hypeContentChild.kDirectionForward, false);
            }
        }
        if (Config.useBacksound) {
            AudioManager.incBacksoundVolume();
        }
        if (AudioConfig.callBack != null) {
            AudioConfig.callBack();
        }
        AudioConfig.playedIDAudio = null;
        if (Config.useSubtitle) {
            PopupManager.resetSubtitleText();
        }
    });
    AudioConfig.playedIDAudio = Config.audio.play(Config.contentIds[AudioConfig.childPage] + '_' + _id);
    if (Config.useBacksound) {
        AudioManager.decBacksoundVolume();
    }
};
AudioManager.stopChildAudio = function() {
    if (Config.audio == null) return;
    if (AudioConfig.useContinue) {
        if (AudioConfig.symbolID != "") {
            if (Config.hypeContent.hypeContentChild.getSymbolInstanceById(AudioConfig.symbolID) != null && Config.hypeContent.hypeContentChild.getSymbolInstanceById(AudioConfig.symbolID) != undefined) {
                Config.hypeContent.hypeContentChild.getSymbolInstanceById(AudioConfig.symbolID).continueTimelineNamed('Main Timeline', Config.hypeContent.hypeContentChild.kDirectionForward, false);
            } else {
                Config.hypeContent.hypeContentChild.continueTimelineNamed('Main Timeline', Config.hypeContent.hypeContentChild.kDirectionForward, false);
            }
        } else {
            Config.hypeContent.hypeContentChild.continueTimelineNamed('Main Timeline', Config.hypeContent.hypeContentChild.kDirectionForward, false);
        }
    }
    if (Config.useBacksound) {
        AudioManager.incBacksoundVolume();
    }
    if (AudioConfig.callBack != null) {
        AudioConfig.callBack();
    }
    Config.audio.stop();
    AudioConfig.playedIDAudio = null;
};