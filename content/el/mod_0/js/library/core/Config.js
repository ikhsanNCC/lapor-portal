var md = new MobileDetect(window.navigator.userAgent); //Mobile Browser
var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0; //Opera 8.0+
var isFirefox = typeof InstallTrigger !== 'undefined'; //Firefox 1.0+
var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0; //At least Safari 3+: "[object HTMLElementConstructor]"
var isIE = /*@cc_on!@*/false || !!document.documentMode; //Internet Explorer 6-11
var isEdge = !isIE && !!window.StyleMedia; //Edge 20+
var isChrome = !!window.chrome && !!window.chrome.webstore; //Chrome 1+
var isBlink = (isChrome || isOpera) && !!window.CSS; //Blink engine detection
if (isIE) {
    var strError = "Your browser not fully support HTML5 and CSS3.\nPlease use one of the browser listed below for better experience.\n- Google Chrome. (latest version recommended)\n- Mozilla FireFox. (latest version recommended)\n- Safari. (latest version recommended)\n- Opera. (latest version recommended)\n- Microsoft Edge. (latest version recommended)\n\nBrowser Anda tidak sepenuhnya mendukung HTML5 dan CSS3.\nSilakan gunakan salah satu browser yang tercantum di bawah untuk pengalaman yang lebih baik.\n- Google Chrome. (Versi terbaru dianjurkan)\n- Mozilla FireFox. (Versi terbaru dianjurkan)\n- Safari. (Versi terbaru dianjurkan)\n- Opera. (Versi terbaru dianjurkan)\n- Microsoft Edge. (Versi terbaru dianjurkan)";
    window.alert(strError);
}
pipwerks.SCORM.version = "1.2";
var Config = {
    debugMode: false,
    debugLayout: false,
    isLock: false,
    isScorm: false,
    dynamicBackgroundColor: false,
    layoutType: 0,
    slidingType: 0,
    navigationType: 0,
    navigationColor: "",
    useContentControl: false,
    useScreenWindow: false,
    useVolumeWindow: false,
    useHelpWindow: false,
    useGlossaryWindow: false,
    useIndexListWindow: false,
    helpType: 0,
    glossaryType: 0,
    indexListType: 0,
    headerBackgroundColor: "",
    headerBackgroundImage: "",
    logoBackgroundImage: "",
    titleBackgroundImage: "",
    modul: 0,
    modulTitle: "",
    modulTitleFontRatio: 25,
    modulTitleTextColor: "",
    contentList: [],
    chapterTitle: [],
    chapterTitleFontRatio: 25,
    chapterTitleTextColor: "",
    useAudio: false,
    useBacksound: false,
    useSubtitle: false,
    openSubtitleAtStart: false,
    syncSubtitleUpdateWithAudio: false,
    syncSubtitleResetWithAudio: false,
    subtitleType: 0,
    subtitleFontRatio: 23,
    subtitleTextColor: "",
    subtitleBackgroundColor: "",
    useContentText: false,
    usePageNumber: false,
    pageNumberTextColor: "",
    pageNumberBackgroundColor: "",
    preloaderType: 0,
    preloaderObjectColor: "",
    preloaderBackgroundColor: "",
    contentAnim: {},
    contentSymbolAnim: {},
    contentChildAnim: {},
    contentChildSymbolAnim: {},
    contentIds: [],
    sceneLocation: [],
    lessonLocation: [],
    page: 0,
    endPage: 0,
    lastPage: 0,
    currentPage: 0,
    currentScene: 0,
    scoreRaw: 0,
    frameObject: 1,
    breakIdx: -1,
    columnNavigation: 0,
    navigationWidth: 0,
    maxSubtitleX: 0,
    maxSubtitleY: 0,
    moveToContentScene: -1,
    moveToContentTimeline: -1,
    audio: null,
    audioBGM: null,
    preloaderFrame: null,
    helpFrame: null,
    indexListFrame: null,
    glossaryFrame: null,
    screenFrame: null,
    volumeFrame: null,
    content1Frame: null,
    content2Frame: null,
    pdfFrame: null,
    pdfFrameBody: null,
    hypeHelp: null,
    hypeIndexList: null,
    hypeGlossary: null,
    hypeVolume: null,
    hypeScreen: null,
    hypeContent: null,
    intervalFunction: null,
    firstLoad: true,
    nextState: false,
    prevState: false,
    jumpingState: false,
    lmsConnected: false,
    contentPaused: false,
    isNextContent: false,
    menuOpen: false,
    helpOpen: false,
    indexListOpen: false,
    glossaryOpen: false,
    volumeOpen: false,
    showSubtitleWindow: false,
    canOpenSubtitleWindow: false,
    blockSubtitleBtn: false,
    checkingLastPage: false,
    preloaderState: false,
    subtitleOpened: false,
    subtitleNotEmpty: false,
    playpauseState: false,
    pauseWithButton: false,
    lessonStatus: "incomplete",
    scorm: pipwerks.SCORM,
    setContentId: function() {
        var strM = "";
        if (Config.modul > 9) {
            strM = "m" + Config.modul;
        } else {
            strM = "m0" + Config.modul;
        }
        for (var i = 0; i < Config.contentList.length; i++) {
            for (var j = 0; j < Config.contentList[i]; j++) {
                var tmpStr = "";
                if (i > 8) {
                    if (j > 8) {
                        tmpStr = strM + "c" + (i + 1) + "p" + (j + 1);
                    } else {
                        tmpStr = strM + "c" + (i + 1) + "p0" + (j + 1);
                    }
                } else {
                    if (j > 8) {
                        tmpStr = strM + "c0" + (i + 1) + "p" + (j + 1);
                    } else {
                        tmpStr = strM + "c0" + (i + 1) + "p0" + (j + 1);
                    }
                }
                Config.contentIds.push(tmpStr);
            }
        }
        Config.endPage = (Config.contentIds.length - 1);
    },
    setLocation: function() {
        Config.sceneLocation = [];
        Config.lessonLocation = [];
        if (Config.isLock) {
            for (var i = 0; i < Config.contentList.length; i++) {
                Config.lessonLocation.push(0);
            }
        } else {
            for (var i = 0; i < Config.contentList.length; i++) {
                Config.lessonLocation.push(Config.contentList[i]);
            }
        }
        for (var j = 0; j < Config.contentIds.length; j++) {
            Config.sceneLocation.push(0);
        }
    }
};
var AudioConfig = {
    bgmSize: 0,
    sfxSize: 0,
    bgmVolume: 0.7,
    maxBgmVolume: 0.7,
    audioVolume: 1,
    playedIDBGM: -1,
    playedIDSFX: -1,
    playedIDAudio: null,
    bgmSprite: {},
    audioSprite: [],
    allMuteState: false,
    bgmMuteState: false,
    audioMuteState: false,
    useContinue: false,
    incDescState: false,
    BGMID: null,
    SFXID: null,
    callBack: null,
    setAudioSprite: null,
    childPage: -1,
    symbolID: ""
};
function debugMsg(_msg) {
    if (Config.debugMode) {
        console.log(_msg);
    }
}
var unloaded = false;
function unloadHandler() {
	if(!unloaded) {
        if (Config.isScorm) {
            ScormManager.setLmsSession();
		    Config.scorm.save();
		    Config.scorm.quit();
        }
        unloaded = true;
		document.getElementById('mod-wrapper').innerHTML = '<iframe id="glossaryF" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe><iframe id="helpF" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe><iframe id="screenF" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe><iframe id="indexListF" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe><iframe id="audioF" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe><iframe id="content-1F" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe><iframe id="content-2F" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe><iframe id="content-3F" type="text/html" frameborder="0" scrolling="no" src="about:blank"></iframe>';
	}
}
window.onbeforeunload = unloadHandler;
window.onunload = unloadHandler;