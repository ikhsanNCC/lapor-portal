pipwerks.SCORM.version="1.2";
var unloaded = false;
function unloadHandler() {
	if(!unloaded) {
        Main.setLmsSession();
        Main.scorm.save();
        Main.scorm.quit();
        unloaded = true;
	}
}
window.onbeforeunload = unloadHandler;
window.onunload = unloadHandler;
var Main = {
    scorm: pipwerks.SCORM,
    dtmSessionTime: new Date()
};
Main.scorm.init();
Main.Initializing = function() {
    Main.dtmSessionTime = new Date();
    var video = document.getElementById('vidObj');
    video.onended = function() {
        Main.SetLmsStatus()
    }
};
Main.MillisecondsToCMIDuration = function(n) {
    var hms = "";
    var dtm = new Date();
    dtm.setTime(n);
    var h = "000" + Math.floor(n / 3600000);
    var m = "0" + dtm.getMinutes();
    var s = "0" + dtm.getSeconds();
    var cs = "0" + Math.round(dtm.getMilliseconds() / 10);
    hms = h.substr(h.length-4)+":"+m.substr(m.length-2)+":";
    hms += s.substr(s.length-2)+"."+cs.substr(cs.length-2);
    return hms;
};
Main.SetLmsSession = function() {
    var dtm = new Date();
    var tm = dtm.getTime() - Main.dtmSessionTime.getTime();
    var success = Main.scorm.set("cmi.core.session_time", Main.MillisecondsToCMIDuration(tm));
    if (success) {
        Main.scorm.save();
    }
};
Main.SetLmsStatus = function() {
    var success = Main.scorm.set("cmi.core.lesson_status", "completed");
    if (success) {
        Main.scorm.save();
    }
};
Main.Initializing();